# Change Log
## 0.2.0
* Better symbol finding for functions/methods
* Making a print statement in JMP to let the user know the server is running
* Fixed an issue with ranges not being correct

## 0.1.0

- Initial release
- Syntax Highlighting
- Snippets
- Symbol Definition