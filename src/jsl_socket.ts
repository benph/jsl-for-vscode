import * as vscode from 'vscode';
const output = vscode.window.createOutputChannel("JSL");
// Include Nodejs' net module.
const Net = require('net');
// The port number and hostname of the server.
const port = 14141;
const host = 'localhost';

export function RunCurrentJMPFile() {
    let editor = vscode.window.activeTextEditor!;
    let document = editor.document;
    let filepath = document.uri.path;
    let txt = document.getText();
    output.appendLine("//:*/");
    output.appendLine(txt);
    output.appendLine("/*");

    txt = "include(\"" + filepath + "\");\n";

    RunJMPCommand(txt);
}

export function RunCurrentJMPSelection() {
    let editor = vscode.window.activeTextEditor!;
    let document = editor.document;
    let txt = document.getText(editor.selection)
    if(txt != ""){
        output.appendLine("//:*/");
        output.appendLine(txt);
        output.appendLine("/*");
        RunJMPCommand(txt);
    }
    else{
        RunCurrentJMPFile();
    }

    
}

function RunJMPCommand(command:string) {
    // Create a new TCP client.
    let client = new Net.Socket();

    output.show();
    // Send a connection request to the server.
    client.connect({ port: port, host: host }, function() {
        // If there is no error, the server has accepted the request and created a new 
        // socket dedicated to us.
        // The client can now send data to the server by writing to its socket.
        client.write(command);
    });

    // The client can also receive data from the server by reading from its socket.
    client.on('data', function(chunk:object) {
        let result = chunk.toString();
        // console.log(`Data received from the server: ${chunk.toString()}.`);
        output.appendLine("");
        output.appendLine(result);
        // Request an end to the connection after the data has been received.
        client.end();
    });

    
}