// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
'use strict';
import * as vscode from 'vscode';
import { open } from 'fs';

import { exec } from 'child_process';
import { parentPort } from 'worker_threads';
const JSLSocket = require('./jsl_socket');
const jsl_functions = require('./JSLFunctions').jsl_functions;

let JMPLocation = "";
let context:vscode.ExtensionContext;

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(ctx: vscode.ExtensionContext) {

	// Use the console to output diagnostic information (console.log) and errors (console.error)
	// This line of code will only be executed once when your extension is activated
    
    const currentJMPLocation = () => vscode.workspace.getConfiguration('jsl').JMPLocation;
    JMPLocation = currentJMPLocation(); 
    
    context = ctx;

	context.subscriptions.push(vscode.languages.registerDocumentSymbolProvider(
        {language: "jsl"}, new JSLDocumentSymbolProvider()
    ));
    vscode.languages.registerHoverProvider('jsl', {
        provideHover(document, position, token) {
            const range = document.getWordRangeAtPosition(position);
            const word = document.getText(range);

            let obj = jsl_functions[word];
            
            return new vscode.Hover({
                language: "JSL",
                value: obj.prototype
            });
        }
    });
	
    registerCommands();
}

const registerCommands = () => {
    const commands:{[key: string]: any} = {
        'jsl.RunCurrentFile': JSLSocket.RunCurrentJMPFile,
        'jsl.RunCurrentSelection': JSLSocket.RunCurrentJMPSelection,
        "jsl.getJMPLocation": getJMPLocation
    };

    Object.keys(commands).forEach(cmd => {
        context.subscriptions.push(vscode.commands.registerCommand(cmd, commands[cmd]));
    });
};



async function getJMPLocation() {
    const jl = await vscode.window.showInputBox({
      ignoreFocusOut: true,
      value: JMPLocation,
      placeHolder: JMPLocation,
      prompt: 'Path to JMP exe',
    });
  
    if (jl) {
        context.globalState.update("JMPLocation", jl);
    }
}


async function sh(cmd: string) {
    return new Promise(function (resolve, reject) {
        exec(cmd, (err, stdout, stderr) => {
        if (err) {
            reject(err);
        } else {
            resolve({ stdout, stderr });
        }
        });
    });
}



class JSLDocumentSymbolProvider implements vscode.DocumentSymbolProvider {
    public provideDocumentSymbols(document: vscode.TextDocument,
            token: vscode.CancellationToken): Thenable<vscode.DocumentSymbol[]> {
        
        return new Promise((resolve, reject) => {
            var symbols: vscode.DocumentSymbol[] = [];
            var symbol_depth = [];
            var depth = 0;
            let txtlog = [];
            let ds = new vscode.DocumentSymbol("fake", "fake", vscode.SymbolKind.Class, document.lineAt(0).range, document.lineAt(0).range);

            let check_symbol_range = (symbols: vscode.DocumentSymbol[]) => {
                // trying to find the range of the parent
                for(let symbol in symbols){
                    let parent = symbols[symbol];

                    if(((parent as any).depth > depth) && (JSON.stringify(parent.selectionRange.end) === JSON.stringify(parent.range.end))){
                        let rng = new vscode.Range(parent.range.start, line.range.end);
                        parent.range = rng;
                    }
                    check_symbol_range(parent.children);
                }
                return null;
            };

            let find_last_symbol = (last_ds: vscode.DocumentSymbol) => {
                // finds where the symbol should be placed in the tree
                if(last_ds.children.length === 0) {return last_ds;}
                let last_symbol = last_ds.children[last_ds.children.length - 1];
                if(depth > (last_symbol as any).depth) {last_symbol = find_last_symbol(last_symbol);}
                else if(depth <= (last_symbol as any).depth) {last_symbol = last_ds;}
                return last_symbol;
            };

            for (var i = 0; i < document.lineCount; i++) {
                var line = document.lineAt(i);
                var txt = line.text;
                if(txt.trim().startsWith("//")){ continue;}
                let open_brackets = txt.match(/\(/g) || [];
                let close_brackets = txt.match(/\)/g) || [];

                let ds_match = false;
                let constructor_match = false;
                // uses depth of the open brackets and closed brackets to find the end 
                depth += open_brackets.length - close_brackets.length;

                if (/define\s*class\(\"(\w[\w\d]*)\"/i.test(txt)) {
                    ds_match = true;
                    let result = txt.match(/define\s*class\(\"(\w[\w\d]*)\"/i)!;
                    let sname = result[1].trim();
                    let detail = "";
                    ds = new vscode.DocumentSymbol(sname, detail, vscode.SymbolKind.Class, line.range, line.range);
                    (ds as any).depth = depth;              
                }
                else if (/new\s?custom\s?function\s?\(\"\w[\w\d]*\"\s*,\s*\"(\w[\w\d.]*)\"/i.test(txt)) { //also hard coded
                    ds_match = true;
                    let result = txt.match(/new\s?custom\s?function\s?\(\"\w[\w\d]*\"\s*,\s*\"(\w[\w\d.]*)\"/i)!;
                    let sname = result[1].trim();
                    let detail = "";
                    if(result.length === 3){
                        detail = result[2].trim();
                    }
                    ds = new vscode.DocumentSymbol(sname, detail, vscode.SymbolKind.Function, line.range, line.range);
                    (ds as any).depth = depth;
                }
                else if (/(?:(\w[\w\d\s.]*)=)?\s*(?:function|method) ?\((?:\s*{((?:\s*(?:\w[\w\d\s]*)(?:=.*)?,?\s*)*)}\s*,)?/i.test(txt)) { 
                    ds_match = true
                    let result = txt.match(/(?:(\w[\w\d\s.]*)=)?\s*(?:function|method) ?\((?:\s*{((?:\s*(?:\w[\w\d\s]*)(?:=.*)?,?\s*)*)}\s*,)?/i)!;
                    let sname = "<function>"
                    let detail = "";
                    if(result[1] != null){
                        sname = result[1].trim();
                    }
                    if(result.length === 3 && result[2] != null){
                        detail = result[2].trim();
                    }
                    ds = new vscode.DocumentSymbol(sname, detail, vscode.SymbolKind.Function, line.range, line.range);
                    (ds as any).depth = depth;
                }
                else if (/(?:ut\s*test\s*case\s*)\(\s*"([\w.:_\s]+)"\s*\)/i.test(txt)) { 
                    ds_match = true
                    let result = txt.match(/(?:ut\s*test\s*case\s*)\(\s*"([\w.:_\s]+)"\s*\)/i)!;
                    let sname = "uttestcase";
                    let detail = result[1].trim();
                    ds = new vscode.DocumentSymbol(sname, detail, vscode.SymbolKind.Object, line.range, line.range);
                    (ds as any).depth = depth;
                }
                else if (/(?:ut\s*test\s*)\(\s*[^,]+,\s*"([\w.:_\s]+)"\s*/i.test(txt)) { 
                    ds_match = true
                    let result = txt.match(/(?:ut\s*test\s*)\(\s*[^,]+,\s*"([\w.:_\s]+)"\s*/i)!;
                    let sname = "uttest";
                    let detail = result[1].trim();
                    ds = new vscode.DocumentSymbol(sname, detail, vscode.SymbolKind.Object, line.range, line.range);
                    (ds as any).depth = depth;
                }
                // this is made to make the functions have a constructor function
                else if (/new\s*object\(/i.test(txt)) { 
                    constructor_match = true;
                }
                
                
                check_symbol_range(symbols);
                
                if(ds_match || constructor_match){
                    
                    txtlog.push(i+" "+depth+" "+txt.trim());
                    // console.log(i, depth, txt.trim());
                    // anonymous function to find the last nested document symbol to put the stuff in. 
                    
                    // the last symbol in the main list
                    let last = symbols[symbols.length - 1];
                    if( symbols.length === 0) {
                        symbols.push(ds);
                    }
                    // checks if the depth is <= to depth of the last symbol
                    // or if the last symbol has already finished its range
                    else if((depth <= (last as any).depth || 
                        (last.range.end.line !== last.selectionRange.end.line || last.range.end.character !== last.selectionRange.end.character) ) && ds_match){
                        symbols.push(ds);
                    }
                    else{
                        // find the last nested symbol for the selected symbol
                        last = find_last_symbol(last);
                        if(ds_match) {
                            last.children.push(ds);
                        }
                        else if(constructor_match) {last.kind = vscode.SymbolKind.Constructor;}
                    }
                    
                }
            }
            
            // console.log(txtlog);
            resolve(symbols);
            return;
            console.log("How did I get past resolve");
            reject(console.log("I hit a boo boo"));
        });
    }
}

// this method is called when your extension is deactivated
export function deactivate() {}
