﻿exports.jsl_functions = {
    "\\[...]\\": {
        "args": [],
        "description": "Passages requiring many escape characters can use the delimiter \\[...]\\.",
        "prototype": "y = \\[string]\\"
    },
    "abbrevdate": {
        "args": [
            "datetime",
            "<format>"
        ],
        "description": "Returns an abbreviated locale-specific representation of a date-time value.",
        "prototype": "s = Abbrev Date( datetime, <format> )"
    },
    "abs": {
        "args": [
            "x"
        ],
        "description": "Returns the absolute value of x. Argument can be a number, matrix, or list of numbers.",
        "prototype": "y = Abs( x )"
    },
    "add": {
        "args": [
            "x0",
            "x1",
            "..."
        ],
        "description": "Adds all arguments, which can be numbers, matrices, or lists of numbers.",
        "prototype": "y = x0 + x1; y = Add( x0, x1, ... )"
    },
    "addcolortheme": {
        "args": [
            "{'Name'",
            "<flags>",
            "{color",
            "...}",
            "<{position",
            "...}>}"
        ],
        "description": "Adds a custom color theme to the global list.",
        "prototype": "Add Color Theme({'Name', <flags>, {color, ...}, <{position, ...}>})"
    },
    "addcustomfunctions": {
        "args": [
            "{f1",
            "f2",
            "...}",
            "|",
            "f"
        ],
        "description": "Defines a list of custom functions for use in scripting and the Formula Editor. The command also adds the list to the environment.",
        "prototype": "Add Custom Functions({f1, f2, ...} | f)"
    },
    "addjmplivebookmark": {
        "args": [
            "name",
            "URL(url)",
            "<User(user)>",
            "<Public(1",
            "|",
            "0)>",
            "<Replace(1",
            "|",
            "0)>"
        ],
        "description": "Add a bookmark location for publishing web reports",
        "prototype": "Add JMP Live Bookmark(name, URL(url), <User(user)>, <Public(1 | 0)>, <Replace(1 | 0)>)"
    },
    "addto": {
        "args": [
            "y",
            "x"
        ],
        "description": "Adds a value to a variable or to a list of variables.",
        "prototype": "y += x; Add To( y, x )"
    },
    "all": {
        "args": [
            "x",
            "..."
        ],
        "description": "Returns 1 if all elements are nonzero, zero otherwise.",
        "prototype": "y = All( x, ... )"
    },
    "alphashape": {
        "args": [
            "Triangulation"
        ],
        "description": "Returns the alpha shape for the given triangulation.",
        "prototype": "ashape = Alpha Shape(Triangulation)"
    },
    "and": {
        "args": [
            "x1",
            "x2",
            "..."
        ],
        "description": "Returns the logical AND of all arguments: 1 if all arguments are nonzero and 0 otherwise.",
        "prototype": "y = x1 & x2; y = And( x1, x2, ... )"
    },
    "andmz": {
        "args": [
            "x1",
            "x2",
            "..."
        ],
        "description": "Returns the logical AND of all arguments with missing values treated as zeros: 1 if all arguments are nonzero and 0 otherwise.",
        "prototype": "y = AndMZ( x1, x2, ... )"
    },
    "any": {
        "args": [
            "x",
            "..."
        ],
        "description": "Returns 1 if any element is nonzero, zero otherwise.",
        "prototype": "y = Any( x, ... )"
    },
    "arc": {
        "args": [
            "left",
            "top",
            "right",
            "bottom",
            "startAngle",
            "endAngle"
        ],
        "description": "Draws an arc of an oval.",
        "prototype": "Arc( left, top, right, bottom, startAngle, endAngle )"
    },
    "arccosh": {
        "args": [
            "x"
        ],
        "description": "Returns the inverse hyperbolic cosine of x.",
        "prototype": "y = ArcCosH( x )"
    },
    "arccosine": {
        "args": [
            "x"
        ],
        "description": "Returns the inverse trigonometric cosine of x, where x is in the range [-1, 1] and the result is in the range [0, Pi()].",
        "prototype": "y = ArcCosine( x )"
    },
    "arcfinder": {
        "args": [
            "Group(",
            "lot",
            "wafer",
            ")",
            "X(",
            "col",
            ")",
            "Y(",
            "col",
            ")",
            "<optional",
            "arguments>"
        ],
        "description": "Finds the arcs in the point data and creates a new column identifying the arcs.",
        "prototype": "Arc Finder( Group( lot, wafer ), X( col ), Y( col ), <optional arguments> )"
    },
    "arcos": {
        "args": [
            "x"
        ],
        "description": "Returns the inverse trigonometric cosine of x, where x is in the range [-1, 1] and the result is in the range [0, Pi()].",
        "prototype": "y = ArcCosine( x )"
    },
    "arcsine": {
        "args": [
            "x"
        ],
        "description": "Returns the inverse trigonometric sine of x, where x is in the range [-1, 1] and the result is in the range [-Pi()/2, Pi()/2].",
        "prototype": "y = ArcSine( x )"
    },
    "arcsinh": {
        "args": [
            "x"
        ],
        "description": "Returns the inverse hyperbolic sine of x.",
        "prototype": "y = ArcSinH( x )"
    },
    "arctan": {
        "args": [
            "x1",
            "<x2=1>"
        ],
        "description": "Returns the inverse trigonometric tangent of x1/x2, where the result is in the range [-Pi()/2, Pi()/2].",
        "prototype": "y = ArcTangent( x1, <x2=1> )"
    },
    "arctangent": {
        "args": [
            "x1",
            "<x2=1>"
        ],
        "description": "Returns the inverse trigonometric tangent of x1/x2, where the result is in the range [-Pi()/2, Pi()/2].",
        "prototype": "y = ArcTangent( x1, <x2=1> )"
    },
    "arctanh": {
        "args": [
            "x"
        ],
        "description": "Returns the inverse hyperbolic tangent of x.",
        "prototype": "y = ArcTanH( x )"
    },
    "arg": {
        "args": [
            "x",
            "i"
        ],
        "description": "Returns the ith argument of the evaluated expression or Empty() if there is no ith argument.",
        "prototype": "y = Arg( x, i )"
    },
    "argexpr": {
        "args": [
            "expr",
            "i"
        ],
        "description": "Returns the ith argument of the expression or Empty() if there is no ith argument. This function is deprecated. Please use Arg() instead.",
        "prototype": "y = Arg Expr( expr, i )"
    },
    "arimaforecast": {
        "args": [
            "dtcol",
            "length",
            "model",
            "estimates",
            "from",
            "to"
        ],
        "description": "Returns a vector of forecasted values for the dtcol column in the range determined by the from and to arguments. The length argument specifies a portion of the column for the function to use. The model argument matches messages that are sent to the Time Series platform to fit a model. The estimates argument matches the child of a Get Models message result of a single model. Typically, the from value is between 1 and the to value, inclusive. However, if from<=0 and from<=to, part of the results are filtered predictions.",
        "prototype": "x = ARIMA Forecast( dtcol, length, model, estimates, from, to )"
    },
    "arrhenius": {
        "args": [
            "tempC"
        ],
        "description": "Returns the non-specific component of the Arrhenius relationship that is then multiplied by the activation energy in the Arrhenius equation. Returns 11604.5181215503 / (tempC + 273.15).",
        "prototype": "y = Arrhenius( tempC )"
    },
    "arrheniusinv": {
        "args": [
            "y"
        ],
        "description": "Returns the inverse of the Arrhenius function, which is (11604.5181215503 / y) - 273.15.",
        "prototype": "tempC = Arrhenius Inv( y )"
    },
    "arrow": {
        "args": [
            "{x1",
            "y1}",
            "{x2",
            "y2}",
            "...",
            ");",
            "Arrow(",
            "xMatrix",
            "yMatrix"
        ],
        "description": "Draws a line with an arrow or a sequence of such lines.",
        "prototype": "Arrow( {x1, y1}, {x2, y2}, ... ); Arrow( xMatrix, yMatrix )"
    },
    "arsin": {
        "args": [
            "x"
        ],
        "description": "Returns the inverse trigonometric sine of x, where x is in the range [-1, 1] and the result is in the range [-Pi()/2, Pi()/2].",
        "prototype": "y = ArcSine( x )"
    },
    "asboolean": {
        "args": [
            "x"
        ],
        "description": "Evaluates an expression and returns a Boolean value.",
        "prototype": "b = As Boolean( x )"
    },
    "ascexpr": {
        "args": [
            "x"
        ],
        "description": "Returns an equivalent expression in the C programming language.",
        "prototype": "y = As C Expr( x )"
    },
    "ascolumn": {
        "args": [
            "name",
            ");\r\ny",
            "=",
            "As",
            "Column(",
            "dataTable",
            "name"
        ],
        "description": "Accesses the specified column in the specified or current data table. An error is thrown if no such column or data table is found.",
        "prototype": "y = :name;\r\ny = dataTable:name;\r\ny = As Column( name );\r\ny = As Column( dataTable, name )"
    },
    "asconstant": {
        "args": [
            "x"
        ],
        "description": "Evaluates an expression to create a constant value that does not change after it has been computed",
        "prototype": "y = As Constant( x )"
    },
    "asdate": {
        "args": [
            "datetime"
        ],
        "description": "Returns a date-time value marked internally as a date for output purposes.",
        "prototype": "dt = As Date( datetime )"
    },
    "asglobal": {
        "args": [
            "name"
        ],
        "description": "Accesses the specified global variable or throws an error if no such global variable exists.",
        "prototype": "y = ::name; y = As Global( name )"
    },
    "asjavascriptexpr": {
        "args": [
            "x"
        ],
        "description": "Returns an equivalent expression in the JavaScript programming language.",
        "prototype": "y = As JavaScript Expr( x )"
    },
    "asjsonexpr": {
        "args": [
            "x"
        ],
        "description": "Returns a JSON (JavaScript Object Notation) representation of the expression.",
        "prototype": "y = As JSON Expr( x )"
    },
    "aslist": {
        "args": [
            "matrix"
        ],
        "description": "Returns a list representation of a matrix. Multi-column matrices are converted to a list of lists, one per row, as would be expected by the Matrix operator.",
        "prototype": "y = As List( matrix )"
    },
    "asname": {
        "args": [
            "s"
        ],
        "description": "Converts a string into a name or a list of strings into a list of names.",
        "prototype": "y = As Name( s )"
    },
    "asnamespace": {
        "args": [
            "ns"
        ],
        "description": "Accesses the specified Namespace or throws an error if no such namespace exists.",
        "prototype": "asns = As Namespace( ns )"
    },
    "aspythonexpr": {
        "args": [
            "x"
        ],
        "description": "Returns an equivalent expression in the Python programming language.",
        "prototype": "y = As Python Expr( x )"
    },
    "asroot": {
        "args": [
            "name"
        ],
        "description": "Accesses the specified root-scoped variable or throws an error if no such root-scoped variable exists.",
        "prototype": "y = :::name; y = As Root( name )"
    },
    "asrowstate": {
        "args": [
            "x"
        ],
        "description": "Converts a number into a row state value.",
        "prototype": "rs = As Row State( x )"
    },
    "assasexpr": {
        "args": [
            "x"
        ],
        "description": "Returns a version of the expression more suitable for a SAS DATA step. The code must be wrapped in a PROC DS2 call.",
        "prototype": "y = As SAS Expr( x )"
    },
    "asscoped": {
        "args": [
            "namespace",
            "variable"
        ],
        "description": "Accesses the specified scoped variable or throws an error if no such scoped variable exists.",
        "prototype": "y = namespace:variable; y = As Scoped( namespace, variable )"
    },
    "assign": {
        "args": [
            "y",
            "x"
        ],
        "description": "Assigns a value to a variable or a list of variables.",
        "prototype": "y = x; Assign( y, x )"
    },
    "associativearray": {
        "args": [
            "{{key1",
            "value1}",
            "...}",
            ");\r\ny",
            "=",
            "Associative",
            "Array(",
            "keys",
            "values"
        ],
        "description": "Creates an associative array, also known as a dictionary or a hash map. In the two-argument form, keys and values may be a list, matrix, or data table column.",
        "prototype": "y = Associative Array( {{key1, value1}, ...} );\r\ny = Associative Array( keys, values )"
    },
    "assqlexpr": {
        "args": [
            "x",
            "<style>"
        ],
        "description": "Returns a string that contains the expression converted to valid SQL syntax for use in an SQL Select statement.",
        "prototype": "y = As SQL Expr( x, <style> )"
    },
    "astable": {
        "args": [
            "matrix",
            "<matrix2",
            "...>",
            "<",
            "<<invisible/private>",
            "<",
            "<<Column",
            "Names(name",
            "list)",
            ">"
        ],
        "description": "Converts a matrix into a data table. The invisible option can be used to avoid displaying the table.",
        "prototype": "dt = As Table( matrix, <matrix2,...> < <<invisible/private>, < <<Column Names(name list) > )"
    },
    "atan": {
        "args": [
            "x1",
            "<x2=1>"
        ],
        "description": "Returns the inverse trigonometric tangent of x1/x2, where the result is in the range [-Pi()/2, Pi()/2].",
        "prototype": "y = ArcTangent( x1, <x2=1> )"
    },
    "backcolor": {
        "args": [
            "<name|index|rgbList>"
        ],
        "description": "Sets the background color for the erase mode in the Text() function.",
        "prototype": "Back Color( <name|index|rgbList> )"
    },
    "beep": {
        "args": [],
        "description": "Makes an alert sound.",
        "prototype": "Beep()"
    },
    "bestpartition": {
        "args": [
            "xIndices",
            "yIndices",
            "<<Ordered",
            "<<ContinuousY",
            "<<ContinuousX"
        ],
        "description": "Determines the optimal grouping (experimental function).",
        "prototype": "{c1, c2, g2} = Best Partition( xIndices, yIndices, <<Ordered, <<ContinuousY, <<ContinuousX )"
    },
    "beta": {
        "args": [
            "x",
            "y"
        ],
        "description": "Returns Beta function of x and y, defined as Gamma( x ) * Gamma( y ) / Gamma( x + y ).",
        "prototype": "z = Beta( x, y )"
    },
    "betabinomialdistribution": {
        "args": [
            "k",
            "p",
            "n",
            "delta"
        ],
        "description": "Returns the probability that a Beta Binomial distributed random variable is less than or equal to k.",
        "prototype": "cumprob = Beta Binomial Distribution( k, p, n, delta )"
    },
    "betabinomialprobability": {
        "args": [
            "k",
            "p",
            "n",
            "delta"
        ],
        "description": "Returns the probability that a Beta Binomial distributed random variable is equal to k.",
        "prototype": "prob = Beta Binomial Probability( k, p, n, delta )"
    },
    "betabinomialquantile": {
        "args": [
            "p",
            "n",
            "delta",
            "cumprob"
        ],
        "description": "Returns the smallest integer quantile for which the cumulative probability of the Beta Binomial( p, n, delta ) distribution is larger than or equal to cumprob.",
        "prototype": "q = Beta Binomial Quantile( p, n, delta, cumprob )"
    },
    "betadensity": {
        "args": [
            "q",
            "alpha",
            "beta",
            "<theta=0>",
            "<sigma=1>"
        ],
        "description": "Returns the density at q for a beta distribution, where q is in the interval theta to theta + sigma, alpha and beta are shape parameters, and theta and sigma are threshold and range parameters, respectively.",
        "prototype": "y = Beta Density( q, alpha, beta, <theta=0>, <sigma=1> )"
    },
    "betadistribution": {
        "args": [
            "q",
            "alpha",
            "beta",
            "<theta=0>",
            "<sigma=1>"
        ],
        "description": "Returns the probability that a beta distributed random variable is less than q, where alpha and beta are shape parameters and theta and sigma are threshold and range parameters, respectively.",
        "prototype": "p = Beta Distribution( q, alpha, beta, <theta=0>, <sigma=1> )"
    },
    "betaquantile": {
        "args": [
            "p",
            "alpha",
            "beta",
            "<theta=0>",
            "<sigma=1>"
        ],
        "description": "Returns the quantile from a Beta distribution, the value for which the probability is p that a random value would be lower, where alpha and beta are shape parameters and theta and sigma are threshold and range parameters, respectively.",
        "prototype": "q = Beta Quantile( p, alpha, beta, <theta=0>, <sigma=1> )"
    },
    "binomialdistribution": {
        "args": [
            "p",
            "n",
            "k"
        ],
        "description": "Returns the probability that a Binomially distributed random variable is less than or equal to k.",
        "prototype": "cumprob = Binomial Distribution( p, n, k )"
    },
    "binomialprobability": {
        "args": [
            "p",
            "n",
            "k"
        ],
        "description": "Returns the probability that a Binomially distributed random variable is equal to k.",
        "prototype": "prob = Binomial Probability( p, n, k )"
    },
    "binomialquantile": {
        "args": [
            "p",
            "n",
            "cumprob"
        ],
        "description": "Returns the smallest integer quantile for which the cumulative probability of the Binomial( p, n ) distribution is larger than or equal to cumprob.",
        "prototype": "q = Binomial Quantile( p, n, cumprob )"
    },
    "blobmd5": {
        "args": [
            "blob"
        ],
        "description": "Makes a 16-byte result BLOB from a source BLOB (Binary Large OBject). The 16 byte BLOB is the MD5 checksum (or the hash) of the source BLOB.",
        "prototype": "blobResult = Blob MD5( blob )"
    },
    "blobpeek": {
        "args": [
            "blob",
            "offset",
            "<length>"
        ],
        "description": "Makes a new blob from a subrange of bytes of the given blob. The offset argument is zero-based, so the first byte is at offset zero.",
        "prototype": "blobResult = Blob Peek( blob, offset, <length> )"
    },
    "blobtochar": {
        "args": [
            "blob",
            "<encoding='utf-8'>"
        ],
        "description": "Makes a character string from a BLOB (Binary Large Object), using the specified encoding. Supported encodings include utf-8, utf-16le, utf-16be, us-ascii, iso-8859-1, shift_jis, euc-jp, and ascii~hex.",
        "prototype": "s = Blob To Char( blob, <encoding='utf-8'> )"
    },
    "blobtomatrix": {
        "args": [
            "blob",
            "type",
            "bytesEach",
            "endian",
            "<nCols=1>"
        ],
        "description": "Makes a matrix by converting bytes in the blob to numbers. type is either 'int', 'uint' or 'float'. bytesEach is either 1, 2, 4, or 8. endian indicates whether the first byte is the most significant ('big') or the least significant ('little'); 'native' indicates the machine's native format.",
        "prototype": "m = Blob To Matrix( blob, type, bytesEach, endian, <nCols=1> )"
    },
    "borderbox": {
        "args": [
            "<Left(",
            "pix",
            ")>",
            "<Right(",
            "pix",
            ")>",
            "<Top(",
            "pix",
            ")>",
            "<Bottom(",
            "pix",
            ")>",
            "<Sides(",
            "0",
            ")>",
            "displayBoxArg"
        ],
        "description": "Returns a display box to add space around the argument display box.",
        "prototype": "y = Border Box( <Left( pix )>, <Right( pix )>, <Top( pix )>, <Bottom( pix )>, <Sides( 0 )>, displayBoxArg )"
    },
    "boxplotseg": {
        "args": [
            "<data>",
            "<frequency>",
            "<weight>",
            "<vertical=0|1>"
        ],
        "description": "Returns a display seg representing a box plot based on the passed in x and y values.",
        "prototype": "b = Box Plot Seg(<data>, <frequency>, <weight>, <vertical=0|1>)"
    },
    "break": {
        "args": [],
        "description": "Causes a break in flow of control within a For or While loop.",
        "prototype": "Break()"
    },
    "bsplinecoef": {
        "args": [
            "x",
            "Internal",
            "Knot",
            "Grid",
            "<degree",
            "=",
            "3>",
            "<KnotEndPoints",
            "=",
            "min(x)",
            "||",
            "max(x)>"
        ],
        "description": "Returns the matrix of B-Spline coefficients. Internal Knot Grid is either the number of desired knot points based on percentiles of x or a vector specifying the internal knot points. Optional parameter degree specifies the degree of the B-splines with a default of 3. Optional parameter KnotEndPoints takes a 2x1 matrix containing [lower, upper] locations for the knots on the boundary. The knot end points default to the min and max of x. The second example demonstrates how B-spline coefficients can be used as the design matrix in a linear model.",
        "prototype": "coef = B Spline Coef( x, Internal Knot Grid, <degree = 3>, <KnotEndPoints = min(x) || max(x)> )"
    },
    "buildinformation": {
        "args": [],
        "description": "Returns the build date and time, release or debug build, and product name.",
        "prototype": "y = Build Information()"
    },
    "busylight": {
        "args": [
            "<",
            "<<Automatic(0|1)>",
            "<Size(x",
            "y)>",
            "<",
            "<<Disable>"
        ],
        "description": "Creates a rotating image to indicate a busy process.",
        "prototype": "y = Busy Light( < <<Automatic(0|1)>, <Size(x, y)>, < <<Disable> )"
    },
    "buttonbox": {
        "args": [
            "title",
            "script"
        ],
        "description": "Returns a display box to show a titled button. The script argument is run when the button is clicked.",
        "prototype": "y = Button Box( title, script )"
    },
    "calendarbox": {
        "args": [],
        "description": "Returns a display box containing a calendar control. The calendar supports single-selection of a date and optional time.",
        "prototype": "y = Calendar Box()"
    },
    "caption": {
        "args": [
            "{h",
            "v}",
            "text",
            "<Delayed(",
            "seconds",
            ")>",
            "<Font(font)>",
            "<Font",
            "Size(size)>",
            "<Text",
            "Color(color)>",
            "<Back",
            "Color(color)>",
            "<Spoken(bool)>"
        ],
        "description": "Shows a caption window at the location specified by {h, v} and containing the text specified by the text argument. The Delayed( seconds ) argument sets the waiting time in seconds before each caption.",
        "prototype": "y = Caption( {h, v}, text, <Delayed( seconds )>, <Font(font)>, <Font Size(size)>, <Text Color(color)>, <Back Color(color)>, <Spoken(bool)> )"
    },
    "casconnect": {
        "args": [
            "<URL(...)>",
            "<Username(...)>",
            "<Password(...)>",
            "<Prompt(Never",
            "|",
            "Always",
            "|",
            "IfNeeded)>"
        ],
        "description": "Creates and connects a CAS server.",
        "prototype": "CAS Connect(<URL(...)>, <Username(...)>, <Password(...)>, <Prompt(Never | Always | IfNeeded)>)"
    },
    "casdeletetable": {
        "args": [
            "tablename",
            "<remove>"
        ],
        "description": "Deletes a CAS table from CAS.",
        "prototype": "CAS Delete Table(tablename, <remove>)"
    },
    "casdisconnect": {
        "args": [],
        "description": "Disconnects the current CAS server.",
        "prototype": "CAS Disconnect()"
    },
    "casexportdata": {
        "args": [
            "dt",
            "libref",
            "dataset",
            "<named_arguments>"
        ],
        "description": "Exports a data table to a CAS server.",
        "prototype": "y = CAS Export Data(dt, libref, dataset, <named_arguments>)"
    },
    "casgetdatasets": {
        "args": [
            "<'caslib'>"
        ],
        "description": "Gets a list of available CAS datasets.",
        "prototype": "y = CAS Get Data Sets(<'caslib'>)"
    },
    "casgetlibraries": {
        "args": [],
        "description": "Gets a list of available CAS libraries.",
        "prototype": "y = CAS Get Libraries()"
    },
    "casimportdata": {
        "args": [
            "libref",
            "dataset",
            "<named_arguments>"
        ],
        "description": "Imports a data table from a CAS server.",
        "prototype": "dt = CAS Import Data(libref, dataset, <named_arguments>)"
    },
    "casisconnected": {
        "args": [],
        "description": "Returns 1 if there is an active CAS server connection. Otherwise, returns 0.",
        "prototype": "CAS Is Connected"
    },
    "casremovetable": {
        "args": [
            "tablename",
            "<delete>"
        ],
        "description": "Removes a CAS table from CAS.",
        "prototype": "CAS Remove Table(tablename, <delete>)"
    },
    "castabletodatatable": {
        "args": [
            "jsonstring",
            "<",
            "Invisible(1|0)",
            "|",
            "Private(1|0)",
            "|",
            "Use",
            "Labels",
            "for",
            "Var",
            "Names(1|0)>"
        ],
        "description": "Converts a SAS CAS Table JSON text to a JMP data table.",
        "prototype": "dt = CAS Table To Data Table(jsonstring <, Invisible(1|0) | Private(1|0) | Use Labels for Var Names(1|0)>)"
    },
    "casterminatesessions": {
        "args": [],
        "description": "Terminates all CAS sessions owned by the current user.",
        "prototype": "CAS Terminate Sessions"
    },
    "cauchydensity": {
        "args": [
            "q",
            "<center>",
            "<scale>"
        ],
        "description": "Returns the density at q of a Cauchy distribution with center mu and scale sigma.",
        "prototype": "y = Cauchy Density( q, <center>, <scale> )"
    },
    "cauchydistribution": {
        "args": [
            "q",
            "<center>",
            "<scale>"
        ],
        "description": "Returns the probability that a Cauchy distributed random variable is less than q.",
        "prototype": "p = Cauchy Distribution( q, <center>, <scale> )"
    },
    "cauchyquantile": {
        "args": [
            "p",
            "<center>",
            "<scale>"
        ],
        "description": "Returns the quantile from a Cauchy distribution, the value for which the probability is p that a random value would be lower.",
        "prototype": "q = Cauchy Quantile( p, <center>, <scale> )"
    },
    "cdf": {
        "args": [
            "Y"
        ],
        "description": "Returns values of the empirical cumulative probability distribution function for vector or list Y. Cumulative probability is the proportion of data values less than or equal to the corresponding entry in vector QuantVec",
        "prototype": "{QuantVec, CumProbVec} = CDF( Y )"
    },
    "ceiling": {
        "args": [
            "x"
        ],
        "description": "Returns the smallest integer greater than or equal to x. Argument can be a number, matrix, or list of numbers.",
        "prototype": "y = Ceiling( x )"
    },
    "char": {
        "args": [
            "x",
            "<w>",
            "<d>",
            "<",
            "<<Use",
            "Locale(",
            "Boolean",
            ")",
            ">"
        ],
        "description": "Returns a representation of x as a character string, using the maximum width w and decimal places d if the x argument is numeric.",
        "prototype": "s = Char( x, <w>, <d>, < <<Use Locale( Boolean ) > )"
    },
    "chartoblob": {
        "args": [
            "string",
            "<encoding='utf-8'>"
        ],
        "description": "Makes a BLOB (Binary Large Object) from a string of characters, using the specified encoding. Supported encodings include utf-8, utf-16le, utf-16be, us-ascii, iso-8859-1, shift_jis, euc-jp, and ascii~hex.",
        "prototype": "blob = Char To Blob( string, <encoding='utf-8'> )"
    },
    "chartohex": {
        "args": [
            "value",
            "<'integer'>|<encoding='utf-8'>"
        ],
        "description": "Returns the hexadecimal text corresponding to the given value and encoding, which can be a number, a string, or a blob. If the value is a number, IEEE 754 64-bit encoding is used unless the optional argument, 'integer', is provided. Supported encodings include utf-8, utf-16le, utf-16be, us-ascii, iso-8859-1, ascii~hex, shift_jis, and euc-jp.",
        "prototype": "h = Char To Hex( value, <'integer'>|<encoding='utf-8'> )"
    },
    "chartopath": {
        "args": [
            "pathText"
        ],
        "description": "Converts a path specification from character form to matrix form.",
        "prototype": "m = Char To Path( pathText )"
    },
    "checkbox": {
        "args": [
            "{item",
            "...}",
            "<script>"
        ],
        "description": "Returns a display box to show one or more check boxes.",
        "prototype": "y = Check Box( {item, ...}, <script> )"
    },
    "chisquaredensity": {
        "args": [
            "q",
            "df",
            "<nonCentrality=0>"
        ],
        "description": "Returns the density at q of a Chi-square distribution with df degrees of freedom.",
        "prototype": "p = ChiSquare Density( q, df, <nonCentrality=0> )"
    },
    "chisquaredistribution": {
        "args": [
            "q",
            "df",
            "<nonCentrality=0>"
        ],
        "description": "Returns the probability that a Chi-square distributed random variable is less than q.",
        "prototype": "p = ChiSquare Distribution( q, df, <nonCentrality=0> )"
    },
    "chisquarelogcdistribution": {
        "args": [
            "x",
            "df",
            "<nonCentrality=0>"
        ],
        "description": "Returns the log of 1 - Chi-square distribution.",
        "prototype": "y = ChiSquare Log CDistribution( x, df, <nonCentrality=0> )"
    },
    "chisquarelogdensity": {
        "args": [
            "x",
            "df",
            "<nonCentrality=0>"
        ],
        "description": "Returns the log of the Chi-square probability density.",
        "prototype": "y = ChiSquare Log Density( x, df, <nonCentrality=0> )"
    },
    "chisquarelogdistribution": {
        "args": [
            "x",
            "df",
            "<nonCentrality=0>"
        ],
        "description": "Returns the log of the Chi-square distribution.",
        "prototype": "y = ChiSquare Log Distribution( x, df, <nonCentrality=0> )"
    },
    "chisquarenoncentrality": {
        "args": [
            "x",
            "df",
            "prob"
        ],
        "description": "Returns the noncentrality parameter nc such that prob is equal to the probability that a Chi-square distributed random variable with df degrees of freedom is less than x.",
        "prototype": "nc = ChiSquare Noncentrality( x, df, prob )"
    },
    "chisquarequantile": {
        "args": [
            "p",
            "df",
            "<nonCentrality=0>"
        ],
        "description": "Returns the quantile from a Chi-Square distribution, the value for which the probability is p that a random value would be lower.",
        "prototype": "q = ChiSquare Quantile( p, df, <nonCentrality=0> )"
    },
    "cholesky": {
        "args": [
            "A"
        ],
        "description": "Returns the Cholesky decomposition of a positive semi-definite matrix. L is a lower triangular matrix such that L*L` = A.",
        "prototype": "L = Cholesky( A )"
    },
    "cholupdate": {
        "args": [
            "L",
            "V",
            "C"
        ],
        "description": "Returns an updated Cholesky root of A+V*C*V' where C is an m by m symmetric matrix and V is an n by m matrix. The argument L must be the Cholesky root of an n by n matrix A.",
        "prototype": "L2 = Chol Update( L, V, C )"
    },
    "choose": {
        "args": [
            "i",
            "expr1",
            "expr2",
            "...",
            "exprElse"
        ],
        "description": "Evaluates and returns the ith expr argument or the exprElse argument if there is no ith expr argument.",
        "prototype": "y = Choose( i, expr1, expr2, ..., exprElse )"
    },
    "chooseclosest": {
        "args": [
            "<source",
            "string>",
            "{<possible",
            "target>...}",
            "[Ignore",
            "Case(ignore=1|0)]?",
            "[Ignore",
            "Nonprintable(ignore=1|0)]?",
            "[Ignore",
            "Whitespace(ignore=1|0)]?",
            "[Ignore",
            "Case(ignore=1|0)]?",
            "[Max",
            "Edit",
            "Count(<count>)]?",
            "[Max",
            "Edit",
            "Ratio[0..1]",
            "]?",
            "[Min",
            "String",
            "Length(<count=3>)]?",
            "[Replace",
            "Unmatched(replace=0|1)]?",
            "[Unmatched",
            "Value(<value>)]?"
        ],
        "description": "Pick the closest string within the given rules and return it.",
        "prototype": "Choose Closest(<source string>, {<possible target>...}, [Ignore Case(ignore=1|0)]?, [Ignore Nonprintable(ignore=1|0)]?, [Ignore Whitespace(ignore=1|0)]?, [Ignore Case(ignore=1|0)]?, [Max Edit Count(<count>)]?, [Max Edit Ratio[0..1] ]?, [Min String Length(<count=3>)]?, [Replace Unmatched(replace=0|1)]?, [Unmatched Value(<value>)]?)"
    },
    "circle": {
        "args": [
            "{x",
            "y}",
            "radius|PixelRadius(",
            "px",
            ")",
            "...",
            "<'FILL'>"
        ],
        "description": "Draws a circle centered at {x, y}. The radius can be specified as an integer based on the vertical axis or as a number of pixels. A pixel-based radius creates a circle that does not vary in size when the vertical axis changes. Arguments may be repeated in any order to draw multiple circles. 'FILL', if used, must be last, and fills the circles with the fill color rather than drawing them with the pen color.",
        "prototype": "Circle( {x, y}, radius|PixelRadius( px ), ..., <'FILL'> )"
    },
    "classexists": {
        "args": [
            "class",
            "reference"
        ],
        "description": "Returns 1 if the class specified by the name argument exists. Otherwise, a 0 is returned.",
        "prototype": "nsexists = Class Exists( class reference )"
    },
    "clearglobals": {
        "args": [
            "<",
            "varname",
            "...",
            ">"
        ],
        "description": "Clears the values of all currently defined global symbols.",
        "prototype": "Clear Globals( < varname, ... > )"
    },
    "clearlog": {
        "args": [],
        "description": "Makes the log empty.",
        "prototype": "Clear Log()"
    },
    "clearsymbols": {
        "args": [
            "<",
            "varname",
            "...",
            ">"
        ],
        "description": "Clears the values of all currently defined symbols.",
        "prototype": "Clear Symbols( < varname, ... > )"
    },
    "close": {
        "args": [
            "<dataTableRef|name>",
            "<NoSave|Save(",
            "'path'",
            ")>"
        ],
        "description": "Closes the data table referenced by the first argument, which defaults to the current data table in the current project (or no project if not running the script in a project).\r\n\r\nTo specify a project, use the optional Project() argument with a title, index, display box, or window object. Use Project(0) to specify no project when running the script in a project.\r\n\r\nThe second argument is used to save the data table. Use an appropriate file extension in the path to save the data table as a non-JMP format. Specifying NoSave bypasses the prompt to save or disregard changes.",
        "prototype": "Close( <dataTableRef|name>, <NoSave|Save( 'path' )> )"
    },
    "closeall": {
        "args": [
            "<Project(title|index|box|window)",
            ">",
            "Data",
            "Tables",
            "|",
            "Reports",
            "|",
            "Journals",
            "<invisible",
            "|",
            "private>",
            "<NoSave|Save>"
        ],
        "description": "Closes all open resources of a specific type: data tables, journals, or reports.\r\n\r\nOnly windows in the current project (or no project if not running the script in a project) will be closed. To specify a project, use the optional Project() argument with a title, index, display box, or window object. Use Project(0) to specify no project when running the script in a project.",
        "prototype": "Close All( <Project(title|index|box|window),> Data Tables | Reports | Journals, <invisible | private>, <NoSave|Save> )"
    },
    "closedatabaseconnection": {
        "args": [
            "databaseConnectionHandle"
        ],
        "description": "Closes a database connection returned from Create Database Connection",
        "prototype": "Close Database Connection(databaseConnectionHandle)"
    },
    "closelog": {
        "args": [],
        "description": "Close the log window",
        "prototype": "Close Log()"
    },
    "colbox": {
        "args": [
            "title",
            "boxes"
        ],
        "description": "Returns a column box made up of the given display boxes.",
        "prototype": "y = Col Box( title, boxes )"
    },
    "colcumulativesum": {
        "args": [
            "xCol",
            "<byVar",
            "...>",
            "<",
            "<<",
            "Freq(",
            "freqCol",
            ")",
            ">"
        ],
        "description": "Returns the cumulative sum for the current row. By variables do not need to be presorted.",
        "prototype": "y = Col Cumulative Sum( xCol, <byVar, ...>, < << Freq( freqCol ) > )"
    },
    "collapsewhitespace": {
        "args": [
            "s"
        ],
        "description": "Trims leading and trailing whitespace and removes duplicate interior white spaces",
        "prototype": "scw = Collapse Whitespace( s )"
    },
    "collistbox": {
        "args": [
            "<Data",
            "Table(",
            "name",
            ")>",
            "<all>|<character|numeric>",
            "<width(",
            "pix",
            ")>",
            "<grouped>",
            "<maxSelected(",
            "n",
            ")>",
            "<nlines(",
            "n",
            ")>",
            "<MaxItems(",
            "n",
            ")>",
            "<MinItems(",
            "n",
            ")>",
            "<onChange(",
            "expr",
            ")>",
            "<",
            "<<Set",
            "Analysis",
            "Type(Any|Continuous|Ordinal|Nominal)>",
            "<",
            "<<",
            "Set",
            "Data",
            "Type(Any|Numeric|Character)>",
            "<script>"
        ],
        "description": "Returns a display box to show list box to select data table columns. Use the <<Modeling Type message to allow specialty modeling types or to restrict the types allowed. The default value of 'Any' will allow any column with a classic modeling type ('Continuous', 'Nominal', 'Ordinal').",
        "prototype": "y = Col List Box( <Data Table( name )>, <all>|<character|numeric>, <width( pix )>, <grouped>, <maxSelected( n )>, <nlines( n )>, <MaxItems( n )>, <MinItems( n )>, <onChange( expr )>, < <<Set Analysis Type(Any|Continuous|Ordinal|Nominal)>, < << Set Data Type(Any|Numeric|Character)>, <script> )"
    },
    "colmax": {
        "args": [
            "xCol",
            "<byVar",
            "...>"
        ],
        "description": "Returns the maximum value across rows in a column. The result is cached internally so that multiple evaluations will be efficient. The optional byVar arguments specify by groups for the calculation. Note that byVar arguments should be used in a column formula or in a For Each Row() function.",
        "prototype": "y = Col Maximum( xCol, <byVar, ...> )"
    },
    "colmaximum": {
        "args": [
            "xCol",
            "<byVar",
            "...>"
        ],
        "description": "Returns the maximum value across rows in a column. The result is cached internally so that multiple evaluations will be efficient. The optional byVar arguments specify by groups for the calculation. Note that byVar arguments should be used in a column formula or in a For Each Row() function.",
        "prototype": "y = Col Maximum( xCol, <byVar, ...> )"
    },
    "colmean": {
        "args": [
            "xCol",
            "<byVar",
            "...>",
            "<",
            "<<",
            "Freq(",
            "freqCol",
            ")",
            ">"
        ],
        "description": "Returns the sample mean across rows in a column. The result is cached internally so that multiple evaluations will be efficient. The optional byVar arguments specify by groups for the calculation. Note that byVar arguments should be used in a column formula or in a For Each Row() function.",
        "prototype": "y = Col Mean( xCol, <byVar, ...>, < << Freq( freqCol ) > )"
    },
    "colmedian": {
        "args": [
            "xCol",
            "<byVar",
            "...>",
            "<",
            "<<",
            "Freq(",
            "freqCol",
            ")",
            ">"
        ],
        "description": "Returns the specified median across rows in a column. The ordering is cached internally so that multiple evaluations will be efficient.",
        "prototype": "y = Col Median( xCol, <byVar, ...>, < << Freq( freqCol ) > )"
    },
    "colmin": {
        "args": [
            "xCol",
            "<byVar",
            "...>"
        ],
        "description": "Returns the minimum value across rows in a column. The result is cached internally so that multiple evaluations will be efficient. The optional byVar arguments specify by groups for the calculation. Note that byVar arguments should be used in a column formula or in a For Each Row() function.",
        "prototype": "y = Col Minimum( xCol, <byVar, ...> )"
    },
    "colminimum": {
        "args": [
            "xCol",
            "<byVar",
            "...>"
        ],
        "description": "Returns the minimum value across rows in a column. The result is cached internally so that multiple evaluations will be efficient. The optional byVar arguments specify by groups for the calculation. Note that byVar arguments should be used in a column formula or in a For Each Row() function.",
        "prototype": "y = Col Minimum( xCol, <byVar, ...> )"
    },
    "colmovingaverage": {
        "args": [
            "xCol",
            "<weighting=0.25>",
            "<before=-1>",
            "<after=0>",
            "<partial",
            "window",
            "is",
            "missing=1>",
            "<byVar",
            "...>"
        ],
        "description": "Returns the moving average over a given interval based at the current row. For the weight multiplier, 1 means equal weighting, 0 means linear weighting, and other values act as an exponential weighting multiplier. By variables do not need to be presorted.",
        "prototype": "y = Col Moving Average( xCol, <weighting=0.25>, <before=-1>, <after=0>, <partial window is missing=1>, <byVar, ...> )"
    },
    "colnmissing": {
        "args": [
            "xCol",
            "<byVar",
            "...>"
        ],
        "description": "Returns the number of missing values across rows in a column. The result is cached internally so that multiple evaluations will be efficient. The optional byVar arguments specify by groups for the calculation. Note that byVar arguments should be used in a column formula or in a For Each Row() function.",
        "prototype": "y = Col N Missing( xCol, <byVar, ...> )"
    },
    "colnumber": {
        "args": [
            "xCol",
            "<byVar",
            "...>",
            "<",
            "<<",
            "Freq(",
            "freqCol",
            ")",
            ">"
        ],
        "description": "Returns the number of nonmissing values across rows in a column. The result is cached internally so that multiple evaluations will be efficient. The optional byVar arguments specify by groups for the calculation. Note that byVar arguments should be used in a column formula or in a For Each Row() function.",
        "prototype": "y = Col Number( xCol, <byVar, ...>, < << Freq( freqCol ) > )"
    },
    "colorof": {
        "args": [
            "rs",
            ");",
            "Color",
            "Of(",
            "<Row",
            "State(",
            "<r>",
            ")>"
        ],
        "description": "Returns the color component of the specified row state value, either a positive JMP color palette index or a negative RGB-encoded value. If Color Of is used as an L-value, it changes the color of the current (or rth) row in the current data table.",
        "prototype": "y = Color Of( rs ); Color Of( <Row State( <r> )> ) = y"
    },
    "colorstate": {
        "args": [
            "color"
        ],
        "description": "Returns a row state value with the color component set to the specified value. The color argument can be any valid JSL color.",
        "prototype": "rs = Color State( color )"
    },
    "colortohls": {
        "args": [
            "color"
        ],
        "description": "Returns a list of the hue, lightness, and saturation components.  The color argument can be any valid JSL color, or a matrix of color numbers.",
        "prototype": "{h, l, s} = Color To HLS( color )"
    },
    "colortorgb": {
        "args": [
            "color"
        ],
        "description": "Returns a list of the red, green, and blue components, between 0 and 1. The color argument can be any valid JSL color, or a matrix of color numbers.",
        "prototype": "{r, g, b} = Color To RGB( color )"
    },
    "colquantile": {
        "args": [
            "xCol",
            "p",
            "<byVar",
            "...>",
            "<",
            "<<",
            "Freq(",
            "freqCol",
            ")",
            ">"
        ],
        "description": "Returns the specified quantile across rows in a column. The ordering is cached internally so that multiple evaluations will be efficient.",
        "prototype": "y = Col Quantile( xCol, p, <byVar, ...>, < << Freq( freqCol ) > )"
    },
    "colrank": {
        "args": [
            "xCol",
            "<byVar",
            "...>",
            "<",
            "<<tie('average'|'row'|'minimum'|'arbitrary')>"
        ],
        "description": "Returns the rank, ranging from 1 as the lowest, with row-order tie-breaking unless specified by the <<Tie argument. 'average' produces the average for tied ranks, and 'minimum' produces the lowest of tied ranks. For 'row' and 'arbitrary' each row has a unique rank.",
        "prototype": "y = Col Rank( xCol, <byVar, ...>, < <<tie('average'|'row'|'minimum'|'arbitrary')> )"
    },
    "colshuffle": {
        "args": [],
        "description": "Returns a random integer between 1 and the number of rows of the current data table. When used in a column formula, Col Shuffle() creates a random ordering of row numbers with each row number appearing only once. The ordering is cached internally so that multiple evaluations are efficient.",
        "prototype": "y = Col Shuffle()"
    },
    "colsimpleexponentialsmoothing": {
        "args": [
            "xCol",
            "alpha",
            "<byVar",
            "...>"
        ],
        "description": "Returns the simple exponential smoothing prediction for the current row, using smoothing weight alpha. By variables do not need to be presorted. Formula is Predicted Value[t]=alpha * Observed Value[t-1] + (1-alpha) * Predicted Value[t-1], with Predicted Value[1] = Observed Value[1].",
        "prototype": "y = Col Simple Exponential Smoothing( xCol, alpha, <byVar, ...> )"
    },
    "colspanbox": {
        "args": [
            "title",
            "children"
        ],
        "description": "Returns a column that has a header that spans child columns",
        "prototype": "y = Col Span Box( title, children )"
    },
    "colstandardize": {
        "args": [
            "xCol"
        ],
        "description": "Returns column value minus mean divided by standard deviation across rows in a column.",
        "prototype": "y = Col Standardize( xCol )"
    },
    "colstddev": {
        "args": [
            "xCol",
            "<byVar",
            "...>",
            "<",
            "<<",
            "Freq(",
            "freqCol",
            ")",
            ">"
        ],
        "description": "Returns the sample standard deviation across rows in a column. The result is cached internally so that multiple evaluations will be efficient. The optional byVar arguments specify by groups for the calculation. Note that byVar arguments should be used in a column formula or in a For Each Row() function.",
        "prototype": "y = Col Std Dev( xCol, <byVar, ...>, < << Freq( freqCol ) > )"
    },
    "colstoredvalue": {
        "args": [
            "<dt",
            ">",
            "xCol",
            "<",
            "row=Row()",
            ">"
        ],
        "description": "Returns column value that has not had column properties applied to it. If row option is not specified, then the current row is assumed.",
        "prototype": "y = Col Stored Value( <dt,> xCol, < row=Row() > )"
    },
    "colsum": {
        "args": [
            "xCol",
            "<byVar",
            "...>",
            "<",
            "<<",
            "Freq(",
            "freqCol",
            ")",
            ">"
        ],
        "description": "Returns the sum across rows in a column. The result is cached internally so that multiple evaluations will be efficient. The optional byVar arguments specify by groups for the calculation. Note that byVar arguments should be used in a column formula or in a For Each Row() function.",
        "prototype": "y = Col Sum( xCol, <byVar, ...>, < << Freq( freqCol ) > )"
    },
    "column": {
        "args": [
            "name|number",
            ");\r\ny",
            "=",
            "Column(",
            "dataTable",
            "name|number",
            "<'formatted'>"
        ],
        "description": "Returns a reference to the specified data table column. The keyword 'formatted' allows accessing formatted data, like the value label.",
        "prototype": "y = Column( name|number );\r\ny = Column( dataTable, name|number, <'formatted'> )"
    },
    "columndialog": {
        "args": [
            "<var",
            "=",
            "ColList('Label'",
            "<Min",
            "Col(min)>",
            "<Max",
            "Col(max)>",
            "<Width(w)>",
            "<Data",
            "Type('Numeric'|'Character'|'Any')>",
            "<Modeling",
            "Type({<'Continuous'>",
            "<'Nominal'>",
            "<'Ordinal'>",
            "<'None'>",
            "<'Multiple",
            "Response'>",
            "<'Unstructured",
            "Text'>",
            "<'Vector'>})>",
            ")>",
            "<var=EditText('string')>",
            "<var=EditNumber(num)>",
            "<var=Check",
            "Box(",
            "'Text'",
            "0|1)>",
            "<var=Combo",
            "Box('choice1'",
            "...)>",
            "<HList(box",
            "...)>",
            "<VList(box",
            "...)>",
            "<LineUp(ncol",
            "box",
            "...)>",
            "<Text",
            "Box('string'"
        ],
        "description": "Prompts the user with a modal window with fields to select columns of a data table. The specification can include several types of input boxes as well as container boxes to organize the window.",
        "prototype": "y = Column Dialog( <var = ColList('Label', <Min Col(min)>, <Max Col(max)>, <Width(w)>, <Data Type('Numeric'|'Character'|'Any')>, <Modeling Type({<'Continuous'>, <'Nominal'>, <'Ordinal'>, <'None'>, <'Multiple Response'>, <'Unstructured Text'>, <'Vector'>})> )>, <var=EditText('string')>, <var=EditNumber(num)>, <var=Check Box( 'Text', 0|1)>, <var=Combo Box('choice1', ...)>, <HList(box, ...)>, <VList(box, ...)>, <LineUp(ncol, box, ...)>, <Text Box('string')>"
    },
    "columnname": {
        "args": [
            "n"
        ],
        "description": "Returns the name of the nth column of the current data table.",
        "prototype": "name = Column Name( n )"
    },
    "combinestates": {
        "args": [
            "rs1",
            "..."
        ],
        "description": "Combines several row state values into one.",
        "prototype": "rs = Combine States( rs1, ... )"
    },
    "combobox": {
        "args": [
            "{item",
            "<(",
            "tipstr",
            ")>",
            "...}",
            "<script>"
        ],
        "description": "Returns a display box to show a combo box with a popup menu. Each item in the combo box can have an optional tooltip that is specified as a string inside of parentheses following the item text string.",
        "prototype": "y = Combo Box( {item <( tipstr )>, ...}, <script> )"
    },
    "concat": {
        "args": [
            "s1",
            "s2",
            "..."
        ],
        "description": "Concatenates strings into a longer string or matrices into a wider matrix.",
        "prototype": "s = s1 || s2 ...; m = m1 || m2 ...; s = Concat( s1, s2, ... )"
    },
    "concatitems": {
        "args": [
            "{list",
            "of",
            "strings}",
            "<separatorString>"
        ],
        "description": "Joins a list of strings into one long string, separating each from next with the separator, a blank if unspecified.",
        "prototype": "string = Concat Items( {list of strings}, <separatorString> )"
    },
    "concatto": {
        "args": [
            "a",
            "b"
        ],
        "description": "Concatenates in place. a ||= b is equivalent to a = a || b. This is an assignment operator.",
        "prototype": "string1 ||= string2; matrix1 ||= matrix2; Concat To( a, b )"
    },
    "constrainedmaximize": {
        "args": [
            "expr",
            "{x1(",
            "low1",
            "up1",
            ")",
            "x2(",
            "low2",
            "up2",
            ")",
            "...}",
            "<<LessThanEQ({mat_A",
            "vec_b})",
            "<<GreaterThanEQ({mat_A",
            "vec_b})",
            "<<EqualTo({mat_A",
            "vec_b})",
            "<<MaxIter(",
            "250",
            ")",
            "<<tolerance(",
            ".00001",
            ")",
            "<<ShowDetails(True)",
            "<<StartingValues([x1",
            "x2",
            "...",
            "]))",
            "<<SetVariableLimit({lowerLimitVector",
            "upperLimitVector}"
        ],
        "description": "Finds values for the function's arguments, given in the list {x1, x2, ...}, that maximize the expr expression with optional linear constraints. The variables, x1, x2, and so on, can be scalars or vectors. Lower and upper bounds must be specified for each variable in parentheses following the variable's name or with the optional parameter <<SetVariableLimits(). Optional arguments for the Constrained Maximize function enable you to specify the following: linear constraints, maximum number of iterations, desired tolerance, output details, starting values, and limits for the optimization variables. (See example 2.) Linear constraints are specified using the mat_A coefficient matrix and the vec_b right hand side vector.",
        "prototype": "Constrained Maximize( expr, {x1( low1, up1 ), x2( low2, up2 ), ...}, <<LessThanEQ({mat_A, vec_b}), <<GreaterThanEQ({mat_A, vec_b}), <<EqualTo({mat_A, vec_b}), <<MaxIter( 250 ), <<tolerance( .00001 ), <<ShowDetails(True), <<StartingValues([x1, x2, ... ])), <<SetVariableLimit({lowerLimitVector,upperLimitVector})"
    },
    "constrainedminimize": {
        "args": [
            "expr",
            "{x1(",
            "low1",
            "up1",
            ")",
            "x2(",
            "low2",
            "up2",
            ")",
            "...}",
            "<<LessThanEQ({mat_A",
            "vec_b})",
            "<<GreaterThanEQ({mat_A",
            "vec_b})",
            "<<EqualTo({mat_A",
            "vec_b})",
            "<<MaxIter(",
            "250",
            ")",
            "<<tolerance(",
            ".00001",
            ")",
            "<<ShowDetails(True)",
            "<<StartingValues([x1",
            "x2",
            "...",
            "]))",
            "<<SetVariableLimit({low",
            "high}"
        ],
        "description": "Finds values for the function's arguments, given in the list {x1, x2, ...}, that minimize the expr expression with optional linear constraints. The variables, x1, x2, and so on, can be scalars or vectors. Lower and upper bounds must be specified for each variable in parentheses following the variable's name or with the optional parameter <<SetVariableLimits(). Optional arguments for the Constrained Minimize function enable you to specify the following: linear constraints, maximum number of iterations, desired tolerance, output details, starting values, and limits for the optimization variables. (See example 2.) Linear constraints are specified using the mat_A coefficient matrix and the vec_b right hand side vector.",
        "prototype": "Constrained Minimize( expr, {x1( low1, up1 ), x2( low2, up2 ), ...}, <<LessThanEQ({mat_A, vec_b}), <<GreaterThanEQ({mat_A, vec_b}), <<EqualTo({mat_A, vec_b}), <<MaxIter( 250 ), <<tolerance( .00001 ), <<ShowDetails(True), <<StartingValues([x1, x2, ... ])), <<SetVariableLimit({low,high})"
    },
    "contains": {
        "args": [
            "x",
            "item",
            "<start=1>"
        ],
        "description": "Returns the position of item within x, starting at position start if provided. If start is negative, the search starts backward from length( x ) - start. The argument x can be a string or a list.",
        "prototype": "pos = Contains( x, item, <start=1> )"
    },
    "containsitem": {
        "args": [
            "x",
            "item",
            "|",
            "list",
            "|",
            "pattern",
            "<delimiter>"
        ],
        "description": "Returns a Boolean indicating whether the word [item], one of a list of words [list], or pattern [pattern] matches one of the words in the text represented by [x]. Words are delimited by the characters in the optional delimiter [delimiter] string. A comma, ',', character is the default delimiter. Blanks are trimmed from the ends of each extracted word from the input text string [x].",
        "prototype": "b = Contains Item( x, item | list | pattern, <delimiter> )"
    },
    "contextbox": {
        "args": [
            "displayBox",
            "..."
        ],
        "description": "Returns a display box that establishes a scoped evaluation context. Allows different parts of a display window to be executed independently of each other.",
        "prototype": "y = Context Box( displayBox, ... )"
    },
    "continue": {
        "args": [],
        "description": "Causes a continuation of next iteration of flow of control within a For or While loop.",
        "prototype": "Continue()"
    },
    "contour": {
        "args": [
            "xVector",
            "yVector",
            "zGridMatrix",
            "zContours",
            "<",
            "<<zColor(",
            "color",
            "option",
            ")>",
            "<",
            "<<Fill|Fill",
            "Between|Fill",
            "Below|Fill",
            "Above>",
            "<",
            "<<Transparency(vector)>"
        ],
        "description": "Draws contours given a grid of values.",
        "prototype": "Contour( xVector, yVector, zGridMatrix, zContours, < <<zColor( color, option )>, < <<Fill|Fill Between|Fill Below|Fill Above>, < <<Transparency(vector)> )"
    },
    "contourfunction": {
        "args": [
            "zExpr",
            "xName",
            "yName",
            "z|zMatrix",
            "<",
            "<<XGrid(",
            "min",
            "max",
            "incr",
            ")>",
            "<",
            "<<YGrid(",
            "min",
            "max",
            "incr",
            ")>",
            "<",
            "<<ZColor(",
            "color",
            "option",
            ")>",
            "<",
            "<<ZLabeled>",
            "<",
            "<<Filled>",
            "<",
            "<<FillBetween>",
            "<",
            "<<Ternary>",
            "<",
            "<<Transparency(",
            "t",
            ")>"
        ],
        "description": "Evaluates the expression on a grid of xName and yName values and draws the contour lines. The color can be specified as a number, a matrix, a list of RGB values, a list of color names, or a Color Theme.  The transparency t can be specified as a number or as a matrix.  If the Ternary option is specified, the contours are clipped to a ternary coordinate system.",
        "prototype": "Contour Function( zExpr, xName, yName, z|zMatrix, < <<XGrid( min, max, incr )>, < <<YGrid( min, max, incr )>, < <<ZColor( color, option )>, < <<ZLabeled>, < <<Filled>, < <<FillBetween>, < <<Ternary>, < <<Transparency( t )> )"
    },
    "contourseg": {
        "args": [
            "Triangulation",
            "[",
            "levels",
            "]",
            "<",
            "zColor([colors]",
            "<Cycle",
            "Colors|Interpolate",
            "Colors>)",
            ">",
            "<",
            "Transparency([]",
            "|",
            "t"
        ],
        "description": "Returns a display seg representing contours of a Triangulation.  Optional colors can be specified for each level as a matrix or list.  The transparency can be specified as a number or matrix.",
        "prototype": "me = Contour Seg( Triangulation, [ levels ], < zColor([colors], <Cycle Colors|Interpolate Colors>) >, < Transparency([] | t) >"
    },
    "convertfilepath": {
        "args": [
            "path",
            "<absolute|relative>",
            "<posix|windows>",
            "<base(",
            "path",
            ")>",
            "<search>"
        ],
        "description": "Returns the converted path.",
        "prototype": "path = Convert File Path( path, <absolute|relative>, <posix|windows>, <base( path )>, <search> )"
    },
    "copydirectory": {
        "args": [
            "from",
            "to",
            "<recursive(0|1)>"
        ],
        "description": "Copies files from one directory to another, optionally copying subdirectories. The directory name will be created at the to-path and should not be part of the to-path.",
        "prototype": "rc = Copy Directory( from, to, <recursive(0|1)> )"
    },
    "copyfile": {
        "args": [
            "from",
            "to"
        ],
        "description": "Copies a file from the original file to a new file with the same or a different name. Specify a complete path and filename for the destination.",
        "prototype": "rc = Copy File( from, to )"
    },
    "correlation": {
        "args": [
            "x",
            "<",
            "<<'Pairwise'",
            ">",
            "<",
            "<<'Shrink'",
            ">",
            "<",
            "<<Freq(vector)",
            ">",
            "<",
            "<<Weight(vector)",
            ">"
        ],
        "description": "Returns the correlation matrix of the matrix argument x. The 'Pairwise' argument handles missing values in pairwise rather than rowwise fashion. The 'Shrink' argument reduces the off-diagonal elements by a factor that is determined using the method described in Schafer and Strimmer, 2005. The Freq and Weight arguments specify vectors of frequency or weight values, respectively.",
        "prototype": "y = Correlation( x , < <<'Pairwise' >, < <<'Shrink' >, < <<Freq(vector) >, < <<Weight(vector) > )"
    },
    "cos": {
        "args": [
            "x"
        ],
        "description": "Returns the trigonometric cosine of x, where x is an angle in radians.",
        "prototype": "y = Cosine( x )"
    },
    "cosh": {
        "args": [
            "x"
        ],
        "description": "Returns the hyperbolic cosine of x.",
        "prototype": "y = CosH( x )"
    },
    "cosine": {
        "args": [
            "x"
        ],
        "description": "Returns the trigonometric cosine of x, where x is an angle in radians.",
        "prototype": "y = Cosine( x )"
    },
    "count": {
        "args": [
            "start",
            "end",
            "s",
            "<n=1>"
        ],
        "description": "Returns the ith value in the sequence of numbers from start to end taking s steps and repeating each number n times, where i is determined by the value of the Row() function. Being dependent on the Row() function, the Count() function is generally used in column formulas.",
        "prototype": "y = Count( start, end, s, <n=1> )"
    },
    "covariance": {
        "args": [
            "x",
            "<",
            "<<'Pairwise'",
            ">",
            "<",
            "<<'Shrink'",
            ">",
            "<",
            "<<Freq(vector)",
            ">",
            "<",
            "<<Weight(vector)",
            ">"
        ],
        "description": "Returns the covariance matrix of the matrix argument x. The 'Pairwise' argument handles missing values in pairwise rather than rowwise fashion. The 'Shrink' argument reduces the off-diagonal elements by a factor that is determined using the method described in Schafer and Strimmer, 2005. The Freq and Weight arguments specify vectors of frequency or weight values, respectively.",
        "prototype": "y = Covariance( x , < <<'Pairwise' >, < <<'Shrink' >, < <<Freq(vector) >, < <<Weight(vector) > )"
    },
    "createdatabaseconnection": {
        "args": [
            "dataSourceName|'Connect",
            "Dialog'",
            "<DriverPrompt(true|false)>"
        ],
        "description": "Creates a database connection and returns a handle to the connection. If DriverPrompt is true, the user will be prompted using the ODBC driver's prompt to supply credentials if necessary.",
        "prototype": "dbc = Create Database Connection( dataSourceName|'Connect Dialog', <DriverPrompt(true|false)> )"
    },
    "createdirectory": {
        "args": [
            "path"
        ],
        "description": "Creates a directory.",
        "prototype": "rc = Create Directory( path )"
    },
    "createexcelworkbook": {
        "args": [
            "<Workbook",
            "Name>",
            "<{List",
            "of",
            "open",
            "tables}>",
            "<Optional",
            "list",
            "of",
            "worksheet",
            "names>"
        ],
        "description": "Generate an Excel Workbook from open JMP Data Tables",
        "prototype": "Create Excel Workbook(<Workbook Name>, <{List of open tables}>, <Optional list of worksheet names> )"
    },
    "creationdate": {
        "args": [
            "path"
        ],
        "description": "Returns the creation date of a file or directory.",
        "prototype": "date = Creation Date( path )"
    },
    "cumulativesum": {
        "args": [
            "x"
        ],
        "description": "Returns a matrix of partial sums for the input matrix.",
        "prototype": "y = Cumulative Sum( x )"
    },
    "currentcasconnection": {
        "args": [],
        "description": "Obtains the connection to the current CAS Server.",
        "prototype": "Current CAS Connection()"
    },
    "currentdatatable": {
        "args": [
            "<Project(title|index|box|window)>",
            ");",
            "Current",
            "Data",
            "Table(",
            "dt"
        ],
        "description": "Returns the current data table or makes the specified data table current if there is one specified.\r\n\r\nTo specify a project, use the optional Project() argument with a title, index, display box, or window object. Use Project(0) to specify no project when running the script in a project.",
        "prototype": "dt = Current Data Table( <Project(title|index|box|window)> ); Current Data Table( dt )"
    },
    "currentjournal": {
        "args": [
            "<Project(title|index|box|window)>"
        ],
        "description": "Returns a reference to the current journal in the current project (or no project when not running the script in a project).\r\n\r\nTo specify a project, use the optional Project() argument with a title, index, display box, or window object. Use Project(0) to specify no project when running the script in a project.\r\n\r\nIf no current journal exists in the given project, one will be created automatically.",
        "prototype": "y = Current Journal( <Project(title|index|box|window)> )"
    },
    "currentmetadataconnection": {
        "args": [],
        "description": "Returns the active SAS metadata server connection, if any, as a scriptable object.",
        "prototype": "metadata = Current Metadata Connection()"
    },
    "currentreport": {
        "args": [
            "<Project(title|index|box|window)>"
        ],
        "description": "Returns a display box reference to the current report in the current project (or no project when not running the script in a project).\r\n\r\nTo specify a project, use the optional Project() argument with a title, index, display box, or window object. Use Project(0) to specify no project when running the script in a project.",
        "prototype": "y = Current Report( <Project(title|index|box|window)> )"
    },
    "currentsasconnection": {
        "args": [],
        "description": "Returns the active SAS server connection, if any, as a scriptable object.",
        "prototype": "sas = Current SAS Connection()"
    },
    "currentwindow": {
        "args": [
            "<Project(title|index|box|window)>"
        ],
        "description": "Returns a reference to the current window in the current project (or no project when not running the script in a project).\r\n\r\nTo specify a project, use the optional Project() argument with a title, index, display box, or window object. Use Project(0) to specify no project when running the script in a project.",
        "prototype": "y = Current Window( <Project(title|index|box|window)> )"
    },
    "cytometrylogicle": {
        "args": [
            "x",
            "T",
            "W",
            "M",
            "A"
        ],
        "description": "Compute cytometry logicle transformation.",
        "prototype": "y = Cytometry Logicle( x, T, W, M, A )"
    },
    "cytometrylogicleinverse": {
        "args": [
            "y",
            "T",
            "W",
            "M",
            "A"
        ],
        "description": "Compute inverse cytometry logicle transformation.",
        "prototype": "x = Cytometry Logicle Inverse( y, T, W, M, A )"
    },
    "datafeed": {
        "args": [
            "..."
        ],
        "description": "Creates an object and window you can send messages to manage real-time data feeds.",
        "prototype": "y = Open Datafeed( ... )"
    },
    "datafiltercontextbox": {
        "args": [
            "displayBox"
        ],
        "description": "Returns a display box that defines the extent of the local data filters contained in a display tree. Data filters and Data Filter Context Boxes can be arranged in a hierarchy and will be shared among platforms or boxes contained within the Data Filter Context Boxes.",
        "prototype": "y = Data Filter Context Box( displayBox )"
    },
    "datafiltersourcebox": {
        "args": [
            "displayBox"
        ],
        "description": "Returns a display box that defines the source of a selection filter. Selected rows in reports contained by the Data Filter Source Box will be included for analysis in the other reports contained within a common Data Filter Context Box.",
        "prototype": "y = Data Filter Source Box( displayBox )"
    },
    "datagridbox": {
        "args": [],
        "description": "Returns a display box that can hold a data table.",
        "prototype": "y = Data Grid Box(  )"
    },
    "datatable": {
        "args": [
            "name|number"
        ],
        "description": "Returns a reference to the specified data table.",
        "prototype": "dt = Data Table( name|number )"
    },
    "datatablebox": {
        "args": [
            "datatable"
        ],
        "description": "Returns a table box representing the given data table.",
        "prototype": "y = Data Table Box( datatable )"
    },
    "datatablecolbox": {
        "args": [
            "col"
        ],
        "description": "Returns a column box corresponding to the given data table column.",
        "prototype": "y = Data Table Col Box( col )"
    },
    "datedifference": {
        "args": [
            "dt1",
            "dt2",
            "intervalName",
            "<alignment='start'>"
        ],
        "description": "Returns the difference in intervals of two date/time values. Supported values of intervalName are 'Year', 'Quarter', 'Month', 'Week', 'Day', 'Hour', 'Minute', and 'Second'. An alignment of 'start' includes full or partial intervals, while 'actual' only includes full intervals. An alignment of 'fractional' returns fractional differences, using averages for the duration of 'Year', 'Quarter', and 'Month' intervals.",
        "prototype": "delta = Date Difference( dt1, dt2, intervalName, <alignment='start'> )"
    },
    "datedmy": {
        "args": [
            "d",
            "m",
            "y"
        ],
        "description": "Converts day, month, and year into a JMP date-time value, which is the number of seconds since 01Jan1904.",
        "prototype": "z = Date DMY( d, m, y )"
    },
    "dateincrement": {
        "args": [
            "datetime",
            "intervalName",
            "<incr=1>",
            "<alignment='start'>"
        ],
        "description": "Returns a new date/time value by adding incr number of intervals. Supported values of intervalName are 'Year', 'Quarter', 'Month', 'Week', 'Day', 'Hour', 'Minute', and 'Second'. An alignment of 'start' truncates to the nearest interval prior to adding the increment, while 'actual' retains the full input date/time. An alignment of 'fractional' allows fractional incr values, using averages for the duration of 'Year', 'Quarter', and 'Month' intervals.",
        "prototype": "d = Date Increment( datetime, intervalName, <incr=1>, <alignment='start'> )"
    },
    "datemdy": {
        "args": [
            "m",
            "d",
            "y"
        ],
        "description": "Converts month, day, and year into a JMP date value, which is the number of seconds since 01Jan1904.",
        "prototype": "z = Date MDY( m, d, y )"
    },
    "day": {
        "args": [
            "datetime"
        ],
        "description": "Returns the day of month part of a date-time value, 1 - 31.",
        "prototype": "d = Day( datetime )"
    },
    "dayofweek": {
        "args": [
            "datetime"
        ],
        "description": "Returns the day of the week of a date-time value. Sunday = 1, ..., Saturday = 7.",
        "prototype": "d = Day Of Week( datetime )"
    },
    "dayofyear": {
        "args": [
            "datetime"
        ],
        "description": "Returns the day of the year of a date-time value. January 1 is 1.",
        "prototype": "d = Day Of Year( datetime )"
    },
    "daysinmonth": {
        "args": [
            "year",
            "month"
        ],
        "description": "Return the number of days in a given month.",
        "prototype": "v = Days In Month(year, month)"
    },
    "debugbreak": {
        "args": [],
        "description": "When this expression is evaluated within the JSL Debugger, the Debugger stops executing the script.",
        "prototype": "Debug Break()"
    },
    "decode64blob": {
        "args": [
            "base64String"
        ],
        "description": "Decodes a printable string of base 64 text into a blob.",
        "prototype": "y = Decode64 Blob( base64String )"
    },
    "decode64double": {
        "args": [
            "base64String"
        ],
        "description": "Returns the double-precision floating point number from the Base64 encoded string.",
        "prototype": "y = Decode64 Double( base64String )"
    },
    "decodeuri": {
        "args": [
            "value"
        ],
        "description": "Encode the string using URI encoding",
        "prototype": "Decode URI( value )"
    },
    "defineclass": {
        "args": [
            "'class",
            "name'",
            "<",
            "Base",
            "Class{",
            "'base",
            "class",
            "name'+",
            ">",
            "<",
            "Show(",
            "All(",
            "boolean",
            ")",
            "|",
            "(",
            "Members(",
            "boolean",
            ")",
            "|",
            "Methods(",
            "boolean",
            ")",
            "|",
            "Functions(",
            "boolean",
            ")",
            ")+",
            ")>",
            "{",
            "method*",
            "|",
            "member*",
            "|",
            "function*",
            "}"
        ],
        "description": "Define a New Class",
        "prototype": "Define Class('class name' <, Base Class{ 'base class name'+ > <, Show( All( boolean ) | ( Members( boolean ) | Methods( boolean ) | Functions( boolean ) )+ )>, { method* | member* | function* } )"
    },
    "deleteclasses": {
        "args": [
            "<",
            "Force(",
            "boolean",
            ")",
            ">",
            "<",
            "<class",
            "reference>",
            "...",
            ">"
        ],
        "description": "Deletes all class definitions or one or more specific class definitions.",
        "prototype": "Delete Classes( < Force( boolean ), > < <class reference>, ... > )"
    },
    "deletedirectory": {
        "args": [
            "path",
            "<Allow",
            "Undo(",
            "boolean",
            ")>"
        ],
        "description": "Deletes a directory and its files and subdirectories.",
        "prototype": "rc = Delete Directory( path, <Allow Undo( boolean )> )"
    },
    "deletefile": {
        "args": [
            "path",
            "<Allow",
            "Undo(",
            "boolean",
            ")>"
        ],
        "description": "Deletes a file.",
        "prototype": "rc = Delete File( path, <Allow Undo( boolean )> )"
    },
    "deleteglobals": {
        "args": [
            "<",
            "varname",
            "...",
            ">"
        ],
        "description": "Deletes all the currently defined global symbols and their values.",
        "prototype": "Delete Globals( < varname, ... > )"
    },
    "deletejmplivebookmark": {
        "args": [
            "name"
        ],
        "description": "Delete a bookmark location used for publishing web reports. Returns 1 on success, 0 otherwise.",
        "prototype": "Delete JMP Live Bookmark(name)"
    },
    "deletejmpliveconnection": {
        "args": [
            "URL(url)",
            "Username(user)"
        ],
        "description": "Deletes a saved JMP Live connection information from disk.",
        "prototype": "Delete JMP Live Connection( URL(url), Username(user) )"
    },
    "deletenamespaces": {
        "args": [
            "<",
            "Force(",
            "boolean",
            ")",
            ">",
            "<",
            "<namespace",
            "reference>",
            "...",
            ">"
        ],
        "description": "Deletes all namespaces or one or more specific namespaces.",
        "prototype": "Delete Namespaces( < Force( boolean ), > < <namespace reference>, ... > )"
    },
    "deletesymbols": {
        "args": [
            "<",
            "varname",
            "...",
            ">"
        ],
        "description": "Deletes all the currently defined symbols and their values.",
        "prototype": "Delete Symbols( < varname, ... > )"
    },
    "derivative": {
        "args": [
            "expr",
            "name"
        ],
        "description": "Returns the symbolic derivative for the given expression with respect to the specified variable name.",
        "prototype": "y = Derivative( expr, name )"
    },
    "design": {
        "args": [
            "v",
            "<",
            "levelsList|<<Levels",
            "<<ElseMissing",
            ">"
        ],
        "description": "Creates a design matrix that contains a column of 1s and 0s for each unique value of the argument. Use the levelsList argument to specify a list of the levels for the design matrix. If the <<Levels argument is specified, the return value is a list that contains the design matrix and a list of the levels. If the <<ElseMissing argument is specified, missing values are placed in the design matrix for values in the v argument that do not appear in the levelsList. Otherwise, 0s are placed in the design matrix.",
        "prototype": "y = Design( v, < levelsList|<<Levels, <<ElseMissing > )"
    },
    "designf": {
        "args": [
            "v",
            "<",
            "levelsList|<<Levels",
            "<<ElseMissing",
            ">"
        ],
        "description": "Creates a design matrix that contains a column of 1s and 0s for all but the last of the unique values of the argument. The last level is coded as a row of -1s. If the levelsList argument is specified, the last level is the last level in levelsList. Otherwise, the last level is defined as the largest value in v. If the <<Levels argument is specified, the return value is a list that contains the design matrix and a list of the levels. If the <<ElseMissing argument is specified, missing values are placed in the design matrix for values in the v argument that do not appear in the levelsList. Otherwise, 0s are placed in the design matrix.",
        "prototype": "y = DesignF( v, < levelsList|<<Levels, <<ElseMissing > )"
    },
    "designlast": {
        "args": [
            "v",
            "<",
            "levelsList",
            "<<ElseMissing",
            ">"
        ],
        "description": "Creates a design matrix that contains a column of 1s and 0s for all but the last of the unique values of the argument. The last level is coded as a row of 0s. If the levelsList argument is specified, the last level is the last level in levelsList. Otherwise, the last level is defined as the largest value in v. If the <<Levels argument is specified, the return value is a list that contains the design matrix and a list of the levels. If the <<ElseMissing argument is specified, missing values are placed in the design matrix for values in the v argument that do not appear in the levelsList. Otherwise, 0s are placed in the design matrix.",
        "prototype": "y = Design Last( v, < levelsList, <<ElseMissing > )"
    },
    "designnom": {
        "args": [
            "v",
            "<",
            "levelsList|<<Levels",
            "<<ElseMissing",
            ">"
        ],
        "description": "Creates a design matrix that contains a column of 1s and 0s for all but the last of the unique values of the argument. The last level is coded as a row of -1s. If the levelsList argument is specified, the last level is the last level in levelsList. Otherwise, the last level is defined as the largest value in v. If the <<Levels argument is specified, the return value is a list that contains the design matrix and a list of the levels. If the <<ElseMissing argument is specified, missing values are placed in the design matrix for values in the v argument that do not appear in the levelsList. Otherwise, 0s are placed in the design matrix.",
        "prototype": "y = Design Nom( v, < levelsList|<<Levels, <<ElseMissing > )"
    },
    "designord": {
        "args": [
            "v",
            "<",
            "levelsList|<<Levels",
            "<<ElseMissing",
            ">"
        ],
        "description": "Creates a design matrix that contains a column for all but the last of the unique values of the argument. The first level is coded as a row of 0s. Each subsequent (nth) level in the levelsList argument is coded as a row of (n-1) 1s and the rest 0s. If the <<Levels argument is specified, the return value is a list that contains the design matrix and a list of the levels. If the <<ElseMissing argument is specified, missing values are placed in the design matrix for values in the v argument that do not appear in the levelsList. Otherwise, 0s are placed in the design matrix.",
        "prototype": "y = Design Ord( v, < levelsList|<<Levels, <<ElseMissing > )"
    },
    "desirability": {
        "args": [
            "yVector",
            "dVector",
            "y"
        ],
        "description": "Returns a desirability curve, where yVector is a vector of 3 input values, dVector is the corresponding 3 desirability values, and y is the argument of which to calculate the desirability.",
        "prototype": "des = Desirability( yVector, dVector, y )"
    },
    "det": {
        "args": [
            "x"
        ],
        "description": "Returns the determinant of a square matrix.",
        "prototype": "y = Det( x )"
    },
    "diag": {
        "args": [
            "matrix",
            ");",
            "y",
            "=",
            "Diag(",
            "vector",
            ");",
            "y",
            "=",
            "Diag(",
            "matrix1",
            "matrix"
        ],
        "description": "Constructs a diagonal matrix from either a matrix or a vector. If two arguments are specified, the function returns the concatenation of the matrices diagonally.",
        "prototype": "y = Diag( matrix ); y = Diag( vector ); y = Diag( matrix1, matrix )"
    },
    "dialog": {
        "args": [
            "specification"
        ],
        "description": "Prompts the user with a modal window. This function is deprecated. Please use the New Window function with the <<Modal argument.",
        "prototype": "y = Dialog( specification )"
    },
    "dif": {
        "args": [
            "x",
            "<n=1>"
        ],
        "description": "Returns x - Lag( x, n ), also known as the 'first difference'. Being dependent on Row(), Dif() is mainly useful in column formulas.",
        "prototype": "y = Dif( x, <n=1> )"
    },
    "digamma": {
        "args": [
            "x"
        ],
        "description": "Returns the digamma function evaluated at x, where the digamma function is the derivative of the logarithm of the gamma function.",
        "prototype": "y = Digamma( x )"
    },
    "dim": {
        "args": [
            ");",
            "y",
            "=",
            "Dim(",
            "dt",
            ");",
            "y",
            "=",
            "Dim(",
            "matrix"
        ],
        "description": "Returns a row vector with the dimensions of the current data table, a specified data table, or a matrix. The dimensions are the number of rows and the number of columns and are listed in that order.",
        "prototype": "y = Dim(); y = Dim( dt ); y = Dim( matrix )"
    },
    "directoryexists": {
        "args": [
            "path"
        ],
        "description": "Tests whether the directory exists.",
        "prototype": "rc = Directory Exists( path )"
    },
    "directproduct": {
        "args": [
            "A",
            "B"
        ],
        "description": "Returns the direct or Kronecker product. Result has A[i,j]*B, expanding to all possible products.",
        "prototype": "y = Direct Product( A, B )"
    },
    "disablejmpliveurl": {
        "args": [
            "url"
        ],
        "description": "Disables a JMP Live URL. This method is available only during jmpStartAdmin.jsl. An asterisk * can be used a wildcard to specify URLs as * (any URL), *.jmp.com (a URL ending in .jmp.com), http://public.* (a URL starting with http://public.), or *public* (a URL that contains public).",
        "prototype": "Disable JMP Live URL(url)"
    },
    "disableproxysettings": {
        "args": [
            "1|0"
        ],
        "description": "Disables or enables proxy settings during jmpStartAdmin.jsl execution. Proxy settings are enabled by default.",
        "prototype": "Disable Proxy Settings( 1|0 )"
    },
    "distance": {
        "args": [
            "x1",
            "x2",
            "<scales>",
            "<powers>"
        ],
        "description": "Produces a matrix of distances between rows of x1 and rows of x2. To customize the scaling and powers for each column, specify the extra arguments scale and powers. For Kriging, Exp(-distance(x1,x2)) is used.",
        "prototype": "y = Distance( x1, x2, <scales>, <powers> )"
    },
    "divide": {
        "args": [
            "x0",
            "x1",
            "..."
        ],
        "description": "Divides all subsequent arguments from the first argument. Arguments can numbers, matrices, or lists of numbers.",
        "prototype": "y = x0 / x1; y = Divide( x0, x1, ... )"
    },
    "divideto": {
        "args": [
            "y",
            "x"
        ],
        "description": "Divides a value into a variable or into a list of variables.",
        "prototype": "y /= x; Divide To( y, x )"
    },
    "doubledecliningbalance": {
        "args": [
            "cost",
            "salvage",
            "life",
            "period",
            "<factor=2>"
        ],
        "description": "Returns the depreciation of an asset for a specified period using the double-declining balance method or some other depreciation factor. Equivalent to the DDB function in Microsoft Excel.",
        "prototype": "x = Double Declining Balance( cost, salvage, life, period, <factor=2> )"
    },
    "dragline": {
        "args": [
            "xMatrixName",
            "yMatrixName",
            "<dragScript>",
            "<MouseUpScript>"
        ],
        "description": "Draws a polyline at the indicated points. Unlike Line though, the points can be dragged across the screen, updating the values in the (LValue) matrix arguments.",
        "prototype": "Drag Line( xMatrixName, yMatrixName, <dragScript>, <MouseUpScript> )"
    },
    "dragmarker": {
        "args": [
            "xMatrixName",
            "yMatrixName",
            "<dragScript>",
            "<MouseUpScript>"
        ],
        "description": "Draws movable markers at the indicated points. The matrix values are updated as the markers are moved.",
        "prototype": "Drag Marker( xMatrixName, yMatrixName, <dragScript>, <MouseUpScript> )"
    },
    "dragpolygon": {
        "args": [
            "xMatrixName",
            "yMatrixName",
            "<dragScript>",
            "<MouseUpScript>"
        ],
        "description": "Draws a filled polygon at the indicated points. The points can be dragged across the screen, updating the values in the (LValue) matrix arguments.",
        "prototype": "Drag Polygon( xMatrixName, yMatrixName, <dragScript>, <MouseUpScript> )"
    },
    "dragrect": {
        "args": [
            "xMatrixName",
            "yMatrixName",
            "<dragScript>",
            "<MouseUpScript>"
        ],
        "description": "Draws a rectangle at the indicated points. Unlike Rect though, these corners can be dragged across the screen, updating the values in the (LValue) matrix arguments.",
        "prototype": "Drag Rect( xMatrixName, yMatrixName, <dragScript>, <MouseUpScript> )"
    },
    "dragtext": {
        "args": [
            "xMatrixName",
            "yMatrixName",
            "text",
            "<dragScript>",
            "<MouseUpScript>"
        ],
        "description": "Draws the text at the indicated points. Unlike the Text() function, however, the points can be dragged across the screen, updating the values in the xMatrixName and yMatrixName matrix arguments. The text argument can be a string argument or a list of strings.",
        "prototype": "Drag Text( xMatrixName, yMatrixName, text, <dragScript>, <MouseUpScript> )"
    },
    "dunnettpvalue": {
        "args": [
            "q",
            "nTrt",
            "dfe",
            "<lambdaVec",
            "=",
            ".>"
        ],
        "description": "Returns the p-value from Dunnett's multiple comparisons test, where q is the test statistic, nTrt is the number of treatments being compared to the control group, dfe is the error degrees of freedom (based on the total study sample), and the optional lambdaVec is a vector of parameters, which by default are set to 1/sqrt(2).",
        "prototype": "p = Dunnett P value( q, nTrt, dfe, <lambdaVec = .> )"
    },
    "dunnettquantile": {
        "args": [
            "1-alpha",
            "nTrt",
            "dfe",
            "<lambdaVec",
            "=",
            ".>"
        ],
        "description": "Returns the quantile needed in Dunnett's multiple comparisons test, where 1-alpha is the confidence level, nTrt is the number of treatments being compared to the control group, dfe is the error degrees of freedom (based on the total study sample), and the optional lambdaVec is a vector of parameters, which by default are set to 1/sqrt(2).",
        "prototype": "q = Dunnett Quantile( 1-alpha, nTrt, dfe, <lambdaVec = .> )"
    },
    "e": {
        "args": [],
        "description": "Returns the mathematical constant e, accurate to approximately 15 decimal digits: 2.7182818....",
        "prototype": "y = e()"
    },
    "ediv": {
        "args": [
            "A",
            "B"
        ],
        "description": "Returns an element-wise division of matrices.",
        "prototype": "y = A :/ B; y = E Div( A, B )"
    },
    "eigen": {
        "args": [
            "X"
        ],
        "description": "Performs eigenvalue decomposition of symmetric matrix X. Returns list {M, E} such that E*Diag(M)*E` = X.",
        "prototype": "{M, E} = Eigen( X )"
    },
    "empty": {
        "args": [],
        "description": "Returns an empty value. Used in formula editor for unspecified arguments.",
        "prototype": "y = Empty()"
    },
    "emult": {
        "args": [
            "A",
            "B"
        ],
        "description": "Returns an element-wise multiplication of matrices.",
        "prototype": "y = A :* B; y = E Mult( A, B )"
    },
    "enablejmpliveurl": {
        "args": [
            "url"
        ],
        "description": "Enables a JMP Live URL. This method is available only during jmpStartAdmin.jsl. An asterisk * can be used a wildcard to specify URLs as * (any URL), *.jmp.com (a URL ending in .jmp.com), http://public.* (a URL starting with http://public.), or *public* (a URL that contains public).",
        "prototype": "Enable JMP Live URL(url)"
    },
    "enableproxysettings": {
        "args": [
            "1|0"
        ],
        "description": "Enables or disables proxy settings during jmpStartAdmin.jsl execution. Proxy settings are enabled by default.",
        "prototype": "Enable Proxy Settings( 1|0 )"
    },
    "encode64blob": {
        "args": [
            "x"
        ],
        "description": "Encodes a blob into a printable string of base 64 text.",
        "prototype": "s = Encode64 Blob( x )"
    },
    "encode64double": {
        "args": [
            "x"
        ],
        "description": "Returns a Base64 string encoding of the floating point number.",
        "prototype": "s = Encode64 Double( x )"
    },
    "encodeuri": {
        "args": [
            "value"
        ],
        "description": "Encode the string using URI encoding",
        "prototype": "Encode URI( value )"
    },
    "endswith": {
        "args": [
            "s",
            "sub"
        ],
        "description": "Returns 1 if s ends with sub, otherwise returns 0. The s and sub arguments can be both strings or both lists. Equivalent to Right( s, Length( sub )) == sub.",
        "prototype": "b = Ends With( s, sub )"
    },
    "equal": {
        "args": [
            "x",
            "y",
            "..."
        ],
        "description": "Returns 1 if each argument is equal to the next argument; returns 0 otherwise.",
        "prototype": "z = x == y == ...; z = Equal( x, y, ... )"
    },
    "estimatefactorscore": {
        "args": [
            "dataRow",
            "modImpVarCov",
            "mvMeanVec",
            "lvMeanVec"
        ],
        "description": "Estimates factor scores from a structural equation model (SEM). The input arguments are a row vector of data, a model-implied variance-covariance matrix, a vector of model-implied manifest variable means, and a vector of model-implied latent variable means. It returns a row vector with estimated factor scores based on the SEM.",
        "prototype": "{factorScores} = Estimate Factor Score( dataRow , modImpVarCov, mvMeanVec, lvMeanVec )"
    },
    "eval": {
        "args": [
            "x"
        ],
        "description": "Evaluates the argument and returns the result.",
        "prototype": "y = Eval( x )"
    },
    "evalexpr": {
        "args": [
            "x"
        ],
        "description": "Returns a copy of expression x with each Expr() clause within x replaced with its evaluated value.",
        "prototype": "y = Eval Expr( x )"
    },
    "evalinsert": {
        "args": [
            "string",
            "<startChar='^'>",
            "<endChar=startChar>"
        ],
        "description": "Looks for substrings enclosed by the startChar/endChar pair and replaces them with the evaluated expression within.",
        "prototype": "y = Eval Insert( string, <startChar='^'>, <endChar=startChar> )"
    },
    "evalinsertinto": {
        "args": [
            "l_string",
            "<startChar='^'>",
            "<endChar=startChar>"
        ],
        "description": "Looks for substrings enclosed by the startChar/endChar pair and replaces them with the evaluated expression within, replacing l_string.",
        "prototype": "Eval Insert Into( l_string, <startChar='^'>, <endChar=startChar> )"
    },
    "evallist": {
        "args": [
            "list"
        ],
        "description": "Returns a list where every item in the list has been evaluated.",
        "prototype": "y = Eval List( list )"
    },
    "excerptbox": {
        "args": [
            "rptnum",
            "lstSubscripts"
        ],
        "description": "Returns a display box containing the excerpt designated by the report held at number rptnum and the list of display subscripts lstSubscripts. The subscripts reflect the current state of the report, after previous excerpts have been removed.",
        "prototype": "y = Excerpt Box( rptnum, lstSubscripts )"
    },
    "excluded": {
        "args": [
            "rs",
            ");",
            "Excluded(",
            "<Row",
            "State(",
            "<r>",
            ")>"
        ],
        "description": "Returns the excluded component of the specified row state value, 0 or 1. If the Excluded() function is used as an L-value, it changes the excluded state of the current (or rth) row in the current data table.",
        "prototype": "y = Excluded( rs ); Excluded( <Row State( <r> )> ) = y"
    },
    "excludedstate": {
        "args": [
            "x"
        ],
        "description": "Returns a row state value with the excluded component set to the specified value.",
        "prototype": "rs = Excluded State( x )"
    },
    "executesql": {
        "args": [
            "databaseConnectionHandle",
            "'SELECT",
            "...'|'SQLFILE=...'|invisible|tableName",
            "outputTableName"
        ],
        "description": "Executes SQL against a database connection returned from Create Database Connection",
        "prototype": "dt = Execute SQL(databaseConnectionHandle,  'SELECT ...'|'SQLFILE=...'|invisible|tableName, outputTableName )"
    },
    "exit": {
        "args": [
            "<'No",
            "Save'>);",
            "Exit(<'No",
            "Save'>"
        ],
        "description": "Exits JMP.",
        "prototype": "Quit(<'No Save'>); Exit(<'No Save'>)"
    },
    "exp": {
        "args": [
            "<x=1>"
        ],
        "description": "Returns e raised to the x power. Argument can be a number, matrix, or list of numbers.",
        "prototype": "y = Exp( <x=1> )"
    },
    "expdensity": {
        "args": [
            "x",
            "<theta=1>"
        ],
        "description": "Returns the density at x of an exponential distribution with parameter theta.",
        "prototype": "y = Exp Density( x, <theta=1> )"
    },
    "expdistribution": {
        "args": [
            "x",
            "<theta=1>"
        ],
        "description": "Returns the probability that an exponentially distributed random variable is less than x.",
        "prototype": "p = Exp Distribution( x, <theta=1> )"
    },
    "expm1": {
        "args": [
            "x"
        ],
        "description": "Returns a more accurate calculation of Exp(x)-1 when x is very small.",
        "prototype": "y = ExpM1( x )"
    },
    "expquantile": {
        "args": [
            "p",
            "<theta=1>"
        ],
        "description": "Returns the quantile from an exponential distribution, the value for which the probability is p that a random value would be lower.",
        "prototype": "q = Exp Quantile( p, <theta=1> )"
    },
    "expr": {
        "args": [
            "x"
        ],
        "description": "Returns its argument unevaluated. Used to quote expressions.",
        "prototype": "y = Expr( x )"
    },
    "expraspicture": {
        "args": [
            "expr(",
            "...",
            ")",
            "<width",
            "in",
            "pixels>"
        ],
        "description": "Returns a display box containing the specified expression as a formula picture.",
        "prototype": "y = Expr As Picture( expr( ... ), <width in pixels> )"
    },
    "extractexpr": {
        "args": [
            "expr",
            "pattern"
        ],
        "description": "Returns a subexpression matching the specified pattern.",
        "prototype": "y = Extract Expr( expr, pattern )"
    },
    "factorial": {
        "args": [
            "x"
        ],
        "description": "Returns the factorial of x, which is the same as Gamma( x + 1 ). If x is an integer, the result is the product 1 * 2 * ... * x.",
        "prototype": "y = Factorial( x )"
    },
    "faurequasirandomsequence": {
        "args": [
            "nDim",
            "nRow"
        ],
        "description": "Generate a sequence of space filling quasi-random numbers using the Faure sequence.",
        "prototype": "points = Faure Quasi Random Sequence(nDim, nRow)"
    },
    "fdensity": {
        "args": [
            "q",
            "dfnum",
            "dfden",
            "<nonCentrality=0>"
        ],
        "description": "Returns the density at q of an F distribution with dfn and dfd degrees of freedom.",
        "prototype": "y = F Density( q, dfnum, dfden, <nonCentrality=0> )"
    },
    "fdistribution": {
        "args": [
            "q",
            "dfnum",
            "dfden",
            "<nonCentrality=0>"
        ],
        "description": "Returns the probability that an F distributed random variable is less than q.",
        "prototype": "y = F Distribution( q, dfnum, dfden, <nonCentrality=0> )"
    },
    "fft": {
        "args": [
            "L",
            "<<inverse(",
            "0",
            ")",
            "<<multivariate(",
            "0",
            ")",
            "<<scale(",
            "1.0",
            ")"
        ],
        "description": "Conducts Fast Fourier Transformation (FFT) on argument L, a required list consisting of real and imaginary parts of the data in matrix forms. If L consists of only one matrix, the matrix is considered to be the real part. If L consists of two matrices, the first is the real part, and the second is the imaginary part. The two matrices must have the same dimensions and must have more than one row. There are three optional arguments. The inverse argument determines whether to conduct inverse FFT. The multivariate argument determines whether to conduct spatial or multivariate FFT. The scale argument determines the constant by which the return values should be multiplied. Return value is a list of two matrices with the same dimensions as the first input argument.",
        "prototype": "ret = FFT( L, <<inverse( 0 ), <<multivariate( 0 ), <<scale( 1.0 ) )"
    },
    "fileexists": {
        "args": [
            "path"
        ],
        "description": "Tests whether the file exists.",
        "prototype": "rc = File Exists( path )"
    },
    "filesindirectory": {
        "args": [
            "'path'",
            "<recursive(0|1)>"
        ],
        "description": "Returns the list of filenames in a directory specified by path. If Recursive is not specified, directory names are included in the list.",
        "prototype": "y = Files In Directory( 'path', <recursive(0|1)> )"
    },
    "filesize": {
        "args": [
            "path"
        ],
        "description": "Determine the size of the file with given path",
        "prototype": "size = File Size( path )"
    },
    "fillcolor": {
        "args": [
            "<name|index|rgbList>"
        ],
        "description": "Sets the color for drawing filled areas.",
        "prototype": "Fill Color( <name|index|rgbList> )"
    },
    "fillpattern": {
        "args": [
            "name|mask|image"
        ],
        "description": "Sets the pattern for drawing filled areas. A mask is a matrix of values between 0 and 1 to be applied to the current fill color.",
        "prototype": "Fill Pattern( name|mask|image )"
    },
    "filtercolselector": {
        "args": [
            "<Data",
            "Table(name)>",
            "<width(pixels)>",
            "<nlines(n)>",
            "<script>",
            "<onchange(expr)>"
        ],
        "description": "Returns a display box that contains a list of items. Control allows column filtering.",
        "prototype": "y = Filter Col Selector(<Data Table(name)>, <width(pixels)>, <nlines(n)>, <script>, <onchange(expr)>)"
    },
    "findall": {
        "args": [
            "<Project(title|index|box|window)",
            ">",
            "Data",
            "Tables",
            "|",
            "Reports",
            "|",
            "Journals",
            "<invisible",
            "|",
            "private>"
        ],
        "description": "Finds all open resources of a specific type: data tables, journals, or reports.\r\n\r\nOnly windows in the current project (or no project if not running the script in a project) will be included. To specify a project, use the optional Project() argument with a title, index, display box, or window object. Use Project(0) to specify no project when running the script in a project.",
        "prototype": "Find All( <Project(title|index|box|window),> Data Tables | Reports | Journals, <invisible | private> )"
    },
    "first": {
        "args": [
            "x1",
            "x2",
            "..."
        ],
        "description": "Evaluates each argument and returns the value of the first argument.",
        "prototype": "y = First( x1, x2, ... )"
    },
    "fitcensored": {
        "args": [
            "Distribution(name)",
            "YLow(vector)",
            "|",
            "Y(vector)",
            "<YHigh(vector)>",
            "<Weight(vector)>",
            "<X(matrix)>",
            "<Z(matrix)>",
            "<HoldParm(vector)>",
            "<Use",
            "random",
            "sample",
            "to",
            "compute",
            "initial",
            "values(percent)>",
            "<Use",
            "first",
            "N",
            "observations",
            "to",
            "compute",
            "initial",
            "values(nobs)>"
        ],
        "description": "Fits a distribution using censored data. The required arguments are Distribution and either YLow or Y. The function returns a list that contains parameter estimates, covariance matrix, log-likelihood, AICc, BIC, and a convergence message. The X and Z arguments specify regression design matrices for location and scale, respectively. When the data vector is large, two optional arguments can be used to specify a sample to compute the initial values. You can specify a percent of the observations or the first nobs observations, but the total sample size must be greater than 100.",
        "prototype": "result = FitCensored( Distribution(name), YLow(vector) | Y(vector), <YHigh(vector)>, <Weight(vector)>, <X(matrix)>, <Z(matrix)>, <HoldParm(vector)>, <Use random sample to compute initial values(percent)>, <Use first N observations to compute initial values(nobs)> )"
    },
    "fitcircle": {
        "args": [
            "Xvec",
            "Yvec"
        ],
        "description": "Fits the circle that best goes through three or more points that are defined by two vectors of coordinates. The result is a list that contains the X and Y coordinates of the center point of the circle, the length of the radius, and the sum of squared errors.",
        "prototype": "{xCenter, yCenter, radius, sse} = Fit Circle( Xvec, Yvec )"
    },
    "fittransformtonormal": {
        "args": [
            "Distribution(name)",
            "Y(vector)",
            "<Freq(vector)>"
        ],
        "description": "Fits a transformation to normality for a vector of data. This includes the Johnson Sl, Johnson Sb, Johnson Su, and GLog distributions. The function returns a list containing parameter estimates, covariance matrix, log-likelihood, AICc, a convergence message, and transformed values.",
        "prototype": "result = Fit Transform To Normal( Distribution(name), Y(vector), <Freq(vector)> )"
    },
    "flogcdistribution": {
        "args": [
            "x",
            "dfnum",
            "dfden",
            "<nonCentrality=0>"
        ],
        "description": "Returns the log of 1 - F Distribution.",
        "prototype": "y = F Log CDistribution( x, dfnum, dfden, <nonCentrality=0> )"
    },
    "flogdensity": {
        "args": [
            "x",
            "dfnum",
            "dfden",
            "<nonCentrality=0>"
        ],
        "description": "Returns the log of the F probability density.",
        "prototype": "y = F Log Density( x, dfnum, dfden, <nonCentrality=0> )"
    },
    "flogdistribution": {
        "args": [
            "x",
            "dfnum",
            "dfden",
            "<nonCentrality=0>"
        ],
        "description": "Returns the log of the F distribution.",
        "prototype": "y = F Log Distribution( x, dfnum, dfden, <nonCentrality=0> )"
    },
    "floor": {
        "args": [
            "x"
        ],
        "description": "Returns the largest integer less than or equal to x. Argument can be a number, matrix, or list of numbers.",
        "prototype": "y = Floor( x )"
    },
    "fnoncentrality": {
        "args": [
            "x",
            "dfnum",
            "dfden",
            "prob"
        ],
        "description": "Solves for the noncentrality parameter nc such that prob = F Distribution( x, ndf, ddf, nc ).",
        "prototype": "nc = F Noncentrality( x, dfnum, dfden, prob )"
    },
    "for": {
        "args": [
            "initExpr",
            "whileExpr",
            "nextExpr",
            "bodyExpr"
        ],
        "description": "Evaluates initExpr once and repeatedly evaluates whileExpr, bodyExpr, and nextExpr as long as whileExpr evaluates to a nonzero value.",
        "prototype": "For( initExpr, whileExpr, nextExpr, bodyExpr )"
    },
    "foreachrow": {
        "args": [
            "<dt",
            ">",
            "body"
        ],
        "description": "Evaluates the body expression iteratively for each row in the current data table.",
        "prototype": "y = For Each Row( <dt,> body )"
    },
    "format": {
        "args": [
            "x",
            "formatString",
            "<options>",
            ")\r\ns",
            "=",
            "Format(",
            "x",
            "'Format",
            "Pattern'",
            "pattern",
            "<options>"
        ],
        "description": "Returns the number in the specified format. Formats include items in the Column Info dialog, such as 'Best' and 'h:m:s'. See Topic Help for other options, including p-value, currency, date and time, and geographic formats.",
        "prototype": "s = Format( x, formatString, <options> )\r\ns = Format( x, 'Format Pattern', pattern, <options> )"
    },
    "formatdate": {
        "args": [
            "x",
            "formatString",
            "<options>",
            ")\r\ns",
            "=",
            "Format(",
            "x",
            "'Format",
            "Pattern'",
            "pattern",
            "<options>"
        ],
        "description": "Returns the number in the specified format. Formats include items in the Column Info dialog, such as 'Best' and 'h:m:s'. See Topic Help for other options, including p-value, currency, date and time, and geographic formats.",
        "prototype": "s = Format( x, formatString, <options> )\r\ns = Format( x, 'Format Pattern', pattern, <options> )"
    },
    "fourierbasiscoef": {
        "args": [
            "x",
            "Number",
            "Pairs",
            "<Period",
            "=",
            "max(x)-min(x)+1>"
        ],
        "description": "Returns the matrix of Fourier Basis coefficients. Number Pairs is the number of sin() and cos() pairs for the basis. Optional parameter Period specifies the period for the trigonometric functions and defaults to max(x) - min(x) + 1.",
        "prototype": "coef = Fourier Basis Coef( x, Number Pairs, <Period = max(x)-min(x)+1> )"
    },
    "fpower": {
        "args": [
            "alpha",
            "dfh",
            "dfm",
            "d",
            "n"
        ],
        "description": "Calculates the power of an F Test, where alpha is the significance level, dfh is the hypothesis degrees of freedom, dfm is the degrees of freedom in the whole model, d is the squared effect size, SSH/(n*sigma^2) where SSH is the sum of squares for the hypothesis, and n is the total number of observations. Note that for the ANOVA model, d = Sum(a[i]^2)/(k * sigma^2) where a[i] are effects and k is the number of means.",
        "prototype": "p = F Power( alpha, dfh, dfm, d, n )"
    },
    "fquantile": {
        "args": [
            "p",
            "dfnum",
            "dfden",
            "<nonCentrality=0>"
        ],
        "description": "Returns the quantile from an F distribution, the value for which the probability is p that a random value would be lower.",
        "prototype": "q = F Quantile( p, dfnum, dfden, <nonCentrality=0> )"
    },
    "frechetdensity": {
        "args": [
            "x",
            "mu",
            "sigma"
        ],
        "description": "Returns the density at x of a Fréchet distribution with location mu and scale sigma.",
        "prototype": "y = Frechet Density( x, mu, sigma )"
    },
    "frechetdistribution": {
        "args": [
            "x",
            "mu",
            "sigma"
        ],
        "description": "Returns the probability at x of a Fréchet distribution with location mu and scale sigma.",
        "prototype": "p = Frechet Distribution( x, mu, sigma )"
    },
    "frechetquantile": {
        "args": [
            "p",
            "mu",
            "sigma"
        ],
        "description": "Returns the quantile at p of a Fréchet distribution with location mu and scale sigma.",
        "prototype": "q = Frechet Quantile( p, mu, sigma )"
    },
    "fsamplesize": {
        "args": [
            "alpha",
            "dfh",
            "dfm",
            "d",
            "power"
        ],
        "description": "Calculates the sample size, where alpha is the significance level, dfh is the hypothesis degrees of freedom, dfm is the degrees of freedom in the whole model, d is the squared effect size, SSH/(n*sigma^2) where SSH is the sum of squares for the hypothesis, and power is the power desired. Note that for the ANOVA model, d = Sum(a[i]^2)/(k * sigma^2) where a[i] are effects and k is the number of means.",
        "prototype": "n = F Sample Size( alpha, dfh, dfm, d, power )"
    },
    "function": {
        "args": [
            "{arg1=val1",
            "...}",
            "<{local1=val1",
            "...}>",
            "expr"
        ],
        "description": "Defines a function with the specified arguments, default values, and optional local variables. Arguments with default values are optional on invocation of the function. If Return() is used within the function's script, the expression within is returned.",
        "prototype": "y = Function( {arg1=val1, ...}, <{local1=val1, ...}>, expr )"
    },
    "futurevalue": {
        "args": [
            "rate",
            "nper",
            "pmt",
            "<pv=0>",
            "<type=0>"
        ],
        "description": "Returns the future value of an investment based on periodic, constant payments and a constant interest rate. The type argument is 0 for end-of-period payments and 1 for beginning-of-period payments. Equivalent to the FV function in Microsoft Excel.",
        "prototype": "x = Future Value( rate, nper, pmt, <pv=0>, <type=0> )"
    },
    "gamma": {
        "args": [
            "x",
            "<limit>"
        ],
        "description": "Returns Gamma function of x, defined as the integral of z^(x-1)*exp(-z) dz from 0 to ∞. If limit is present, an incomplete Gamma is computed using that limit of integration.",
        "prototype": "y = Gamma( x, <limit> )"
    },
    "gammadensity": {
        "args": [
            "q",
            "<alpha=1>",
            "<scale=1>",
            "<threshold=0>"
        ],
        "description": "Returns the density at q of a Gamma probability distribution, where the alpha shape parameter argument must be positive.",
        "prototype": "y = Gamma Density( q, <alpha=1>, <scale=1>, <threshold=0> )"
    },
    "gammadistribution": {
        "args": [
            "q",
            "<alpha=1>",
            "<scale=1>",
            "<threshold=0>"
        ],
        "description": "Returns the probability that a Gamma distributed random variable is less than q, where the alpha shape parameter argument must be positive. IGamma() is an alias name to Gamma Distribution(). The Gamma Distribution() function is equivalent to Gamma(alpha,q)/Gamma(alpha).",
        "prototype": "p = Gamma Distribution( q, <alpha=1>, <scale=1>, <threshold=0> )"
    },
    "gammalogcdistribution": {
        "args": [
            "x",
            "<alpha=1>",
            "<scale=1>",
            "<threshold=0>"
        ],
        "description": "Returns the log of 1 - Gamma distribution.",
        "prototype": "p = Gamma Log CDistribution( x, <alpha=1>, <scale=1>, <threshold=0> )"
    },
    "gammalogdensity": {
        "args": [
            "x",
            "<alpha=1>",
            "<scale=1>",
            "<threshold=0>"
        ],
        "description": "Returns the log of the Gamma probability density.",
        "prototype": "y = Gamma Log Density( x, <alpha=1>, <scale=1>, <threshold=0> )"
    },
    "gammalogdistribution": {
        "args": [
            "x",
            "<alpha=1>",
            "<scale=1>",
            "<threshold=0>"
        ],
        "description": "Returns the log of the Gamma distribution.",
        "prototype": "p = Gamma Log Distribution( x, <alpha=1>, <scale=1>, <threshold=0> )"
    },
    "gammapoissondistribution": {
        "args": [
            "k",
            "lambda",
            "sigma"
        ],
        "description": "Returns the probability that a gamma Poisson distributed random variable is less than or equal to k, where lambda is the mean parameter, sigma is the overdispersion parameter, and k is the count of interest.",
        "prototype": "cumprob = Gamma Poisson Distribution( k, lambda, sigma )"
    },
    "gammapoissonprobability": {
        "args": [
            "k",
            "lambda",
            "sigma"
        ],
        "description": "Returns the probability that a gamma Poisson distributed random variable is equal to k, where lambda is the mean parameter, sigma is the overdispersion parameter, and k is the count of interest.",
        "prototype": "prob = Gamma Poisson Probability( k, lambda, sigma )"
    },
    "gammapoissonquantile": {
        "args": [
            "lambda",
            "sigma",
            "cumprob"
        ],
        "description": "Returns the smallest integer quantile for which the cumulative probability of the Gamma Poisson( lambda, sigma ) distribution is larger than or equal to cumprob.",
        "prototype": "q = Gamma Poisson Quantile( lambda, sigma, cumprob )"
    },
    "gammaquantile": {
        "args": [
            "p",
            "<alpha=1>",
            "<scale=1>",
            "<threshold=0>"
        ],
        "description": "Returns the quantile from a Gamma distribution, the value for which the probability is p that a random value would be lower.",
        "prototype": "q = Gamma Quantile( p, <alpha=1>, <scale=1>, <threshold=0> )"
    },
    "gengammadensity": {
        "args": [
            "x",
            "mu",
            "sigma",
            "lambda"
        ],
        "description": "Returns the density at x of an extended generalized gamma probability distribution with parameters mu, sigma, and lambda.",
        "prototype": "y = GenGamma Density( x, mu, sigma, lambda )"
    },
    "gengammadistribution": {
        "args": [
            "x",
            "mu",
            "sigma",
            "lambda"
        ],
        "description": "Returns the probability that an extended generalized gamma distributed random variable (with parameters mu, sigma, and lambda) is less than x.",
        "prototype": "p = GenGamma Distribution( x, mu, sigma, lambda )"
    },
    "gengammaquantile": {
        "args": [
            "p",
            "mu",
            "sigma",
            "lambda"
        ],
        "description": "Returns the quantile from an extended generalized gamma distribution (with parameters mu, sigma, and lambda), the value for which the probability is p that a random value would be lower.",
        "prototype": "q = GenGamma Quantile( p, mu, sigma, lambda )"
    },
    "getaddin": {
        "args": [
            "ID"
        ],
        "description": "Retrieves a registered add-in specified by its ID.",
        "prototype": "Get Addin( ID )"
    },
    "getaddins": {
        "args": [],
        "description": "Returns a list of all registered add-ins.",
        "prototype": "Get Addins( )"
    },
    "getaddrinfo": {
        "args": [
            "string"
        ],
        "description": "Looks up the numeric address for a name. In most cases the name should be used for future IPV6 compatibility.",
        "prototype": "Get Addr Info( string )"
    },
    "getclasses": {
        "args": [
            "<",
            "<class",
            "reference>",
            "...",
            ">"
        ],
        "description": "Returns a list of references to all currently defined classes",
        "prototype": "Get Classes( < <class reference>, ... > )"
    },
    "getclassnames": {
        "args": [
            "<",
            "<class",
            "reference>",
            "...",
            ">"
        ],
        "description": "Returns a list of names of all currently defined classes.",
        "prototype": "Get Class Names( < <class reference>, ... > )"
    },
    "getclipboard": {
        "args": [],
        "description": "Get the current contents of the clipboard",
        "prototype": "Get Clipboard()"
    },
    "getcolorthemedetail": {
        "args": [
            "name"
        ],
        "description": "Returns the script for a given color theme name",
        "prototype": "script = Get Color Theme Detail(name)"
    },
    "getcolorthemenames": {
        "args": [
            "<kind>"
        ],
        "description": "Returns a list of color theme strings which match the optional parameter kind. kind is one of the following: 'continuous', 'categorical', 'sequential', 'diverging', 'qualitative', or 'chromatic'.",
        "prototype": "{list of names} = Get Color Theme Names(<kind>)"
    },
    "getcustomfunctions": {
        "args": [
            "<{function",
            "1",
            "full",
            "name",
            "function",
            "2",
            "full",
            "name",
            "...}",
            "|",
            "function",
            "full",
            "name>"
        ],
        "description": "Get a list of custom functions",
        "prototype": "Get Custom Functions(<{function 1 full name, function 2 full name, ...} | function full name>)"
    },
    "getdatatable": {
        "args": [
            "<Project(title|index|box|window)",
            ">",
            "name|index"
        ],
        "description": "Returns a reference to the specified data table.\r\n\r\nThe search is limited to tables in the current project (or no project when not running the script in a project).\r\n\r\nTo specify a project, use the optional Project() argument with a title, index, display box, or window object. Use Project(0) to specify no project when running the script in a project.",
        "prototype": "dt = Get Data Table( <Project(title|index|box|window),> name|index )"
    },
    "getdatatablelist": {
        "args": [
            "<Project(title|index|box|window)>"
        ],
        "description": "Returns a list of all open data tables.\r\n\r\nThe list is limited to tables in the current project (or no project when not running the script in a project).\r\n\r\nTo specify a project, use the optional Project() argument with a title, index, display box, or window object. Use Project(0) to specify no project when running the script in a project.",
        "prototype": "tableList = Get Data Table List( <Project(title|index|box|window)> )"
    },
    "getdefaultdirectory": {
        "args": [],
        "description": "Returns the JMP default directory, which is used as a base for subsequent relative paths.",
        "prototype": "y = Get Default Directory()"
    },
    "getenvironmentvariable": {
        "args": [
            "string"
        ],
        "description": "Returns the value of the specified environment variable from the operating system.\r\n\r\nNOTE: On the Macintosh operating system, the variable name is case-sensitive.",
        "prototype": "value = Get Environment Variable( string )"
    },
    "getexcelworksheets": {
        "args": [
            "'filepath'"
        ],
        "description": "Returns a list of worksheets within an Excel Workbook",
        "prototype": "list = Get Excel Worksheets('filepath')"
    },
    "getfilesearchpath": {
        "args": [],
        "description": "Returns the current list of directories to search for opening files.",
        "prototype": "y = Get File Search Path()"
    },
    "getjmplivebookmarks": {
        "args": [],
        "description": "Returns an associative array of bookmark name keys and URL values",
        "prototype": "Get JMP Live Bookmarks()"
    },
    "getjmpliveconnections": {
        "args": [],
        "description": "Gets saved JMP Live connection information.",
        "prototype": "Get JMP Live Connections( )"
    },
    "getlocalesetting": {
        "args": [
            "settingName"
        ],
        "description": "Retrieves a locale setting such as decimal separator",
        "prototype": "value = Get Locale Setting( settingName )"
    },
    "getlog": {
        "args": [
            "<N>"
        ],
        "description": "Returns a list of lines from the log. If no argument is specified, all the lines from the log are returned. If the numeric argument N is positive, the first N lines from the log are returned. If N is negative, the last N lines from the log are returned. If N is zero, no lines are returned.",
        "prototype": "list = Get Log( <N> )"
    },
    "getnameinfo": {
        "args": [
            "string"
        ],
        "description": "Looks up the name for a numeric address. In most cases the name should be used for future IPV6 compatibility.",
        "prototype": "Get Name Info( string )"
    },
    "getnamespacenames": {
        "args": [
            "<",
            "<namespace",
            "reference>",
            "...",
            ">"
        ],
        "description": "Returns a list of names of all currently defined namespaces.",
        "prototype": "Get Namespace Names( < <namespace reference>, ... > )"
    },
    "getnamespaces": {
        "args": [
            "<",
            "<namespace",
            "reference>",
            "...",
            ">"
        ],
        "description": "Returns a list of references to all currently defined namespaces",
        "prototype": "Get Namespaces( < <namespace reference>, ... > )"
    },
    "getoauth2granttypes": {
        "args": [],
        "description": "Gets the supported JMP OAuth2 grant types.",
        "prototype": "Get OAuth2 Grant Types"
    },
    "getopenidcdiscovery": {
        "args": [],
        "description": "",
        "prototype": ""
    },
    "getopenidconnectdiscovery": {
        "args": [],
        "description": "",
        "prototype": ""
    },
    "getpathvariable": {
        "args": [
            "name"
        ],
        "description": "Returns the value of a path variable, which is a name like SAMPLE_DATA that is substituted for when found in pathnames.",
        "prototype": "value = Get Path Variable( name )"
    },
    "getplatformpreference": {
        "args": [
            "<",
            "platformName",
            "<",
            "(",
            "optionName",
            "...",
            ")",
            ">",
            "...",
            ">"
        ],
        "description": "Gets platform preferences as specified.",
        "prototype": "Get Platform Preferences( < platformName < ( optionName, ... ) > ... > )"
    },
    "getplatformpreferences": {
        "args": [
            "<",
            "platformName",
            "<",
            "(",
            "optionName",
            "...",
            ")",
            ">",
            "...",
            ">"
        ],
        "description": "Gets platform preferences as specified.",
        "prototype": "Get Platform Preferences( < platformName < ( optionName, ... ) > ... > )"
    },
    "getpreference": {
        "args": [
            "pref1",
            "..."
        ],
        "description": "Gets preferences as specified.",
        "prototype": "Get Preferences( pref1, ... )"
    },
    "getpreferences": {
        "args": [
            "pref1",
            "..."
        ],
        "description": "Gets preferences as specified.",
        "prototype": "Get Preferences( pref1, ... )"
    },
    "getproject": {
        "args": [
            "title|index|box|window"
        ],
        "description": "Returns a reference to a specific open project by title, index, or box.",
        "prototype": "project = Get Project( title|index|box|window )"
    },
    "getprojectlist": {
        "args": [],
        "description": "Returns a list of all open projects.",
        "prototype": "projectList = Get Project List()"
    },
    "getpunctuationcharacters": {
        "args": [
            "<Exclude",
            "Chars(chars)",
            "|",
            "Include",
            "Chars(chars)>"
        ],
        "description": "Returns a string containing the punctuation characters that are typically used for delimiting words. These include ,:;.?!\\/#@&~()[]<>'*`%$+=^|{} and some common Unicode punctuation.",
        "prototype": "Get Punctuation Characters(<Exclude Chars(chars) | Include Chars(chars)>)"
    },
    "getsasversionpreference": {
        "args": [],
        "description": "Returns the SAS version selected in Preferences.",
        "prototype": "pref = Get SAS Version Preference()"
    },
    "getwhitespacecharacters": {
        "args": [],
        "description": "Returns a string containing all of the whitespace characters that are typically used.",
        "prototype": "Get Whitespace Characters()"
    },
    "getwindow": {
        "args": [
            "<Project(title|index|box|window)",
            ">",
            "<Type(string)",
            ">",
            "title|index|box"
        ],
        "description": "Returns a reference to a specific open window by title, index, or box.\r\n\r\nThe search is limited to windows in the current project (or no project when not running the script in a project).\r\n\r\nTo specify a project, use the optional Project() argument with a title, index, display box, or window object. Use Project(0) to specify no project when running the script in a project.\r\n\r\nUse the optional Type() argument with one of 'Data Tables', 'Journals', 'Reports', or 'Dialogs' to limit the search to windows of a particular type.",
        "prototype": "window = Get Window( <Project(title|index|box|window),> <Type(string),> title|index|box )"
    },
    "getwindowlist": {
        "args": [
            "<Project(title|index|box|window)",
            ">",
            "<Type(string)>"
        ],
        "description": "Returns a list of all open windows.\r\n\r\nThe list is limited to windows in the current project (or no project when not running the script in a project).\r\n\r\nTo specify a project, use the optional Project() argument with a title, index, display box, or window object. Use Project(0) to specify no project when running the script in a project.\r\n\r\nUse the optional Type() argument with one of 'Data Tables', 'Journals', 'Reports', or 'Dialogs' to limit the list to windows of a particular type.",
        "prototype": "windowList = Get Window List( <Project(title|index|box|window),> <Type(string)> )"
    },
    "ginverse": {
        "args": [
            "A"
        ],
        "description": "Returns the generalized (Moore-Penrose) matrix inverse.",
        "prototype": "g = G Inverse( A )"
    },
    "globalbox": {
        "args": [
            "name"
        ],
        "description": "Creates a display box showing the value of a global variable.",
        "prototype": "box = Global Box( name )"
    },
    "glogdensity": {
        "args": [
            "q",
            "mu",
            "sigma",
            "lambda"
        ],
        "description": "Returns the density at q of a generalized logarithm distribution with location mu, scale sigma, and shape lambda.",
        "prototype": "y = GLog Density( q, mu, sigma, lambda )"
    },
    "glogdistribution": {
        "args": [
            "q",
            "mu",
            "sigma",
            "lambda"
        ],
        "description": "Returns the probability that a generalized logarithm distributed random variable is less than q.",
        "prototype": "p = GLog Distribution( q, mu, sigma, lambda )"
    },
    "glogquantile": {
        "args": [
            "p",
            "mu",
            "sigma",
            "lambda"
        ],
        "description": "Returns the quantile from a generalized logarithm distribution, the value for which the probability is p that a random value would be lower.",
        "prototype": "q = GLog Quantile( p, mu, sigma, lambda )"
    },
    "glue": {
        "args": [
            "expr1;",
            "expr2;",
            "...",
            ");",
            "y",
            "=",
            "Glue(",
            "expr1",
            "expr2",
            "..."
        ],
        "description": "Evaluates each argument and returns the last result.",
        "prototype": "y = ( expr1; expr2; ... ); y = Glue( expr1, expr2, ... )"
    },
    "googlesheetexport": {
        "args": [
            "dt",
            "email",
            "spreadsheet",
            "url/id",
            "|",
            "new",
            "spreadsheet",
            "name",
            "sheet",
            "name"
        ],
        "description": "Exports a Data Table to a new Google Spreadsheet or a new Sheet within an existing Google Spreadsheet.",
        "prototype": "Google Sheet Export(dt, email, spreadsheet url/id | new spreadsheet name, sheet name)"
    },
    "googlesheetimport": {
        "args": [
            "email",
            "spreadsheet",
            "url/id",
            "<'sheet",
            "name'",
            "|",
            "{'sheet",
            "name'",
            "'sheet",
            "name'}",
            "|",
            "googlesheet",
            "settings>"
        ],
        "description": "Opens a Google Sheet file.",
        "prototype": "Google Sheet Import(email, spreadsheet url/id, <'sheet name' | {'sheet name', 'sheet name'} | googlesheet settings>)"
    },
    "gradientfunction": {
        "args": [
            "zExpr",
            "xName",
            "yName",
            "zLimits",
            "zColor(",
            "color",
            "option",
            ")",
            "<",
            "<<XGrid(",
            "min",
            "max",
            "incr",
            ")>",
            "<",
            "<<YGrid(",
            "min",
            "max",
            "incr",
            ")>",
            "<",
            "<<Transparency(",
            "t",
            ")>"
        ],
        "description": "Fills the graph with a gradient between two colors. The zExpr argument is a function in terms of the variables specified by xName and yName. The vector zLimits specifies the range of values for zExpr. The zColor argument is a vector that defines the two colors that are blended together to create the gradient.",
        "prototype": "Gradient Function( zExpr, xName, yName, zLimits, zColor( color, option ), < <<XGrid( min, max, incr )>, < <<YGrid( min, max, incr )>, < <<Transparency( t )> )"
    },
    "graph": {
        "args": [
            "props",
            "script"
        ],
        "description": "Returns a display box containing a graph with axes. Named property arguments can be title('title'), XScale(low,high), YScale(low,high), FrameSize(h,v), XName('x'), yName('y'), DoubleBuffer, and SuppressAxes.",
        "prototype": "y = Graph Box( props, script )"
    },
    "graph3dbox": {
        "args": [],
        "description": "(Experimental) Returns a display box with 3D content that can be used with other display boxes to create custom reports.",
        "prototype": "y = Graph 3D Box()"
    },
    "graphbox": {
        "args": [
            "props",
            "script"
        ],
        "description": "Returns a display box containing a graph with axes. Named property arguments can be title('title'), XScale(low,high), YScale(low,high), FrameSize(h,v), XName('x'), yName('y'), DoubleBuffer, and SuppressAxes.",
        "prototype": "y = Graph Box( props, script )"
    },
    "greater": {
        "args": [
            "x",
            "y",
            "..."
        ],
        "description": "Returns 1 if each argument is greater than the next argument; returns 0 otherwise.",
        "prototype": "z = x > y > ... ; z = Greater( x, y, ... )"
    },
    "greaterorequal": {
        "args": [
            "x",
            "y",
            "..."
        ],
        "description": "Returns 1 if each argument is greater than or equal to the next argument; returns 0 otherwise.",
        "prototype": "z = x >= y >= ... ; z = Greater or Equal( x, y, ... )"
    },
    "gzipcompress": {
        "args": [
            "blob"
        ],
        "description": "Compresses a blob of data into a gzip blob.",
        "prototype": "blob = Gzip Compress( blob )"
    },
    "gzipuncompress": {
        "args": [
            "blob"
        ],
        "description": "Uncompresses a blob of gzip data into a blob.",
        "prototype": "blob = Gzip Uncompress( blob )"
    },
    "hadamard": {
        "args": [
            "n",
            "<normalize",
            "=",
            "0>"
        ],
        "description": "Creates a Hadamard matrix of order n.",
        "prototype": "y = Hadamard( n, <normalize = 0> )"
    },
    "handle": {
        "args": [
            "xPos",
            "yPos",
            "dragScript",
            "<mouseUpScript>"
        ],
        "description": "Draws a square marker at the coordinates specified by xPos and yPos and repeatedly evaluates the dragScript expression when the mouse is pressed over the marker. Before running the script, the globals x and y are set to the mouse value and are restored to their original values afterward. The mouseUpScript expression is run after the mouse button is released.",
        "prototype": "Handle( xPos, yPos, dragScript, <mouseUpScript> )"
    },
    "hcenterbox": {
        "args": [
            "<childbox>"
        ],
        "description": "Returns a display box with the childbox display box argument centered in the horizontal space defined by the maximum size of that child and all the other siblings of the center box.",
        "prototype": "y = H Center Box( <childbox> )"
    },
    "hdirectproduct": {
        "args": [
            "A",
            "B"
        ],
        "description": "Returns the horizontal direct product, which is the direct product of each row of matrices A and B.",
        "prototype": "y = H Direct Product( A, B )"
    },
    "head": {
        "args": [
            "x"
        ],
        "description": "Returns the head of the evaluated expression, without its arguments.",
        "prototype": "y = Head( x )"
    },
    "headexpr": {
        "args": [
            "expr"
        ],
        "description": "Returns the head of the expression, without its arguments. This function is deprecated. Please use Head() instead.",
        "prototype": "y = Head Expr( expr )"
    },
    "headname": {
        "args": [
            "x"
        ],
        "description": "Returns the head of the evaluated expression as a string, without its arguments.",
        "prototype": "y = Head Name( x )"
    },
    "headnameexpr": {
        "args": [
            "expr"
        ],
        "description": "Returns the head of the expression as a string, without its arguments. This function is deprecated. Please use Head Name() instead.",
        "prototype": "y = Head Name Expr( expr )"
    },
    "heatcolor": {
        "args": [
            "x",
            ");",
            "y",
            "=",
            "Heat",
            "Color(",
            "x",
            "<",
            "<<theme>"
        ],
        "description": "Returns a color corresponding to a value between 0 and 1. Default theme is 'Blue to Gray to Red'. Any theme supported by Cell Plot is supported here. Matrix arguments supported.",
        "prototype": "y = Heat Color( x ); y = Heat Color( x, < <<theme> )"
    },
    "hex": {
        "args": [
            "value",
            "<'integer'>|<encoding='utf-8'>|<Base(number)>",
            "<Pad",
            "To(number)>"
        ],
        "description": "Returns the hexadecimal (or other base number system) text corresponding to the given value and encoding, which can be a number a string or a blob. If the value is a number, IEEE 754 64-bit encoding is used unless one of the optional arguments, integer or Base, is provided. If Base is specified, the function returns the text corresponding to the specified number in that base number system instead of hexadecimal. The base must be an integer value between 2 and 36 inclusive. Supported encodings include utf-8, utf-16le, utf-16be, us-ascii, iso-8859-1, ascii~hex, shift_jis and euc-jp.",
        "prototype": "h = Hex( value, <'integer'>|<encoding='utf-8'>|<Base(number)>,<Pad To(number)> )"
    },
    "hextoblob": {
        "args": [
            "hex",
            "string"
        ],
        "description": "Makes a BLOB (Binary Large OBject) from the given string of hexadecimal codes, which may also include whitespace.",
        "prototype": "blob = Hex To Blob( hex string )"
    },
    "hextochar": {
        "args": [
            "hextext",
            "<encoding='utf-8'>"
        ],
        "description": "Returns the text corresponding to the hexadecimal text, using the specified encoding. Supported encodings include utf-8, utf-16le, utf-16be, us-ascii, iso-8859-1, ascii~hex, shift_jis, and euc-jp.",
        "prototype": "s = Hex To Char( hextext, <encoding='utf-8'> )"
    },
    "hextonumber": {
        "args": [
            "hextext",
            "<Base(number)>"
        ],
        "description": "Returns the number corresponding to the hexadecimal (or other base number system) text. 16 hex digits are converted as IEEE 754 64-bit floating point numbers; otherwise the input is treated as a hex integer. If Base is specified, the text is treated as a string representing the number in that base. Base must be an integer between 2 and 36 inclusive.",
        "prototype": "x = Hex To Number( hextext, <Base(number)> )"
    },
    "hidden": {
        "args": [
            "rs",
            ");",
            "Hidden(",
            "<Row",
            "State(",
            "<r>",
            ")>"
        ],
        "description": "Returns the hidden component of the specified row state value, 0 or 1. If Hidden is used as an L-value, it changes the hidden state of the current (or rth) row in the current data table.",
        "prototype": "y = Hidden( rs ); Hidden( <Row State( <r> )> ) = y"
    },
    "hiddenstate": {
        "args": [
            "x"
        ],
        "description": "Returns a row state value with the hidden component set to the specified value.",
        "prototype": "rs = Hidden State( x )"
    },
    "hierbox": {
        "args": [
            "text",
            "Hier",
            "Box(",
            "...",
            ")",
            "Hier",
            "Box(",
            "...",
            ")",
            "..."
        ],
        "description": "Returns a display box for hierarchy trees. The text argument is the node's name and can be a Text Edit Box.",
        "prototype": "y = Hier Box( text, Hier Box( ... ), Hier Box( ... ), ... )"
    },
    "hierclust": {
        "args": [
            "x"
        ],
        "description": "Returns the clustering history for a hierarchical clustering using Ward's method (without standardizing data), where x is a data matrix.",
        "prototype": "{c1, c2, c3, c4, c5} = Hier Clust( x )"
    },
    "histseg": {
        "args": [
            "[data]",
            "<[freq",
            "data]>",
            "<[weight",
            "data]>",
            "<vertical=0|1>",
            "<Row",
            "States()>"
        ],
        "description": "Returns a hist seg",
        "prototype": "b = Hist Seg([data], <[freq data]>,<[weight data]>, <vertical=0|1>, <Row States()>)"
    },
    "hline": {
        "args": [
            "y",
            ");",
            "H",
            "Line(",
            "x1",
            "x2",
            "y"
        ],
        "description": "Draws a horizontal line at y from x1 to x2 or through the entire frame.",
        "prototype": "H Line( y ); H Line( x1, x2, y )"
    },
    "hlistbox": {
        "args": [
            "<Align(",
            "center|bottom",
            ")>",
            "displayBox",
            "..."
        ],
        "description": "Returns a display box that arranges the display boxes provided by the arguments in a horizontal layout. The <<Hold message tells the sheet to own the report(s) that will be excerpted. The optional Align argument allows for bottom or center alignment of the contents inside the display box.",
        "prototype": "y = H List Box( <Align( center|bottom )>, displayBox, ... )"
    },
    "hlscolor": {
        "args": [
            "h",
            "l",
            "s",
            ");",
            "y",
            "=",
            "HLS",
            "Color(",
            "{h",
            "l",
            "s}"
        ],
        "description": "Returns a color number from the hue, lightness, and saturation components, all between 0 and 1.",
        "prototype": "y = HLS Color( h, l, s ); y = HLS Color( {h, l, s} )"
    },
    "hostis": {
        "args": [
            "'Mac'|'Windows'"
        ],
        "description": "Returns 1 if the JMP application matches the argument; returns 0 otherwise. The arguments Windows or Mac test for the specified operating system, and the arguments Bits32 or Bits64 test for the specified 32-bit or 64-bit JMP application. Only one argument can be tested at a time.",
        "prototype": "y = Host is( 'Mac'|'Windows')"
    },
    "houghlinetransform": {
        "args": [
            "matrix",
            "<NAngle(number)>",
            "<NRadius(number)>"
        ],
        "description": "Returns the Hough transform for detecting lines in image data",
        "prototype": "accum = Hough Line Transform( matrix, <NAngle(number)> <NRadius(number)> )"
    },
    "hour": {
        "args": [
            "datetime",
            "<12>"
        ],
        "description": "Returns the hours part of a date-time value, in 12-hour mode (12, 1 - 11) or 24-hour mode (0 - 23).",
        "prototype": "hr = Hour( datetime, <12> )"
    },
    "hptime": {
        "args": [],
        "description": "Returns a High Precision time value in microseconds. Only useful relative to another HP Time() value. The time value represents the number of microseconds since the start of the JMP session.",
        "prototype": "t = HP Time()"
    },
    "hscrollbox": {
        "args": [
            "<Size(",
            "h",
            ")>",
            "displayBox"
        ],
        "description": "Returns a display box that positions a larger child box using a horizontal scroll bar.",
        "prototype": "y = H Scroll Box( <Size( h )>, displayBox )"
    },
    "hsheetbox": {
        "args": [
            "<<Hold(",
            "rpt",
            ")",
            "displayBox",
            "..."
        ],
        "description": "Returns a display box that arranges the display boxes provided by the arguments in a horizontal layout. The <<Hold message tells the sheet to own the report(s) that will be excerpted. The optional Align argument allows for right or center alignment of the contents inside the display box.",
        "prototype": "y = H Sheet Box( <<Hold( rpt ), displayBox, ... )"
    },
    "hsize": {
        "args": [],
        "description": "Returns the horizontal size of the graphics frame in pixels.",
        "prototype": "h = H Size()"
    },
    "hsplitterbox": {
        "args": [
            "<Size(x",
            "y)>",
            "displayBox",
            "..."
        ],
        "description": "Returns a display box that arranges other display boxes horizontally, with interactive control of sizes. Child sizes are specified as a proportion of the width or height of the Splitter Box. The optional Size argument is only used for the top-most Splitter Box; lower level boxes are sized like any other child box.",
        "prototype": "y = H Splitter Box( <Size(x,y)>, displayBox, ... )"
    },
    "huestate": {
        "args": [
            "x"
        ],
        "description": "Returns a row state value with the color hue component set to the specified value. Needs to be combined with a Shade State() value to produce a valid color.",
        "prototype": "rs = Hue State( x )"
    },
    "hypergeometricdistribution": {
        "args": [
            "N",
            "K",
            "n",
            "x",
            "<r>"
        ],
        "description": "Returns the probability that a hypergeometrically distributed random variable is less than or equal to x, where N is the population size, K is the number of items in the category of interest, n is the sample size, x is the count of interest, and r is the optional odds ratio.",
        "prototype": "cumprob = Hypergeometric Distribution( N, K, n, x, <r> )"
    },
    "hypergeometricprobability": {
        "args": [
            "N",
            "K",
            "n",
            "x",
            "<r>"
        ],
        "description": "Returns the probability that a hypergeometrically distributed random variable is equal to x, where N is the population size, K is the number of items in the category of interest, n is the sample size, x is the count of interest, and r is the optional odds ratio.",
        "prototype": "prob = Hypergeometric Probability( N, K, n, x, <r> )"
    },
    "iconbox": {
        "args": [
            "'Name'"
        ],
        "description": "Constructs a display box containing an icon, where the name argument can be a JMP icon name or a path to a an image.",
        "prototype": "Box = Icon Box( 'Name' )"
    },
    "identity": {
        "args": [
            "n"
        ],
        "description": "Creates an n-by-n identity matrix, with ones on diagonal, zeros elsewhere.",
        "prototype": "y = Identity( n )"
    },
    "if": {
        "args": [
            "condition1",
            "result1",
            "<condition2",
            "result2",
            ">",
            "...",
            "<elseResult>"
        ],
        "description": "Evaluates the first of each pair of arguments and returns the evaluation of the result expression associated with the first condition argument that evaluates to a nonzero result. The condition arguments are evaluated in order. If all of the condition arguments evaluate to zero, the optional elseResult is evaluated and the result is returned. If no elseResult is specified, and none of the conditions are true, a missing value is returned. If all of the condition arguments evaluate to missing, a missing value is returned.",
        "prototype": "y = If( condition1, result1, <condition2, result2,> ..., <elseResult> )"
    },
    "ifbox": {
        "args": [
            "0|1",
            "displayBoxArgs"
        ],
        "description": "Returns a display box that conditionally displays the specified display box arguments.",
        "prototype": "box = If Box( 0|1, displayBoxArgs )"
    },
    "ifmax": {
        "args": [
            "expr1",
            "result1",
            "expr2",
            "result2",
            "...",
            "<allMissingResult>"
        ],
        "description": "Evaluates the first of each pair of arguments, and returns the evaluation of the result expression associated with the maximum of the expressions. If there are ties, it returns the first maximum. If all expressions are missing, it returns Empty if an even number of arguments, or the last argument if odd. The test expressions must evaluate to numeric, but the result expressions can be anything.",
        "prototype": "y = IfMax( expr1, result1, expr2, result2, ..., <allMissingResult> )"
    },
    "ifmin": {
        "args": [
            "expr1",
            "result1",
            "expr2",
            "result2",
            "...",
            "<allMissingResult>"
        ],
        "description": "Evaluates the first of each pair of arguments, and returns the evaluation of the result expression associated with the minimum of the expressions. If there are ties, it returns the first minimum. If all expressions are missing, it returns Empty if an even number of arguments, or the last argument if odd. The test expressions must evaluate to numeric, but the result expressions can be anything.",
        "prototype": "y = IfMin( expr1, result1, expr2, result2, ..., <allMissingResult> )"
    },
    "ifmz": {
        "args": [
            "condition1",
            "result1",
            "<condition2",
            "result2",
            ">",
            "...",
            "<elseResult>"
        ],
        "description": "Evaluates the first of each pair of arguments and returns the evaluation of the result expression associated with the first condition argument that evaluates to a nonzero result. The condition arguments are evaluated in order. If all condition arguments evaluate to zero or missing, the optional elseResult is evaluated and the result is returned. If no elseResult is specified, and none of the conditions are true, a missing value is returned. (IfMZ() is equivalent to If() where missing values for evaluated condition arguments are treated as zero.)",
        "prototype": "y = IfMZ( condition1, result1, <condition2, result2,> ..., <elseResult> )"
    },
    "ifseg": {
        "args": [
            "<state=0|1>"
        ],
        "description": "Returns a display seg that shows or hides display seg children.",
        "prototype": "seg = If Seg(<state=0|1>)"
    },
    "igamma": {
        "args": [
            "q",
            "<alpha=1>",
            "<scale=1>",
            "<threshold=0>"
        ],
        "description": "Returns the probability that a Gamma distributed random variable is less than q, where the alpha shape parameter argument must be positive. IGamma() is an alias name to Gamma Distribution(). The Gamma Distribution() function is equivalent to Gamma(alpha,q)/Gamma(alpha).",
        "prototype": "p = Gamma Distribution( q, <alpha=1>, <scale=1>, <threshold=0> )"
    },
    "include": {
        "args": [
            "filepath",
            "<",
            "<<Parse",
            "Only>",
            "<",
            "<<New",
            "Context>",
            "<",
            "<<Names",
            "Default",
            "to",
            "Here>"
        ],
        "description": "Executes the JSL in the specified file. If Parse Only is specified, the script is parsed rather than executed. If New Context is specified, the included JSL is executed in its own unique namespace. If both the parent and included scripts use the global namespace, then specify both New Context and Names Default to Here to avoid name collisions.",
        "prototype": "y = Include( filepath, < <<Parse Only>, < <<New Context>, < <<Names Default to Here> )"
    },
    "includefilelist": {
        "args": [],
        "description": "Returns a list of included files at the point of execution.",
        "prototype": "y = Include File List()"
    },
    "indays": {
        "args": [
            "<x=1>"
        ],
        "description": "Converts x from a number of days to the equivalent number of seconds.",
        "prototype": "y = In Days( <x=1> )"
    },
    "index": {
        "args": [
            "n1",
            "n2",
            "<n3=1>"
        ],
        "description": "Returns a column matrix that contains the sequence of values from n1 to n2 by increments of n3.",
        "prototype": "ii = n1::n2; ii = n1::n2::n3; ii = Index( n1, n2, <n3=1>)"
    },
    "informat": {
        "args": [
            "s",
            "formatString",
            "<",
            "<<Use",
            "Locale(b=1)>",
            ")\r\ndt",
            "=",
            "In",
            "Format(",
            "s",
            "'Format",
            "Pattern'",
            "pattern",
            "<",
            "<<Use",
            "Locale(b=1)>"
        ],
        "description": "Parses a string of a given format and returns date/time value expressed as if surrounded by As Date(), returning the date in ddMonyyyy format.",
        "prototype": "dt = In Format( s, formatString, < <<Use Locale(b=1)> )\r\ndt = In Format( s, 'Format Pattern', pattern, < <<Use Locale(b=1)> )"
    },
    "inhours": {
        "args": [
            "<x=1>"
        ],
        "description": "Converts x from a number of hours to the equivalent number of seconds.",
        "prototype": "y = In Hours( <x=1> )"
    },
    "inminutes": {
        "args": [
            "<x=1>"
        ],
        "description": "Converts x from a number of minutes to the equivalent number of seconds.",
        "prototype": "y = In Minutes( <x=1> )"
    },
    "inpath": {
        "args": [
            "x",
            "y",
            "pathMatrix|pathText"
        ],
        "description": "Returns 1 if the point (x,y) is in the given path, otherwise returns 0.",
        "prototype": "b = In Path( x, y, pathMatrix|pathText )"
    },
    "inpolygon": {
        "args": [
            "x",
            "y",
            "xMatrix",
            "<yMatrix>"
        ],
        "description": "Returns 1 if the point (x,y) is in the polygon defined by the vector arguments, otherwise returns 0.",
        "prototype": "b = In Polygon( x, y, xMatrix, <yMatrix> )"
    },
    "insert": {
        "args": [
            "x",
            "y",
            "<i>"
        ],
        "description": "Returns a copy of list x with y inserted at the ith position or appended to the end if the optional i argument is not specified.",
        "prototype": "z = Insert( x, y, <i> )"
    },
    "insertinto": {
        "args": [
            "x",
            "y",
            "<i>"
        ],
        "description": "Modifies list, associative array, or display box x with y inserted into the collection. Lists and display boxes support an optional i to specify the position, or the items will be appended if the position is not specified. Note that the x argument must be a variable.",
        "prototype": "Insert Into( x, y, <i> )"
    },
    "integrate": {
        "args": [
            "expr",
            "varname",
            "lowLimit",
            "upLimit",
            "<<Tolerance(1e-10)",
            "<<StoreInfo(list)",
            "<<StartingValue(val)"
        ],
        "description": "Integrates an expression with respect to a scalar value, using adaptive quadrature method from Gander and Gautschi (2000). If the variable specified with varname has a value assigned to it or the <<StartingValue() optional argument specifies a starting value, that value is used as a typical value to improve the accuracy of the integral. To specify infinite ranges of integration, set lowLimit, upLimit, or both to missing. If <<StoreInfo() is specified, the argument of <<StoreInfo() will contain diagnostics of the numerical integration routine. If <<Tolerance() is specified, the argument of <<Tolerance() is used as the tolerance level in the autointegration function used to evaluate the integral. Smaller values result in longer run time but more precise results.",
        "prototype": "y = Integrate( expr, varname, lowLimit, upLimit, <<Tolerance(1e-10), <<StoreInfo(list), <<StartingValue(val) )"
    },
    "interestpayment": {
        "args": [
            "rate",
            "per",
            "nper",
            "pv",
            "<fv=0>",
            "<type=0>"
        ],
        "description": "Returns the interest payment for a given period for an investment based on periodic, constant payments and a constant interest rate. The type argument is 0 for end-of-period payments and 1 for beginning-of-period payments. Equivalent to the IPMT function in Microsoft Excel.",
        "prototype": "x = Interest Payment( rate, per, nper, pv, <fv=0>, <type=0> )"
    },
    "interestrate": {
        "args": [
            "nper",
            "pmt",
            "pv",
            "<fv=0>",
            "<type=0>",
            "<guess=0.1>"
        ],
        "description": "Returns the interest rate per period of an annuity. The type argument is 0 for end-of-period payments and 1 for beginning-of-period payments. Equivalent to the RATE function in Microsoft Excel.",
        "prototype": "x = Interest Rate( nper, pmt, pv, <fv=0>, <type=0>, <guess=0.1> )"
    },
    "internalrateofreturn": {
        "args": [
            "values",
            "<guess=0.1>",
            ");\r\nx",
            "=",
            "Internal",
            "Rate",
            "of",
            "Return(",
            "guess",
            "value1",
            "value2",
            "<value3",
            "...>"
        ],
        "description": "Returns the internal rate of return for a series of cash flows represented by the numbers in the values argument. Equivalent to the IRR function in Microsoft Excel. The second prototype of the function accepts all scalar arguments.",
        "prototype": "x = Internal Rate of Return( values, <guess=0.1> );\r\nx = Internal Rate of Return( guess, value1, value2, <value3, ...> )"
    },
    "interpolate": {
        "args": [
            "x",
            "x1",
            "y1",
            "x2",
            "y2",
            "...",
            ");",
            "y",
            "=",
            "Interpolate(",
            "x",
            "xvec",
            "yvec",
            ");",
            "z",
            "=",
            "Interpolate({x",
            "y}",
            "xvec",
            "yvec",
            "zmatrix);",
            "z",
            "=",
            "Interpolate(xmat",
            "xvec",
            "yvec);",
            "z",
            "=",
            "Interpolate({x1",
            "x2...}",
            "xvec",
            "yvec"
        ],
        "description": "Finds the xi arguments that x is between and linearly interpolates the corresponding yi arguments. Note that the xi arguments must be specified in order.",
        "prototype": "y = Interpolate( x, x1, y1, x2, y2, ... ); y = Interpolate( x, xvec, yvec ); z = Interpolate({x,y},xvec,yvec,zmatrix); z = Interpolate(xmat, xvec, yvec); z = Interpolate({x1,x2...}, xvec, yvec)"
    },
    "inv": {
        "args": [
            "x",
            ");",
            "y",
            "=",
            "Inv(",
            "x"
        ],
        "description": "Returns the inverse of the x argument, which must be a square non-singular matrix.",
        "prototype": "y = Inverse( x ); y = Inv( x )"
    },
    "inverse": {
        "args": [
            "x",
            ");",
            "y",
            "=",
            "Inv(",
            "x"
        ],
        "description": "Returns the inverse of the x argument, which must be a square non-singular matrix.",
        "prototype": "y = Inverse( x ); y = Inv( x )"
    },
    "invertexpr": {
        "args": [
            "expr",
            "xname",
            "yname"
        ],
        "description": "Inverts the expr expression argument, unfolding around the single occurrence of xname.",
        "prototype": "y = Invert Expr( expr, xname, yname )"
    },
    "invupdate": {
        "args": [
            "S",
            "X",
            "<w=1>"
        ],
        "description": "Returns an updated inverse matrix, where the first argument S is a symmetric positive definite matrix with the same number of columns as X, the second argument X is a matrix that contains the rows to add or delete, and the third argument w determines whether to add or delete rows (use 1 to add rows and -1 to delete rows). This function evaluates as S-w*S*X`*Inv(I+w*X*S*X`)*X*S, where I is an identity matrix and Inv(A) means an inverse matrix of A.",
        "prototype": "y = Inv Update( S, X, <w=1> )"
    },
    "inweeks": {
        "args": [
            "<x=1>"
        ],
        "description": "Converts x from a number of weeks to the equivalent number of seconds.",
        "prototype": "y = In Weeks( <x=1> )"
    },
    "inyears": {
        "args": [
            "<x=1>"
        ],
        "description": "Converts x from a number of years to the equivalent number of seconds.",
        "prototype": "y = In Years( <x=1> )"
    },
    "irtability": {
        "args": [
            "Q1",
            "...",
            "Qn",
            "parmMatrix"
        ],
        "description": "Produces scores for the latent variable in an item response theory model with n binary items and a matrix of known parameters, specified by parmMatrix. The parameter matrix should contain as many rows as there are parameters in the model and as many columns as there are items in the analysis.",
        "prototype": "y = IRT Ability( Q1, ..., Qn, parmMatrix )"
    },
    "isaltkey": {
        "args": [],
        "description": "Returns 1 if the Alt key is being pressed; returns 0 otherwise. Intended to be used in graphics callback scripts. On the Mac, Alt means Option key.",
        "prototype": "y = Is Alt Key()"
    },
    "isassociativearray": {
        "args": [
            "x"
        ],
        "description": "Returns 1 is the x argument is an associative array, otherwise returns 0.",
        "prototype": "y = Is Associative Array( x )"
    },
    "isclass": {
        "args": [
            "class",
            "reference"
        ],
        "description": "Returns 1 if the class argument is a class. Otherwise, a 0 is returned.",
        "prototype": "isns = Is Class( class reference )"
    },
    "iscommandkey": {
        "args": [],
        "description": "Returns 1 if the Command key is being pressed; returns 0 otherwise. Intended to be used in graphics callback scripts.",
        "prototype": "y = Is Command Key()"
    },
    "iscontextkey": {
        "args": [],
        "description": "Returns 1 if the Context key is being pressed; returns 0 otherwise. Intended to be used in graphics callback scripts.",
        "prototype": "y = Is Context Key()"
    },
    "iscontrolkey": {
        "args": [],
        "description": "Returns 1 if the Control key is being pressed; returns 0 otherwise. Intended to be used in graphics callback scripts. On the Mac, Control means Command key.",
        "prototype": "y = Is Control Key()"
    },
    "isdirectory": {
        "args": [
            "path"
        ],
        "description": "Determine if the given path is a directory",
        "prototype": "rc = Is Directory( path )"
    },
    "isdirectorywritable": {
        "args": [
            "path"
        ],
        "description": "Determine if the given directory path is writable",
        "prototype": "rc = Is Directory Writable( path )"
    },
    "isempty": {
        "args": [
            "name"
        ],
        "description": "Returns 1 if the variable is undefined or holds the Empty() value.",
        "prototype": "y = Is Empty( name )"
    },
    "isexpr": {
        "args": [
            "x"
        ],
        "description": "Returns 1 if the x argument is an expression, 0 otherwise.",
        "prototype": "y = Is Expr( x )"
    },
    "isfile": {
        "args": [
            "path"
        ],
        "description": "Determine if the given path is a file",
        "prototype": "rc = Is File( path )"
    },
    "isfilewritable": {
        "args": [
            "path"
        ],
        "description": "Determine if the given file path is writable",
        "prototype": "rc = Is File Writable( path )"
    },
    "isjmpliveurlenabled": {
        "args": [
            "url"
        ],
        "description": "Determines if the specified URL can be used in this JMP session. URLs can be enabled and/or disabled using the jmpStartAdmin.jsl script. This does not determine if it is a valid URL, nor if the user is able to login. It only determines if the URL is blocked by JMP.",
        "prototype": "Is JMP Live URL Enabled(url)"
    },
    "isleapyear": {
        "args": [
            "year"
        ],
        "description": "Return whether a given year is a leap year.",
        "prototype": "v = Is Leap Year(year)"
    },
    "islist": {
        "args": [
            "x"
        ],
        "description": "Returns 1 if the x argument is a list, 0 otherwise.",
        "prototype": "y = Is List( x )"
    },
    "islogopen": {
        "args": [],
        "description": "Return result to indicate whether the log window is open",
        "prototype": "Is Log Open()"
    },
    "ismatrix": {
        "args": [
            "x"
        ],
        "description": "Returns 1 if the argument is a matrix, 0 otherwise.",
        "prototype": "y = Is Matrix( x )"
    },
    "ismissing": {
        "args": [
            "x"
        ],
        "description": "Returns 1 if the x argument is a missing value; returns 0 otherwise.",
        "prototype": "y = Is Missing( x )"
    },
    "isname": {
        "args": [
            "x"
        ],
        "description": "Returns 1 if the x argument is a name, 0 otherwise.",
        "prototype": "y = Is Name( x )"
    },
    "isnamespace": {
        "args": [
            "namespace",
            "reference"
        ],
        "description": "Returns 1 if the namespace argument is a namespace; otherwise a 0 is returned.",
        "prototype": "isns = Is Namespace( namespace reference )"
    },
    "isnumber": {
        "args": [
            "x"
        ],
        "description": "Returns 1 if the x argument is a number, 0 otherwise.",
        "prototype": "y = Is Number( x )"
    },
    "isoptionkey": {
        "args": [],
        "description": "Returns 1 if the Option key is being pressed; returns 0 otherwise. Intended to be used in graphics callback scripts.",
        "prototype": "y = Is Option Key()"
    },
    "isscriptable": {
        "args": [
            "x"
        ],
        "description": "Returns 1 if the x argument is a scriptable object, 0 otherwise.",
        "prototype": "tf = Is Scriptable( x )"
    },
    "isshiftkey": {
        "args": [],
        "description": "Returns 1 if the Shift key is being pressed; returns 0 otherwise. Intended to be used in graphics callback scripts.",
        "prototype": "y = Is Shift Key()"
    },
    "isstring": {
        "args": [
            "x"
        ],
        "description": "Returns 1 if the x argument is a string, 0 otherwise.",
        "prototype": "y = Is String( x )"
    },
    "item": {
        "args": [
            "n|[first",
            "last]",
            "s",
            "<delim>",
            "<Unmatched(result",
            "string)>",
            "<Include",
            "Boundary",
            "Delimiters(0|1)>"
        ],
        "description": "Returns the nth item of the s argument, where items are the (possibly empty) sub-strings separated by exactly one of any of the characters specified in the delim argument. If delim is absent, whitespace characters are used. If delim is the empty string, each character is treated as a separate item.",
        "prototype": "w = Item( n|[first last], s, <delim>, <Unmatched(result string)>, <Include Boundary Delimiters(0|1)>)"
    },
    "items": {
        "args": [
            "<[first",
            "last]>",
            "s",
            "<delim>",
            "<Include",
            "Boundary",
            "Delimiters(0|1)>"
        ],
        "description": "Returns a list of (possibly empty) sub-strings separated by exactly one of any of the characters specified in the delim argument. If delim is absent, whitespace characters are used. If delim is the empty string, each character is treated as a separate item.",
        "prototype": "wl = Items(<[first last]>, s, <delim>, <Include Boundary Delimiters(0|1)>)"
    },
    "j": {
        "args": [
            "nr",
            "<nc>",
            "<value>",
            ");",
            "y",
            "=",
            "J(",
            "nr",
            "nc",
            ");",
            "y",
            "=",
            "J(",
            "n"
        ],
        "description": "Creates a matrix of identical values. Default value is 1. Default number of columns is number of rows.",
        "prototype": "y = J( nr, <nc>, <value> ); y = J( nr, nc ); y = J( n )"
    },
    "jmp6sascompatibilitymode": {
        "args": [
            "<mode>"
        ],
        "description": "Returns 1 if JMP version 6 compatibility mode is set and 0 otherwise. To set the compatibility mode, specify a value of 0 or 1 for the mode argument. Setting mode to 1 causes SAS related operators to operate in a mode compatible with JMP version 6 capabilities.",
        "prototype": "y = JMP6 SAS Compatibility Mode( <mode> )"
    },
    "jmpliveconnect": {
        "args": [
            "URL(...)",
            "<Username(...)>",
            "<Password(...)>",
            "<API",
            "Key(...)>",
            "<Prompt(ifNeeded",
            "|",
            "Always",
            "|",
            "Never)>"
        ],
        "description": "Explicitly connect to JMP Live",
        "prototype": "JMP Live Connect( URL(...), <Username(...)>, <Password(...)>, <API Key(...)>, <Prompt(ifNeeded | Always | Never)> )"
    },
    "jmplivedisconnect": {
        "args": [
            "<API",
            "Key(...)>"
        ],
        "description": "Explicitly disconnect from JMP Live",
        "prototype": "JMP Live Disconnect(<API Key(...)>)"
    },
    "jmpproductname": {
        "args": [],
        "description": "Returns 'Standard' or 'Pro' based on the version of the product that has been licensed.",
        "prototype": "y = JMP Product Name()"
    },
    "jmpversion": {
        "args": [],
        "description": "Returns the JMP version (release.revision{.fix}); not available before 6.0.",
        "prototype": "y = JMP Version()"
    },
    "johnsonsbdensity": {
        "args": [
            "q",
            "gamma",
            "delta",
            "theta",
            "sigma"
        ],
        "description": "Returns the density at q of a Johnson Sb distribution, where q is in the interval theta to theta + sigma, delta>0 and gamma between -∞ and +∞ are shape parameters, sigma>0 is a scale parameter, and theta between -∞ and +∞ is a threshold parameter. Note: theta is the lower endpoint of the distribution and sigma is the range of the support of the distribution.",
        "prototype": "y = Johnson Sb Density( q, gamma, delta, theta, sigma )"
    },
    "johnsonsbdistribution": {
        "args": [
            "q",
            "gamma",
            "delta",
            "theta",
            "sigma"
        ],
        "description": "Returns the probability that a Johnson Sb distributed random variable is less than q. (Note: see the Johnson Sb Density() function for parameter descriptions.)",
        "prototype": "p = Johnson Sb Distribution( q, gamma, delta, theta, sigma )"
    },
    "johnsonsbquantile": {
        "args": [
            "p",
            "gamma",
            "delta",
            "theta",
            "sigma"
        ],
        "description": "Returns the quantile from a Johnson Sb distribution, the value for which the probability is p that a random value would be lower. (Note: p is the first parameter. See the Johnson Sb Density() function for parameter descriptions.)",
        "prototype": "q = Johnson Sb Quantile( p, gamma, delta, theta, sigma )"
    },
    "johnsonsldensity": {
        "args": [
            "q",
            "gamma",
            "delta",
            "theta",
            "<sigma=1>"
        ],
        "description": "Returns the density at q of a Johnson Sl distribution, where q is in the interval theta to +∞, delta>0 and gamma between -∞ and +∞ are shape parameters, sigma equal to +1 or -1 is a scale parameter, and theta between -∞ and +∞ is a threshold parameter. Note: When sigma = 1, theta is the lower bound on the distribution, and when sigma=-1, theta is the upper bound. Also, positive sigma implies positive skew, and negative sigma implies negative skew.",
        "prototype": "y = Johnson Sl Density( q, gamma, delta, theta, <sigma=1> )"
    },
    "johnsonsldistribution": {
        "args": [
            "q",
            "gamma",
            "delta",
            "theta",
            "<sigma=1>"
        ],
        "description": "Returns the probability that a Johnson Sl distributed random variable is less than q. (Note: see the Johnson Sl Density() function for parameter descriptions.)",
        "prototype": "p = Johnson Sl Distribution( q, gamma, delta, theta, <sigma=1> )"
    },
    "johnsonslquantile": {
        "args": [
            "p",
            "gamma",
            "delta",
            "theta",
            "<sigma=1>"
        ],
        "description": "Returns the quantile from a Johnson Sl distribution, the value for which the probability is p that a random value would be lower. (Note: p is the first parameter. See the Johnson Sl Density() function for parameter descriptions.)",
        "prototype": "q = Johnson Sl Quantile( p, gamma, delta, theta, <sigma=1> )"
    },
    "johnsonsudensity": {
        "args": [
            "q",
            "gamma",
            "delta",
            "theta",
            "sigma"
        ],
        "description": "Returns the density at q of a Johnson Su distribution, where q is between -∞ and +∞, delta>0 and gamma between -∞ and +∞ are shape parameters, sigma>0 is a scale parameter, and theta between -∞ and +∞ is a threshold parameter.",
        "prototype": "y = Johnson Su Density( q, gamma, delta, theta, sigma )"
    },
    "johnsonsudistribution": {
        "args": [
            "q",
            "gamma",
            "delta",
            "theta",
            "sigma"
        ],
        "description": "Returns the probability that a Johnson Su distributed random variable is less than q. (Note: see the Johnson Su Density() function for parameter descriptions.)",
        "prototype": "p = Johnson Su Distribution( q, gamma, delta, theta, sigma )"
    },
    "johnsonsuquantile": {
        "args": [
            "p",
            "gamma",
            "delta",
            "theta",
            "sigma"
        ],
        "description": "Returns the quantile from a Johnson Su distribution, the value for which the probability is p that a random value would be lower. (Note: p is the first parameter. See the Johnson Su Density() function for parameter descriptions.)",
        "prototype": "q = Johnson Su Quantile( p, gamma, delta, theta, sigma )"
    },
    "journalbox": {
        "args": [
            "journalText"
        ],
        "description": "Constructs a display box from instructions that would be stored in a journal.",
        "prototype": "y = Journal Box( journalText )"
    },
    "jslencrypted": {
        "args": [
            "script"
        ],
        "description": "Embeds an encrypted script within another script. Create an encrypted script by selecting Edit > Encrypt Script from the main menu of a script editor. Enter your passwords and the encrypted text will appear in a new window. Copy this text into a JSL Encrypted('') command to embed the encrypted script in another script.",
        "prototype": "y = JSL Encrypted(script)"
    },
    "jslquote": {
        "args": [
            "script"
        ],
        "description": "Store a JSL script in a variable, including all comments and formatting.",
        "prototype": "y = JSL Quote(script)"
    },
    "jsonliteral": {
        "args": [
            "string"
        ],
        "description": "Returns a valid JSON Boolean or null constant value depending on the specification of the parameter.",
        "prototype": "l = JSON Literal( string )"
    },
    "jsontodatatable": {
        "args": [
            "jsonstring",
            "<",
            "Invisible(",
            "boolean",
            ")",
            "|",
            "Private(",
            "boolean",
            ")",
            ">"
        ],
        "description": "Convert JSON text to a JMP data table",
        "prototype": "dt = JSON To Data Table( jsonstring <, Invisible( boolean ) | Private( boolean ) > )"
    },
    "jsontolist": {
        "args": [
            "jsonstring"
        ],
        "description": "Convert JSON text to a JSL list representing the structure specified by the JSON data.",
        "prototype": "l = JSON To List( jsonstring )"
    },
    "kde": {
        "args": [
            "Vector",
            "<<weights",
            "<<bandwidth(",
            "0",
            ")",
            "<<bandwidth",
            "scale(",
            "1",
            ")",
            "<<bandwidth",
            "selection(",
            "0",
            ")",
            "<<kernel"
        ],
        "description": "Returns a kernel density estimator with automatic bandwidth selection. The optional weights argument must be a vector of the same length as the Vector argument. The optional bandwidth argument must be a nonnegative real number or zero, which forces the use of the value of the bandwidth selection argument. The optional bandwidth scale argument must be a positive real number. The optional bandwidth selection argument must be either 0, 1, 2, or 3, corresponding to Sheather and Jones, Normal Reference, Silverman rule of thumb, or Oversmoother, respectively. The optional kernel argument accepts the values 0, 1, 2, 3, or 4, corresponding to Gaussian, Epanechnikov, Biweight, Triangular, or Rectangular, respectively.",
        "prototype": "{Estimates, Bins, Counts, ActualBandwidth, Error} = KDE( Vector, <<weights, <<bandwidth( 0 ), <<bandwidth scale( 1 ), <<bandwidth selection( 0 ), <<kernel )"
    },
    "kdtable": {
        "args": [
            "[",
            "1",
            "1",
            "1",
            "1",
            "2",
            "1",
            "1",
            "2",
            "2",
            "2",
            "2",
            "2",
            "3",
            "3",
            "3",
            "4",
            "5",
            "6",
            "]"
        ],
        "description": "Returns a table for efficiently looking up near neighbors. The matrix arguments are k-dimensional points. There is no built in limit on the number of dimensions or points.",
        "prototype": "tab = KDTable( [ 1 1 1, 1 2 1, 1 2 2, 2 2 2, 3 3 3, 4 5 6 ] )"
    },
    "labeled": {
        "args": [
            "rs",
            ");",
            "Labeled(",
            "<Row",
            "State(",
            "<r>",
            ")>"
        ],
        "description": "Returns the labeled component of the specified row state value, 0 or 1. If Labeled is used as an L-value, it changes the labeled state of the current (or rth) row in the current data table.",
        "prototype": "y = Labeled( rs ); Labeled( <Row State( <r> )> ) = y"
    },
    "labeledstate": {
        "args": [
            "x"
        ],
        "description": "Returns a row state value with the labeled component set to the specified value.",
        "prototype": "rs = Labeled State( x )"
    },
    "lag": {
        "args": [
            "<x>",
            "<n=1>"
        ],
        "description": "Returns the value of the x argument with the current row set to Row() - n. Being dependent on Row(), Lag() is mainly useful in column formulas.",
        "prototype": "y = Lag( <x>, <n=1> )"
    },
    "lastmodificationdate": {
        "args": [
            "path"
        ],
        "description": "Returns the last modification date of a file or directory.",
        "prototype": "date = Last Modification Date( path )"
    },
    "leastsquaressolve": {
        "args": [
            "y",
            "X",
            "<<noIntercept",
            "<<weights(optionalWeightVector)",
            "<<method('Sweep'|'GInv')"
        ],
        "description": "Returns a list that contains a vector of estimates, Beta = Inverse(X'X)X'y, and the estimated variance matrix of Beta. The optional <<noIntercept argument specifies a no-intercept model. The optional <<weights argument specifies a vector of weights to perform weighted least squares. The optional <<method argument enables you to choose between the default Sweep method and a generalized inverse ('GInv') method for solving the normal equations.",
        "prototype": "{Beta, VarBeta} = Least Squares Solve(y, X, <<noIntercept, <<weights(optionalWeightVector), <<method('Sweep'|'GInv'))"
    },
    "left": {
        "args": [
            "s",
            "n",
            "<filler>"
        ],
        "description": "Returns a truncated or padded version of the original string or list s. The result contains the left n characters or list items, padded with any filler on the right if the length of s is less than n.",
        "prototype": "sub = Left( s, n, <filler> )"
    },
    "length": {
        "args": [
            "x"
        ],
        "description": "Returns the length of the given string (in characters), list (in items), associative array (in number of keys), blob (in bytes), matrix (in elements), or namespace/class (in number of functions and variables).",
        "prototype": "l = Length( x )"
    },
    "lenthpse": {
        "args": [
            "x"
        ],
        "description": "Returns Lenth's pseudo-standard error of the values within a single vector x.",
        "prototype": "y = LenthPSE( x )"
    },
    "less": {
        "args": [
            "x",
            "y",
            "..."
        ],
        "description": "Returns 1 if each argument is less than the next argument; returns 0 otherwise.",
        "prototype": "z = x < y < ... ; z = Less( x, y, ... )"
    },
    "lessequalless": {
        "args": [
            "x",
            "y",
            "..."
        ],
        "description": "Returns 1 if the first argument is less than or equal to the second argument and each argument except the first is less than the next argument; returns 0 otherwise.",
        "prototype": "z = x <= y < ... ; z = LessEqual Less( x, y, ... )"
    },
    "lesslessequal": {
        "args": [
            "x",
            "y",
            "..."
        ],
        "description": "Returns 1 if the first argument is less than the second argument and each argument except the first is less than or equal to the next argument; returns 0 otherwise.",
        "prototype": "z = x < y <= ... ; z = Less LessEqual( x, y, ... )"
    },
    "lessorequal": {
        "args": [
            "x",
            "y",
            "..."
        ],
        "description": "Returns 1 if each argument is less than or equal to the next argument; returns 0 otherwise.",
        "prototype": "z = x <= y <= ... ; z = Less or Equal( x, y, ... )"
    },
    "levdensity": {
        "args": [
            "x",
            "mu",
            "sigma"
        ],
        "description": "Returns the density at x of a largest extreme distribution with location mu and scale sigma.",
        "prototype": "y = LEV Density( x, mu, sigma )"
    },
    "levdistribution": {
        "args": [
            "x",
            "mu",
            "sigma"
        ],
        "description": "Returns the probability at x of a largest extreme distribution with location mu and scale sigma.",
        "prototype": "p = LEV Distribution( x, mu, sigma )"
    },
    "levelcolor": {
        "args": [
            "i",
            ");",
            "y",
            "=",
            "Level",
            "Color(",
            "i",
            "n",
            ");",
            "y",
            "=",
            "Level",
            "Color(",
            "i",
            "n",
            "<theme>",
            ");",
            "y",
            "=",
            "Level",
            "Color(",
            "i",
            "<theme>"
        ],
        "description": "Returns a category color, where i is the category level; n is the number of categories (optional); and theme are the color themes in the Column Info dialog's Value Color combo box. ('JMP Default' is the default theme.) The category index must be >= 1 and <= the number of categories specified in the call or defined by the theme. If the second argument is a character, it is the color theme and n is unspecified.",
        "prototype": "y = Level Color( i ); y = Level Color( i, n ); y = Level Color( i, n, <theme> ); y = Level Color( i, <theme> )"
    },
    "levquantile": {
        "args": [
            "p",
            "mu",
            "sigma"
        ],
        "description": "Returns the quantile at p of a largest extreme distribution with location mu and scale sigma.",
        "prototype": "q = LEV Quantile( p, mu, sigma )"
    },
    "lgamma": {
        "args": [
            "x"
        ],
        "description": "Returns the natural logarithm of the Gamma function of x. Useful when Gamma(x) is too large to use directly.",
        "prototype": "y = LGamma( x )"
    },
    "line": {
        "args": [
            "{x1",
            "y1}",
            "{x2",
            "y2}",
            "...",
            "<",
            "<<Value",
            "Space(",
            "0|1",
            ")",
            ">",
            ");",
            "Line(",
            "xMatrix",
            "yMatrix",
            "<",
            "<<Value",
            "Space(0",
            "|",
            "1)",
            ">"
        ],
        "description": "Draws a line or connected lines. In the default case, the line is drawn linearly between the endpoints. If the Value Space option is set, the line will follow the projection specified by the underlying axis scales.",
        "prototype": "Line( {x1, y1}, {x2, y2}, ..., < <<Value Space( 0|1 ) > ); Line( xMatrix, yMatrix, < <<Value Space(0 | 1) > )"
    },
    "linearregression": {
        "args": [
            "y",
            "X",
            "<<noIntercept",
            "<<printToLog",
            "<<weight(WeightVector)",
            "<<freq(FrequencyVector"
        ],
        "description": "Fits a linear regression for the assumed model y = X * beta + error. The optional <<noIntercept argument specifies a no-intercept model. The optional <<printToLog argument specifies that a summary of fit is printed to the log window. The optional weight argument specifies a vector of weights to perform weighted least squares, and the optional freq argument specifies a vector of frequencies. Returns a list containing a vector of the estimates, a vector of the standard errors, and a list of diagnostics. The list of diagnostics contains vectors of the t statistics and p-values for the estimates, as well as the R-Square and adjusted R-Square values for the regression fit.",
        "prototype": "{Estimates, Std_Error, Diagnostics} = Linear Regression(y, X, <<noIntercept, <<printToLog, <<weight(WeightVector), <<freq(FrequencyVector)"
    },
    "lineseg": {
        "args": [
            "x",
            "values",
            "y",
            "values",
            "<Row",
            "States(",
            "dt",
            "|",
            "dt",
            "[rows]",
            "|",
            "dt",
            "{{rows}",
            "...}",
            "|",
            "{states}",
            ")",
            ">",
            "<",
            "Sizes(",
            "s",
            ")",
            ">",
            ")>"
        ],
        "description": "Returns a display seg with lines connecting all of the x and y values.",
        "prototype": "ls = Line Seg(x values, y values, <Row States( dt | dt,[rows] | dt,{{rows}, ...} | {states} ) >, < Sizes( s ) > )>)"
    },
    "linesseg": {
        "args": [
            "[x1",
            "y1",
            "x2",
            "y2",
            "...]"
        ],
        "description": "Returns a display seg with a sequence of line segments for the passed in x and y values.",
        "prototype": "ls = Lines Seg([x1 y1 x2 y2,...])"
    },
    "linestyle": {
        "args": [
            "x"
        ],
        "description": "Sets the current line style, which can be one of: 0 (Solid), 1 (Dotted), 2 (Dashed), 3 (DashDot), or 4 (DashDotDot).",
        "prototype": "Line Style( x )"
    },
    "lineupbox": {
        "args": [
            "<NCol(",
            "nc",
            ")>",
            "<Spacing(",
            "pixels",
            "<vspace>",
            ")>",
            "displayBoxArgs",
            "..."
        ],
        "description": "Returns a display box to show an alignment of boxes in nc columns. The optional Spacing argument specifies the horizontal and vertical space around the display boxes. If the vspace argument is used, vspace is the vertical space and pixels is the horizontal space.",
        "prototype": "y = Lineup Box( <NCol( nc )>, <Spacing( pixels, <vspace> )>, displayBoxArgs, ... )"
    },
    "lineuprulerbox": {
        "args": [
            "<Widths(",
            "{width1",
            "width2",
            "...}",
            ")>",
            "displayBoxArgs",
            "..."
        ],
        "description": "Returns a display box that sets the column widths of the Lineup Boxes that it contains.",
        "prototype": "y = Lineup Box( <Widths( {width1, width2, ...} )>, displayBoxArgs, ... )"
    },
    "list": {
        "args": [
            "a",
            "b",
            "..."
        ],
        "description": "Creates a list of items without evaluating them.",
        "prototype": "y = {a, b, ...}; y = List( a, b, ... )"
    },
    "listbox": {
        "args": [
            "{item",
            "...}",
            "<width(",
            "pixels",
            ")>",
            "<maxSelected(",
            "9999",
            ")>",
            "<nlines(",
            "12",
            ")>",
            "<script>"
        ],
        "description": "Returns a display box to show a list box of selection items. If item itself is a two-item list containing the item name and a string specifying a modeling type or sorting order, such as 'Ordinal' or 'Ascending', the appropriate icon will show up next to that item in the list box.",
        "prototype": "y = List Box( {item, ...}, <width( pixels )>, <maxSelected( 9999 )>, <nlines( 12 )>, <script> )"
    },
    "ln": {
        "args": [
            "x"
        ],
        "description": "Returns the natural logarithm of x.",
        "prototype": "y = Ln( x )"
    },
    "loaddll": {
        "args": [
            "file",
            "path",
            "<",
            "AutoDeclare(",
            "bool",
            "|",
            "Quiet",
            "|",
            "Verbose)",
            "|",
            "Quiet",
            "|",
            "Verbose",
            ")>"
        ],
        "description": "Loads a DLL pointed to by the specified path.",
        "prototype": "dll = Load DLL( file path, < AutoDeclare( bool | Quiet | Verbose) | Quiet | Verbose )> )"
    },
    "loadtextfile": {
        "args": [
            "path",
            "<Charset('best",
            "guess'",
            "<force('throw'",
            "|",
            "'alert'",
            "|",
            "'silent')>)>",
            "<LineSeparator('\\!N')>",
            "<XMLParse>|<SASODSXML>|<JSON>|<BLOB(",
            "<readOffsetFromBegin(0)>|<readOffsetFromEnd(42)>",
            "<readLength(2147483647)>",
            "<base64Compressed(",
            "1",
            "/*",
            "0:",
            "ascii~hex",
            "*/)>",
            ")>"
        ],
        "description": "Reads a whole text file into a JSL variable. Load Text File() prompts for a filename. Load Text File( path ) returns a string. The XMLParse option converts XML into an expression tree. The SASODSXML parses as SAS ODS default XML. The [{JSON}] option converts JSON into an expression tree. The BLOB argument returns binary data in a JSL Blob variable; optional named parameters to BLOB allow reading a substring from the file.",
        "prototype": "text = Load Text File( path, <Charset('best guess', <force('throw' | 'alert' | 'silent')>)>, <LineSeparator('\\!N')>, <XMLParse>|<SASODSXML>|<JSON>|<BLOB( <readOffsetFromBegin(0)>|<readOffsetFromEnd(42)>, <readLength(2147483647)>, <base64Compressed( 1 /* 0: ascii~hex */)> )> )"
    },
    "loc": {
        "args": [
            "m",
            ");",
            "y",
            "=",
            "Loc(",
            "v",
            "x"
        ],
        "description": "Returns a matrix of the positions of the matrix m that are nonzero. If two arguments are specified, Loc( v, x ) returns a matrix of the positions of the list or matrix v that are equal to the value x.",
        "prototype": "y = Loc( m ); y = Loc( v, x )"
    },
    "local": {
        "args": [
            "{name=value",
            "...}",
            "expression"
        ],
        "description": "Resolves names to local variables.",
        "prototype": "y = Local( {name=value, ...}, expression )"
    },
    "localhere": {
        "args": [
            "expression"
        ],
        "description": "Executes expression with local Names Default To Here(1)",
        "prototype": "y = Local Here( expression )"
    },
    "lockglobals": {
        "args": [
            "name",
            "..."
        ],
        "description": "Locks specified global names, preventing them from being modified or being cleared by the Clear Globals function.",
        "prototype": "Lock Globals( name, ... )"
    },
    "locksymbols": {
        "args": [
            "name",
            "..."
        ],
        "description": "Locks specified global names, preventing them from being modified or being cleared by the Clear Symbols function.",
        "prototype": "Lock Symbols( name, ... )"
    },
    "locmax": {
        "args": [
            "x"
        ],
        "description": "Returns the first position in x of the maximum value.",
        "prototype": "y = Loc Max( x )"
    },
    "locmin": {
        "args": [
            "x"
        ],
        "description": "Returns the first position in x of the minimum value.",
        "prototype": "y = Loc Min( x )"
    },
    "locnonmissing": {
        "args": [
            "matrixArg",
            "...",
            "{listArg}",
            "..."
        ],
        "description": "Returns a vector of row numbers in argument matrix rows that have no missing values, or for lists, those that are nonmissing numbers or nonempty character.",
        "prototype": "y = Loc Nonmissing( matrixArg,...,{listArg},... )"
    },
    "locsorted": {
        "args": [
            "x",
            "y"
        ],
        "description": "Creates a column vector of subscript positions where the values of x have values less than or equal to the values in y based on a binary search. x must be a matrix sorted in ascending order without missing values.",
        "prototype": "idx = Loc Sorted( x, y )"
    },
    "log": {
        "args": [
            "x",
            "<b>"
        ],
        "description": "Returns the base-b logarithm of x or the natural logarithm of x if b is not specified.",
        "prototype": "y = Log( x, <b> )"
    },
    "log10": {
        "args": [
            "x"
        ],
        "description": "Returns the base 10 logarithm of x.",
        "prototype": "y = Log10( x )"
    },
    "log1p": {
        "args": [
            "x"
        ],
        "description": "Returns a more accurate calculation of Log(1 + x) when x is very small.",
        "prototype": "y = Log1P( x )"
    },
    "logcapture": {
        "args": [
            "expr"
        ],
        "description": "Evaluates the expr argument and captures the output that would have appeared in the JMP log window and returns it in a string instead.",
        "prototype": "string = Log Capture( expr )"
    },
    "loggengammadensity": {
        "args": [
            "x",
            "mu",
            "sigma",
            "lambda"
        ],
        "description": "Returns the density at x of a log generalized gamma probability distribution with parameters mu, sigma, and lambda.",
        "prototype": "y = LogGenGamma Density( x, mu, sigma, lambda )"
    },
    "loggengammadistribution": {
        "args": [
            "x",
            "mu",
            "sigma",
            "lambda"
        ],
        "description": "Returns the probability that a log generalized gamma distributed random variable (with parameters mu, sigma, and lambda) is less than x.",
        "prototype": "p = LogGenGamma Distribution( x, mu, sigma, lambda )"
    },
    "loggengammaquantile": {
        "args": [
            "p",
            "mu",
            "sigma",
            "lambda"
        ],
        "description": "Returns the quantile from a log generalized gamma distribution (with parameters mu, sigma, and lambda), the value for which the probability is p that a random value would be lower.",
        "prototype": "q = LogGenGamma Quantile( p, mu, sigma, lambda )"
    },
    "logist": {
        "args": [
            "x"
        ],
        "description": "Returns 1 / (1 + Exp( -x )), which converts a number in the domain -∞...+∞ into range 0...1. The Logist() function is useful in logistic regression.",
        "prototype": "y = Logist( x )"
    },
    "logisticdensity": {
        "args": [
            "x",
            "mu",
            "sigma"
        ],
        "description": "Returns the density at x of a logistic distribution with location mu and scale sigma.",
        "prototype": "y = Logistic Density( x, mu, sigma )"
    },
    "logisticdistribution": {
        "args": [
            "x",
            "mu",
            "sigma"
        ],
        "description": "Returns the probability at x of a logistic distribution with location mu and scale sigma.",
        "prototype": "p = Logistic Distribution( x, mu, sigma )"
    },
    "logisticquantile": {
        "args": [
            "p",
            "mu",
            "sigma"
        ],
        "description": "Returns the quantile at p of a logistic distribution with location mu and scale sigma.",
        "prototype": "q = Logistic Quantile( p, mu, sigma )"
    },
    "logistpercent": {
        "args": [
            "x"
        ],
        "description": "Logist function with result scaled 0 to 100.",
        "prototype": "y = Logist Percent( x )"
    },
    "logit": {
        "args": [
            "p"
        ],
        "description": "Returns the logit of p, which is defined as log(p / (1 - p)).",
        "prototype": "y = Logit( p )"
    },
    "logitpercent": {
        "args": [
            "p"
        ],
        "description": "Logit function with argument 0 to 100, rather than 0 to 1.",
        "prototype": "y = Logit Percent( p )"
    },
    "loglogisticdensity": {
        "args": [
            "x",
            "mu",
            "sigma"
        ],
        "description": "Returns the density at x of a loglogistic distribution with location mu and scale sigma.",
        "prototype": "y = Loglogistic Density( x, mu, sigma )"
    },
    "loglogisticdistribution": {
        "args": [
            "x",
            "mu",
            "sigma"
        ],
        "description": "Returns the probability at x of a loglogistic distribution with location mu and scale sigma.",
        "prototype": "p = Loglogistic Distribution( x, mu, sigma )"
    },
    "loglogisticquantile": {
        "args": [
            "p",
            "mu",
            "sigma"
        ],
        "description": "Returns the quantile at p of a loglogistic distribution with location mu and scale sigma.",
        "prototype": "q = Loglogistic Quantile( p, mu, sigma )"
    },
    "lognormaldensity": {
        "args": [
            "x",
            "mu",
            "sigma"
        ],
        "description": "Returns the density at x of a lognormal distribution with location mu and scale sigma.",
        "prototype": "y = Lognormal Density( x, mu, sigma )"
    },
    "lognormaldistribution": {
        "args": [
            "x",
            "mu",
            "sigma"
        ],
        "description": "Returns the probability at x of a lognormal distribution with location mu and scale sigma.",
        "prototype": "p = Lognormal Distribution( x, mu, sigma )"
    },
    "lognormalquantile": {
        "args": [
            "p",
            "mu",
            "sigma"
        ],
        "description": "Returns the quantile at p of a lognormal distribution with location mu and scale sigma.",
        "prototype": "q = Lognormal Quantile( p, mu, sigma )"
    },
    "longdate": {
        "args": [
            "datetime",
            "<format>"
        ],
        "description": "Returns a long locale-specific representation of a date-time value.",
        "prototype": "s = Long Date( datetime, <format> )"
    },
    "lowercase": {
        "args": [
            "s"
        ],
        "description": "Converts uppercase letters to lowercase letters in the specified string. Rules for upper and lower case are locale dependent.",
        "prototype": "sl = Lowercase( s )"
    },
    "lpsolve": {
        "args": [
            "A",
            "b",
            "c",
            "L",
            "U",
            "neq",
            "nle",
            "nge",
            "<slackVars=0>"
        ],
        "description": "Minimizes the objective function subject to the given constraints and returns a list of two items. The first list item, x, contains the decision variables (and slack variable values if slackVars=1). The second list item, z, contains optimal objective function value (if one exists). The first five arguments are matrices. The A argument is the matrix of constraint coefficients. The b argument is the column of right hand side values of the constraints. The c argument is the vector of cost coefficients of the objective function. The L and U arguments are the lower and upper bounds for the variables, respectively. The neq, nle, and nge arguments are the number of equality constraints, less than or equal constraints, and greater than or equal constraints, respectively. Note that the constraints must be listed as equality first, less than or equal next, and greater than or equal last.",
        "prototype": "{x, z} = LPSolve( A, b, c, L, U, neq, nle, nge, <slackVars=0> )"
    },
    "mail": {
        "args": [
            "'address'",
            "'subject'",
            "'message'",
            "<'attachment",
            "filepath'>",
            "|",
            "{",
            "'attachment",
            "filepath'",
            "...}"
        ],
        "description": "Creates an outgoing e-mail message as specified if the operating system allows doing so. Not all options will work on all operating system versions. See Help for details.",
        "prototype": "Mail( 'address', 'subject', 'message', <'attachment filepath'> | { 'attachment filepath', ...} )"
    },
    "mainmenu": {
        "args": [
            "command",
            "<window",
            "name>"
        ],
        "description": "Executes the specified main menu command.",
        "prototype": "menu = Main Menu( command, <window name> )"
    },
    "makevalidationformula": {
        "args": [
            "rates",
            "<<Stratification",
            "Columns(",
            "cols",
            ")",
            "<<Grouped",
            "Columns(",
            "cols",
            ")",
            "<<",
            "Cutpoint",
            "Column",
            "(",
            "col",
            ")"
        ],
        "description": "This JSL function is used by the Make Validation Column platform to generate formula columns. The rates argument is a 3 by 1 matrix that contains the training, validation, and test rates, respectively.",
        "prototype": "y = Make Validation Formula(rates, <<Stratification Columns( cols ), <<Grouped Columns( cols ), << Cutpoint Column ( col ))"
    },
    "mandelbrot": {
        "args": [
            "n",
            "radius",
            "x",
            "y"
        ],
        "description": "calculates the value of the Mandelbrot function at x,y, stopping after n iterations or when radius exceeded",
        "prototype": "v = Mandelbrot( n, radius, x, y )"
    },
    "mapvalue": {
        "args": [
            "string",
            "|",
            "number",
            "{<key1",
            "value1...>|<{keys}",
            "{values}>}",
            "<Unmatched(value)>"
        ],
        "description": "Evaluate the initial value and return the mapped result or a default.",
        "prototype": "Map Value(string | number, {<key1, value1...>|<{keys},{values}>}, <Unmatched(value)>)"
    },
    "marker": {
        "args": [
            "<rs>",
            "{x1",
            "y1}",
            "{x2",
            "y2}",
            "...",
            ");",
            "Marker(",
            "<rs>",
            "xMatrix",
            "yMatrix"
        ],
        "description": "Draws markers at the indicated coordinates.",
        "prototype": "Marker( <rs>, {x1, y1}, {x2, y2}, ... ); Marker( <rs>, xMatrix, yMatrix )"
    },
    "markerof": {
        "args": [
            "rs",
            ");",
            "Marker",
            "Of(",
            "<Row",
            "State(",
            "<r>",
            ")>"
        ],
        "description": "Returns the marker component of the specified row state value. If Marker Of is used as an L-value, it changes the marker of the current (or rth) row in the current data table.",
        "prototype": "y = Marker Of( rs ); Marker Of( <Row State( <r> )> ) = y"
    },
    "markerseg": {
        "args": [
            "x",
            "y",
            "<",
            "Row",
            "States(",
            "dt",
            "|",
            "dt",
            "[rows]",
            "|",
            "dt",
            "{{rows}",
            "...}",
            "|",
            "{states}",
            ")",
            ">",
            "<",
            "Sizes(",
            "s",
            ")",
            ">"
        ],
        "description": "Returns a display seg with markers for all of the x and y values.",
        "prototype": "me = Marker Seg( x, y, < Row States( dt | dt,[rows] | dt,{{rows}, ...} | {states} ) >, < Sizes( s ) > )"
    },
    "markersize": {
        "args": [
            "n"
        ],
        "description": "Sets the size markers are drawn in the graphics frame. 0 = dot, 1 = small, ....",
        "prototype": "Marker Size( n )"
    },
    "markerstate": {
        "args": [
            "marker"
        ],
        "description": "Returns a row state value with the marker component set to the specified value. The marker argument specifies a marker and can be a positive integer, a character, a positive integer for Unicode character, or a hex character for Unicode character.",
        "prototype": "rs = Marker State( marker )"
    },
    "match": {
        "args": [
            "x",
            "v1",
            "expr1",
            "v2",
            "expr2",
            "...",
            "exprElse"
        ],
        "description": "Evaluates and returns the exprN argument corresponding to the first vN argument that equals x or evaluates and returns the exprElse argument if no value equals x.",
        "prototype": "y = Match( x, v1, expr1, v2, expr2, ..., exprElse )"
    },
    "matchmz": {
        "args": [
            "x",
            "v1",
            "expr1",
            "v2",
            "expr2",
            "...",
            "exprElse"
        ],
        "description": "Evaluates and returns the exprN argument corresponding to the first vN argument that equals x or evaluates and returns the exprElse argument if no value equals x. (The MatchMZ() function behaves the same as the Match() function, except that missing values are treated as 0.)",
        "prototype": "y = MatchMZ( x, v1, expr1, v2, expr2, ..., exprElse )"
    },
    "matlabconnect": {
        "args": [],
        "description": "Returns a MATLAB connection scriptable object.",
        "prototype": "MATLABConnection = MATLAB Connect()"
    },
    "matlabcontrol": {
        "args": [
            "Visible(",
            "bool",
            ")"
        ],
        "description": "Changes the control options for MATLAB",
        "prototype": "MATLAB Control( Visible( bool ) )"
    },
    "matlabexecute": {
        "args": [
            "{",
            "list",
            "of",
            "Inputs",
            "}",
            "{",
            "list",
            "of",
            "Outputs",
            "}",
            "statements"
        ],
        "description": "Sends a list of inputs, executes statements and returns a list of outputs.",
        "prototype": "MATLAB Execute( { list of Inputs }, { list of Outputs }, statements )"
    },
    "matlabget": {
        "args": [
            "name"
        ],
        "description": "Returns data from MATLAB, where the name argument can represent any of the following MATLAB data types ( numeric | string | matrix | list | data frame).",
        "prototype": "y = MATLAB Get( name )"
    },
    "matlabgetgraphics": {
        "args": [
            "format"
        ],
        "description": "Returns the last graphics object written to the MATLAB graph display window in a graphics format specified by the format argument.",
        "prototype": "MATLAB graphics = MATLAB Get Graphics( format )"
    },
    "matlabgetversion": {
        "args": [],
        "description": "Returns the version number of MATLAB being used with the JMP MATLAB interfaces.",
        "prototype": "version = MATLAB Get Version()"
    },
    "matlabinit": {
        "args": [],
        "description": "Initializes the MATLAB Interfaces.",
        "prototype": "MATLAB Init()"
    },
    "matlabisconnected": {
        "args": [],
        "description": "Returns 1 if there is an active MATLAB connection; otherwise, returns 0.",
        "prototype": "connected = MATLAB Is Connected()"
    },
    "matlabjmpnametomatlabname": {
        "args": [
            "JMP",
            "name"
        ],
        "description": "Maps a JMP variable name to a MATLAB variable name using MATLAB variable naming rules.",
        "prototype": "MATLAB name = MATLAB JMP Name To MATLAB Name( JMP name )"
    },
    "matlabsend": {
        "args": [
            "name",
            "<",
            "MATLAB",
            "Name(",
            "name",
            ")>"
        ],
        "description": "Sends data to MATLAB, where the name argument can represent any of the following JMP data types ( numeric | string | matrix | list | data table).",
        "prototype": "MATLAB Send( name <, MATLAB Name( name )> )"
    },
    "matlabsendfile": {
        "args": [
            "filename",
            "<",
            "MATLAB",
            "Name(",
            "name",
            ")>"
        ],
        "description": "Sends a data file to MATLAB, where the filename argument is a string specifying a pathname to the file to be sent to MATLAB.",
        "prototype": "MATLAB Send File( filename <, MATLAB Name( name )> )"
    },
    "matlabsubmit": {
        "args": [
            "statements"
        ],
        "description": "Submit statements to MATLAB. Statements can be in the form of a string value or list of string values.",
        "prototype": "MATLAB Submit( statements )"
    },
    "matlabsubmitfile": {
        "args": [
            "path"
        ],
        "description": "Submits statements to MATLAB using a file specified by the path argument.",
        "prototype": "MATLAB Submit File( path )"
    },
    "matlabterm": {
        "args": [],
        "description": "Terminate the MATLAB Interfaces.",
        "prototype": "MATLAB Term()"
    },
    "matrix": {
        "args": [
            "{{x11",
            "...",
            "x1m}",
            "{...}",
            "{xn1",
            "...",
            "xnm}}",
            ")\r\ny",
            "=",
            "Matrix(",
            "{x1",
            "...",
            "xn}",
            ")\r\ny",
            "=",
            "Matrix(",
            "n",
            "m"
        ],
        "description": "Constructs an n-by-m matrix. If you specify a list of n lists that each contain m row values, the matrix is formed by vertically concatenating the evaluated lists. If you specify a single list of n items, the return value is an n-by-1 column vector. If you specify two integer arguments, the return value is a matrix of zeros that contains n rows and m columns.",
        "prototype": "y = Matrix( {{x11, ..., x1m}, {...}, {xn1, ..., xnm}} )\r\ny = Matrix( {x1, ..., xn} )\r\ny = Matrix( n, m )"
    },
    "matrixbox": {
        "args": [
            "matrix",
            "<",
            "<<Column",
            "Names(",
            "'c1'",
            "'c2'",
            "...",
            ")>",
            "<",
            "<<Row",
            "Names(",
            "'r1'",
            "'r2'",
            "...",
            ")>"
        ],
        "description": "Returns a display box to show a matrix of numbers.",
        "prototype": "y = Matrix Box( matrix, < <<Column Names( 'c1', 'c2', ... )>, < <<Row Names( 'r1', 'r2', ... )> )"
    },
    "matrixmult": {
        "args": [
            "A",
            "B",
            "..."
        ],
        "description": "Performs matrix multiplication. The matrix arguments must be conformable: NCol(a)==NRow(b). Note that A * B also works.",
        "prototype": "y = Matrix Mult( A, B, ... ); y = A * B"
    },
    "matrixrank": {
        "args": [
            "X"
        ],
        "description": "Returns the rank of the matrix X.",
        "prototype": "r = Matrix Rank( X )"
    },
    "matrixtoblob": {
        "args": [
            "matrix",
            "type",
            "bytesEach",
            "endian"
        ],
        "description": "Makes a blob from a matrix by converting the matrix elements to 1, 2, or 4 byte signed or unsigned integers or 4 or 8 byte floating point numbers.",
        "prototype": "m = Matrix To Blob( matrix, type, bytesEach, endian )"
    },
    "max": {
        "args": [
            "x1",
            "...",
            ");",
            "y",
            "=",
            "Maximum(",
            "x1",
            "..."
        ],
        "description": "Returns the maximum value among the arguments or of the values within a single matrix or list argument.",
        "prototype": "y = Max( x1, ... ); y = Maximum( x1, ... )"
    },
    "maximize": {
        "args": [
            "expr",
            "{x1",
            "x2",
            "...}",
            ");\r\nMaximize(",
            "expr",
            "{x1(",
            "low1",
            "up1",
            ")",
            "x2(",
            "low2",
            "up2",
            ")",
            "...}",
            "<<MaxIter(",
            "250",
            ")",
            "<<Tolerance(",
            ".00000001",
            ")",
            "<<details(both",
            "|",
            "returnDetails",
            "|",
            "displaySteps)",
            "<<gradient()",
            "<<hessian()",
            "method(NR",
            "|",
            "SR1)",
            "<<useNumericDeriv(True)"
        ],
        "description": "Finds values for the function's arguments, given in the list {x1, x2, ...}, that maximize the expr expression. You can specify lower and upper bounds for each argument in parentheses following the argument's name. If expr is not a concave function, Maximize might find a local maximum rather than the global maximum. If this is a concern, try multiple starting values. Also, Maximize works best for functions with a continuous second derivative. Additional arguments for the Maximize function enable you to set the maximum number of iterations, tolerance for convergence, and view more details about the optimization. Click the Topic Help button for more information about the optional arguments.",
        "prototype": "Maximize( expr, {x1, x2, ...} );\r\nMaximize( expr, {x1( low1, up1 ), x2( low2, up2 ), ...}, <<MaxIter( 250 ), <<Tolerance( .00000001 ), <<details(both | returnDetails | displaySteps), <<gradient(), <<hessian(), method(NR | SR1), <<useNumericDeriv(True))"
    },
    "maximum": {
        "args": [
            "x1",
            "...",
            ");",
            "y",
            "=",
            "Maximum(",
            "x1",
            "..."
        ],
        "description": "Returns the maximum value among the arguments or of the values within a single matrix or list argument.",
        "prototype": "y = Max( x1, ... ); y = Maximum( x1, ... )"
    },
    "mdyhms": {
        "args": [
            "datetime",
            "<format>"
        ],
        "description": "Returns a representation of a date-time value with the ordering: month, day, year, hour, minute, second.",
        "prototype": "s = MDYHMS( datetime, <format> )"
    },
    "mean": {
        "args": [
            "x1",
            "..."
        ],
        "description": "Returns the arithmetic mean of the arguments or of the values within a single matrix or list argument.",
        "prototype": "y = Mean( x1, ... )"
    },
    "median": {
        "args": [
            "x1",
            "..."
        ],
        "description": "Returns the median of the combined arguments, which can be scalar, matrix or list arguments.",
        "prototype": "y = Median( x1, ... )"
    },
    "metaconnect": {
        "args": [
            "<machine",
            "port>",
            "<authDomain>",
            "<username>",
            "<password>",
            "<named_arguments>",
            ");\r\ny",
            "=",
            "Meta",
            "Connect(",
            "<Profile(",
            "profileName",
            ")>",
            "<Password(",
            "password",
            ")>",
            "<named_arguments>",
            ");\r\ny",
            "=",
            "Meta",
            "Connect(",
            "Environment(",
            "envname",
            ")",
            "<named_arguments>"
        ],
        "description": "Connects to a SAS Metadata Server. If a connection cannot be made using supplied or saved connection information, the user is prompted. Named optional arguments include Prompt(Always|Never|IfNeeded), string-valued argument SASVersion, as well as Boolean-valued arguments CheckPreferenceOnly and ProfileLookup. Returns 1 if connection was successful, 0 otherwise.",
        "prototype": "y = Meta Connect( <machine, port>, <authDomain>, <username>, <password>, <named_arguments> );\r\ny = Meta Connect( <Profile( profileName )>, <Password( password )>, <named_arguments> );\r\ny = Meta Connect( Environment( envname ), <named_arguments> )"
    },
    "metacreateprofile": {
        "args": [
            "profileName",
            "HostName(",
            "string",
            ")",
            "Port(",
            "integer",
            ")",
            "<named_arguments>"
        ],
        "description": "Creates a metadata server connection profile and adds it to the current user's set of saved metadata server connection profiles. There are several optional arguments. String-valued arguments include AuthenticationDomain (or AuthDomain), Description (or Desc), Password, Repository, and UserName. Boolean-valued arguments include Replace (only valid with SAS 9.1.3 Metadata Servers) and UseSingleSignOn (only valid with SAS 9.2 Metadata Servers). Use the Meta Connect() function to connect to the profile. Returns 1 if profile was successfully created; otherwise, returns 0.",
        "prototype": "y = Meta Create Profile( profileName, HostName( string ), Port( integer ), <named_arguments> )"
    },
    "metadeleteprofile": {
        "args": [
            "profileName"
        ],
        "description": "Deletes the named metadata server profile from the current user's set of saved metadata server connection profiles. Returns 1 if profile was successfully deleted, 0 otherwise.",
        "prototype": "y = Meta Delete Profile( profileName )"
    },
    "metadisconnect": {
        "args": [],
        "description": "Disconnects from the active SAS Metadata Server connection.",
        "prototype": "Meta Disconnect()"
    },
    "metagetenvironments": {
        "args": [],
        "description": "Returns a list of the SAS Environments that are defined in the SAS Environments definition file that is configured in Preferences.",
        "prototype": "environmentList = Meta Get Environments()"
    },
    "metagetrepositories": {
        "args": [],
        "description": "Returns a list of the repositories available on the active SAS Metadata Server connection.",
        "prototype": "reposlist = Meta Get Repositories()"
    },
    "metagetservers": {
        "args": [],
        "description": "Returns a list of the SAS Servers that are registered in the SAS Metadata Repository to which JMP is currently connected.",
        "prototype": "serverlist = Meta Get Servers()"
    },
    "metagetstoredprocess": {
        "args": [
            "path"
        ],
        "description": "Returns a stored process scriptable object from the active SAS Metadata Repository.",
        "prototype": "stp = Meta Get Stored Process( path )"
    },
    "metaisconnected": {
        "args": [],
        "description": "Returns 1 if an active connection to a SAS Metadata Server exists, 0 otherwise.",
        "prototype": "c = Meta Is Connected()"
    },
    "metasetrepository": {
        "args": [
            "repositoryName"
        ],
        "description": "Sets the SAS Metadata Repository to use for metadata searches. Returns 1 if successful, 0 otherwise.",
        "prototype": "y = Meta Set Repository( repositoryName )"
    },
    "method": {
        "args": [
            "{",
            "arg1",
            "=",
            "val1",
            "...",
            "}",
            "expression*"
        ],
        "description": "Create a Method within a Class",
        "prototype": "m = Method( { arg1 = val1, ... }, expression* )"
    },
    "min": {
        "args": [
            "x1",
            "...",
            ");",
            "y",
            "=",
            "Minimum(",
            "x1",
            "..."
        ],
        "description": "Returns the minimum value among the arguments or of the values within a single matrix or list argument.",
        "prototype": "y = Min( x1, ... ); y = Minimum( x1, ... )"
    },
    "minimize": {
        "args": [
            "expr",
            "{x1",
            "x2",
            "...}",
            ");\r\nMinimize(",
            "expr",
            "{x1(",
            "low1",
            "up1",
            ")",
            "x2(",
            "low2",
            "up2",
            ")",
            "...}",
            "<<MaxIter(",
            "250",
            ")",
            "<<Tolerance(",
            ".00000001",
            ")",
            "<<details(both",
            "|",
            "returnDetails",
            "|",
            "displaySteps)",
            "<<gradient()",
            "<<Hessian()",
            "<<method(NR",
            "|",
            "SR1)",
            "<<useNumericDeriv(True)"
        ],
        "description": "Finds values for the function's arguments, given in the list {x1, x2, ...}, that minimize the expr expression. You can specify lower and upper bounds for each argument in parentheses following the argument's name. If expr is not a convex function, Minimize might find a local minimum rather than the global minimum. If this is a concern, try multiple starting values. Also, Minimize works best for functions with a continuous second derivative. Additional arguments for the Minimize function enable you to set the maximum number of iterations, tolerance for convergence, and view more details about the optimization. Click the Topic Help button for more information about the optional arguments.",
        "prototype": "Minimize( expr, {x1, x2, ...} );\r\nMinimize( expr, {x1( low1, up1 ), x2( low2, up2 ), ...}, <<MaxIter( 250 ), <<Tolerance( .00000001 ), <<details(both | returnDetails | displaySteps), <<gradient(), <<Hessian(), <<method(NR | SR1), <<useNumericDeriv(True))"
    },
    "minimum": {
        "args": [
            "x1",
            "...",
            ");",
            "y",
            "=",
            "Minimum(",
            "x1",
            "..."
        ],
        "description": "Returns the minimum value among the arguments or of the values within a single matrix or list argument.",
        "prototype": "y = Min( x1, ... ); y = Minimum( x1, ... )"
    },
    "minus": {
        "args": [
            "x"
        ],
        "description": "Negates x, which can be a number, matrix, or list of numbers.",
        "prototype": "y = -x; y = Minus( x )"
    },
    "minute": {
        "args": [
            "datetime"
        ],
        "description": "Returns the minutes part of a date-time value, 0 - 59.",
        "prototype": "min = Minute( datetime )"
    },
    "mod": {
        "args": [
            "x",
            "y"
        ],
        "description": "Returns the remainder from dividing x by y. The remainder has the same sign as x.",
        "prototype": "z = Modulo( x, y )"
    },
    "mode": {
        "args": [
            "list",
            "or",
            "matrix"
        ],
        "description": "Picks the 'most frequent' item from a matrix or list, the lower value for ties",
        "prototype": "y = Mode( list or matrix )"
    },
    "modifiedinternalrateofreturn": {
        "args": [
            "values",
            "finance_rate",
            "reinvest_rate",
            ");\r\nx",
            "=",
            "Modified",
            "Internal",
            "Rate",
            "of",
            "Return(",
            "finance_rate",
            "reinvest_rate",
            "value1",
            "value2",
            "<value3",
            "...>"
        ],
        "description": "Returns the modified internal rate of return for a series of periodic cash flows, taking into account both the cost of the investment and the interest received on reinvestment of cash. Equivalent to the MIRR function in Microsoft Excel. The second prototype of the function accepts all scalar arguments.",
        "prototype": "x = Modified Internal Rate of Return( values, finance_rate, reinvest_rate );\r\nx = Modified Internal Rate of Return( finance_rate, reinvest_rate, value1, value2, <value3, ...> )"
    },
    "modulo": {
        "args": [
            "x",
            "y"
        ],
        "description": "Returns the remainder from dividing x by y. The remainder has the same sign as x.",
        "prototype": "z = Modulo( x, y )"
    },
    "month": {
        "args": [
            "datetime"
        ],
        "description": "Returns the month part of a date-time value, 1 - 12.",
        "prototype": "mon = Month( datetime )"
    },
    "mousebox": {
        "args": [
            "displayBoxArgs"
        ],
        "description": "Returns a box that can make JSL callbacks for mouse actions",
        "prototype": "box = MouseBox( displayBoxArgs )"
    },
    "mousetrap": {
        "args": [
            "dragScript",
            "<mouseUpScript>"
        ],
        "description": "Evaluates the dragScript expression repeatedly while the mouse is pressed within the graph and not handled by another graph object. Before running the script, the globals x and y are set to the mouse value and are restored to their original values afterward. The mouseUpScript expression is run after the mouse button is released.",
        "prototype": "Mousetrap( dragScript, <mouseUpScript> )"
    },
    "movedirectory": {
        "args": [
            "from",
            "to"
        ],
        "description": "Moves a directory from one place to another.",
        "prototype": "rc = Move Directory( from, to )"
    },
    "movefile": {
        "args": [
            "from",
            "to"
        ],
        "description": "Moves a file from one place to another.",
        "prototype": "rc = Move File( from, to )"
    },
    "movetoproject": {
        "args": [
            "<Source(project)>",
            "<Destination(project)>",
            "<Windows({list",
            "of",
            "windows",
            "to",
            "move})>"
        ],
        "description": "Moves one or more windows into a project, out of a project, or between projects. Only one of Source and Destination must be specified; the other will default to the current project. (Use only Source to move windows into the current project, and only Destination to move windows out of it.) A data table window will be moved together with its dependent reports, though only one need be specified in the Windows argument. If omitted, the Windows argument defaults to all open windows in the source project.",
        "prototype": "Move to Project(<Source(project)>, <Destination(project)>, <Windows({list of windows to move})>)"
    },
    "movingaverage": {
        "args": [
            "x",
            "weighting",
            "<before=-1>",
            "<after=0>",
            "<partial",
            "window",
            "is",
            "missing=0>"
        ],
        "description": "Returns a matrix of moving averages for the input matrix. before and after determine the range ('window') of items to average, where before may be -1 to indicate all prior items. If weighting is 1, all items have equal weight. If weighting is 0, items have linearly incremental weights. Otherwise weighting is the parameter for exponential weighting (EWMA). partial window is missing indicates whether averages are reported when not all neighbors are present, which can occur at the ends or near missing values. If partial window is missing is non-zero, missing values are reported instead for such partial windows.",
        "prototype": "y = Moving Average( x, weighting, <before=-1>, <after=0>, <partial window is missing=0> )"
    },
    "multiplefileimport": {
        "args": [],
        "description": "Creates a Multiple File Import object; the object accepts messages to set a folder, filter files, and import.",
        "prototype": "mfiObj = Multiple File Import(); "
    },
    "multiply": {
        "args": [
            "x0",
            "x1",
            "..."
        ],
        "description": "Multiplies all arguments, which can be numbers, matrices, or lists of numbers.",
        "prototype": "y = x0 * x1; y = Multiply( x0, x1, ... )"
    },
    "multiplyto": {
        "args": [
            "y",
            "x"
        ],
        "description": "Multiplies a value to a variable or to a list of variables.",
        "prototype": "y *= x; Multiply To( y, x )"
    },
    "multivariatenormalimpute": {
        "args": [
            "yVec",
            "meanYvec",
            "symCovMat",
            "colMin",
            "colMax"
        ],
        "description": "Returns a response vector with imputed values for the missing values in the yVec vector of responses. Imputations are based on a multivariate normal distribution with mean vector meanYvec and symmetric covariance matrix symCovMat. Optional arguments colMin and colMax are the respective vectors of column minimums and maximums. These arguments provide bounds for the imputations.",
        "prototype": "y = Multivariate Normal Impute( yVec, meanYvec, symCovMat, colMin, colMax )"
    },
    "munger": {
        "args": [
            "s",
            "startPos",
            "findStringOrNChars",
            "<replaceString>"
        ],
        "description": "Searches the s argument for a substring or position depending on combination of arguments.",
        "prototype": "r = Munger( s, startPos, findStringOrNChars, <replaceString> )"
    },
    "name": {
        "args": [
            "string"
        ],
        "description": "A name is simply something to call an item. Names are used for both variables and functions, and can be used directly in scripts as long as certain rules are followed. If the name begins with an alphabetic character or underscore, and continues with alphanumeric characters, whitespace, Unicode mathematical symbols and certain punctuation (apostrophes (’), percent signs (%), periods (.), backslashes (\\), and underscores (_)), then the name can be used directly in scripts. Names that do not follow these rules can be used by using the Name() keyword.",
        "prototype": "Name(string)"
    },
    "nameexpr": {
        "args": [
            "x"
        ],
        "description": "Returns the value of a symbol, without evaluating it if it is an expression.",
        "prototype": "y = Name Expr( x )"
    },
    "namesdefaulttohere": {
        "args": [
            "boolean"
        ],
        "description": "Determines where unresolved names are stored, either as a global/local ( 0 ) or in the Here: namespace ( 1 ).",
        "prototype": "Names Default To Here( boolean )"
    },
    "namespace": {
        "args": [
            "namespace",
            "reference"
        ],
        "description": "Return a reference to the namespace specified by the name argument.",
        "prototype": "ns = Namespace( namespace reference )"
    },
    "namespaceexists": {
        "args": [
            "namespace",
            "reference"
        ],
        "description": "Returns 1 if the namespace specified by the name argument exists; otherwise a 0 is returned.",
        "prototype": "nsexists = Namespace Exists( namespace reference )"
    },
    "narg": {
        "args": [
            "expr"
        ],
        "description": "Returns the number of arguments of the evaluated expression head.",
        "prototype": "n = N Arg( expr )"
    },
    "nargexpr": {
        "args": [
            "expr"
        ],
        "description": "Returns the number of arguments of the expression head. This function is deprecated. Please use N Arg() instead.",
        "prototype": "n = N Arg Expr( expr )"
    },
    "nchoosek": {
        "args": [
            "n",
            "k"
        ],
        "description": "Returns n! / (k! * (n - k)!), which is the number of ways to choose k items out of n, ignoring order.",
        "prototype": "m = N Choose K( n, k )"
    },
    "nchoosekmatrix": {
        "args": [
            "n",
            "k"
        ],
        "description": "Creates a matrix of nChooseK(n,k) rows and k columns forming all the combinations of k integers from 1 to n.",
        "prototype": "m = NChooseK Matrix( n, k )"
    },
    "ncol": {
        "args": [
            ");",
            "y",
            "=",
            "N",
            "Col(",
            "dataTable",
            ");",
            "y",
            "=",
            "N",
            "Col(",
            "matrix"
        ],
        "description": "Returns the number of columns in the current data table, a specified data table, or a matrix.",
        "prototype": "y = N Col(); y = N Col( dataTable ); y = N Col( matrix )"
    },
    "ncols": {
        "args": [
            ");",
            "y",
            "=",
            "N",
            "Cols(",
            "dataTable",
            ");",
            "y",
            "=",
            "N",
            "Col(",
            "matrix"
        ],
        "description": "Returns the number of columns in the current data table, a specified data table, or a matrix.",
        "prototype": "y = N Cols(); y = N Cols( dataTable ); y = N Col( matrix )"
    },
    "negbinomialdistribution": {
        "args": [
            "p",
            "n",
            "k"
        ],
        "description": "Returns the probability that a negative binomially distributed random variable is less than or equal to k, where the probability of success is p and the number of successes is n.",
        "prototype": "cumprob = Neg Binomial Distribution( p, n, k )"
    },
    "negbinomialprobability": {
        "args": [
            "p",
            "n",
            "k"
        ],
        "description": "Returns the probability that a negative binomially distributed random variable is equal to k, where the probability of success is p and the number of successes is n.",
        "prototype": "prob = Neg Binomial Probability( p, n, k )"
    },
    "netpresentvalue": {
        "args": [
            "rate",
            "values",
            ");\r\nx",
            "=",
            "Net",
            "Present",
            "Value(",
            "rate",
            "value1",
            "value2",
            "<value3",
            "...>"
        ],
        "description": "Returns the net present value of an investment by using a discount rate and a series of future payments (negative values) and income (positive values). The values argument is a one dimensional matrix. Equivalent to the NPV function in Microsoft Excel. The second prototype of the function accepts all scalar arguments.",
        "prototype": "x = Net Present Value( rate, values );\r\nx = Net Present Value( rate, value1, value2, <value3, ...> )"
    },
    "newcasaction": {
        "args": [
            "..."
        ],
        "description": "Creates a CAS Action.",
        "prototype": "action = New CAS Action(...)"
    },
    "newcasdatastepaction": {
        "args": [
            "..."
        ],
        "description": "Creates a CAS DATA step action.",
        "prototype": "action = New CAS DATA Step Action(...)"
    },
    "newcasserver": {
        "args": [
            "<...>"
        ],
        "description": "Creates a new CAS server.",
        "prototype": "cas = New CAS Server(<...>)"
    },
    "newcolumn": {
        "args": [
            "name",
            "<'Numeric'|'Character'|'RowState'|'Expression'>",
            "<'Continuous'|'Ordinal'|'Nominal'|'Multiple",
            "Response'|'Unstructured",
            "Text'|'Vector'|'None'>",
            "<Width(",
            "n",
            ")|Format(format",
            "name",
            "width",
            "precision)>",
            "<actions>"
        ],
        "description": "Creates a new column in the current data table. The optional actions arguments are any messages that data columns support.",
        "prototype": "dc = New Column( name, <'Numeric'|'Character'|'RowState'|'Expression'>, <'Continuous'|'Ordinal'|'Nominal'|'Multiple Response'|'Unstructured Text'|'Vector'|'None'>, <Width( n )|Format(format name, width, precision)>, <actions> )"
    },
    "newcolumnbytextmatching": {
        "args": [
            "Column(:name)",
            "Set",
            "Regex()",
            "<Output",
            "Column",
            "Name('Name')>",
            "<Use",
            "Result(0",
            "|",
            "1)>"
        ],
        "description": "Creates a new column by performing a regular expression pattern match on an existing column.",
        "prototype": "dc = New Column by Text Matching( Column(:name), Set Regex(), <Output Column Name('Name')>, <Use Result(0 | 1)> )"
    },
    "newcustomfunction": {
        "args": [
            "namespace",
            "name",
            "function",
            "definition"
        ],
        "description": "Create a new custom function object. A custom function will be colorized in the script editor and show up in the Scripting Index. The required information for a custom user function are a namespace (to prevent collisions with global functions), a name, and a function definition. Other help information can be added using messages. Use the Add Custom Functions command to publish the new function into the JMP environment.",
        "prototype": "f=New Custom Function(namespace, name, function definition)"
    },
    "newhttprequest": {
        "args": [
            "URL(...)",
            "Method(...)",
            "<Form(<Fields(...)>",
            "<Files(...)>)>",
            "|",
            "<File(...)>",
            "|",
            "<Blob(...)>",
            "|",
            "<JSON(...)>",
            "<QueryString(...)>",
            "<Headers(...)>",
            "<Username(...)>",
            "<Password(...)>"
        ],
        "description": "Creates a request to send to a web service.",
        "prototype": "obj = New HTTP Request(URL(...), Method(...), <Form(<Fields(...)>, <Files(...)>)> | <File(...)> | <Blob(...)> | <JSON(...)>, <QueryString(...)>, <Headers(...)>, <Username(...)>, <Password(...)>)"
    },
    "newimage": {
        "args": [
            ")\r\nimg",
            "=",
            "New",
            "Image(",
            "width",
            "height",
            ")\r\nimg",
            "=",
            "New",
            "Image(",
            "pathname",
            ")\r\nimg",
            "=",
            "New",
            "Image(",
            "picture",
            ")\r\nimg",
            "=",
            "New",
            "Image(",
            "matrix",
            "of",
            "JSL",
            "color",
            "pixels",
            ")",
            "\r\nimg",
            "=",
            "New",
            "Image(",
            "rgb|r|g|rgba",
            "{i",
            "i",
            "i}"
        ],
        "description": "Returns a new image which can then be edited through JSL commands. If a path is specified to an existing image file, the file should be a .JPG, .PNG, .GIF, .BMP or .TIF file.",
        "prototype": "img = New Image()\r\nimg = New Image( width, height )\r\nimg = New Image( pathname )\r\nimg = New Image( picture )\r\nimg = New Image( matrix of JSL color pixels ) \r\nimg = New Image( rgb|r|g|rgba, {i, i, i} )"
    },
    "newnamespace": {
        "args": [
            "<name>",
            "<list",
            "of",
            "expressions>"
        ],
        "description": "Create a new namespace with the name specified by the name argument or with an anonymous name if name is not specified.",
        "prototype": "ns = New Namespace( <name>, <list of expressions> )"
    },
    "newoauth2": {
        "args": [],
        "description": "Creates a new OAuth2 authorization.",
        "prototype": "oauth2 = New OAuth2()"
    },
    "newoauth2token": {
        "args": [
            "Account('jmpgoogldev@gmail.com')",
            "Client",
            "ID('test')",
            "Client",
            "Secret('test",
            "2')",
            "Refresh",
            "Token('')",
            "Token",
            "URL('')"
        ],
        "description": "Creates an OAuth2 Token for securely accessing data across many different web APIs.",
        "prototype": "token = New OAuth2 Token( Account('jmpgoogldev@gmail.com'), Client ID('test'), Client Secret('test 2'), Refresh Token(''), Token URL(''))"
    },
    "newobject": {
        "args": [
            "'class",
            "name'",
            "|",
            "class",
            "name",
            "|",
            "class",
            "reference(",
            "constructor",
            "arguments*",
            ")"
        ],
        "description": "Creates an instance object of a class.",
        "prototype": "New Object( 'class name' | class name | class reference( constructor arguments* ) )"
    },
    "newproject": {
        "args": [
            "<project",
            "messages>"
        ],
        "description": "Creates a new empty project window. One or more project messages can be included as arguments in order to create a project in one step.",
        "prototype": "project = new Project( <project messages> )"
    },
    "newsqlquery": {
        "args": [
            "Connection(",
            "'ODBC:my_connection_string'",
            ")",
            "Select(",
            "Column(",
            "'mycolumn'",
            "'t1'",
            ")",
            ")",
            "From(",
            "Table(",
            "'my_table'",
            "Schema(",
            "'my_schema'",
            ")",
            "Alias(",
            "'t1'",
            ")",
            ")",
            ")",
            ");\r\n\r\n\t\tobj",
            "=",
            "New",
            "SQL",
            "Query(",
            "Connection(",
            "'ODBC:my_connection_string;'",
            ")",
            "CustomSQL(",
            "'SELECT",
            "c1",
            "c2",
            "c3",
            "FROM",
            "my_table;'",
            ")",
            ");\r\n\r\n\t\tobj",
            "=",
            "New",
            "SQL",
            "Query(",
            "Connection(",
            "'SAS:my_connection_string'",
            ")",
            "Select(",
            "Column(",
            "'mycolumn'",
            "'t1'",
            ")",
            ")",
            "From(",
            "Table(",
            "'my_table'",
            "Schema(",
            "'my_schema'",
            ")",
            "Alias(",
            "'t1'",
            ")",
            ")",
            ")",
            ");\r\n\r\n\t\tobj",
            "=",
            "New",
            "SQL",
            "Query(",
            "Connection(",
            "'SAS:my_connection_string;'",
            ")",
            "CustomSQL(",
            "'SELECT",
            "c1",
            "c2",
            "c3",
            "FROM",
            "my_table;'",
            ")"
        ],
        "description": "Creates an SQL Query object for the connection, columns and table specified, or for the custom SQL query specified. Use Query Builder to generate scripts that create queries.",
        "prototype": "obj = New SQL Query( Connection( 'ODBC:my_connection_string' ), Select( Column( 'mycolumn', 't1' ) ), From( Table( 'my_table', Schema( 'my_schema' ), Alias( 't1' ) ) ) );\r\n\r\n\t\tobj = New SQL Query( Connection( 'ODBC:my_connection_string;' ), CustomSQL( 'SELECT c1, c2, c3 FROM my_table;' ) );\r\n\r\n\t\tobj = New SQL Query( Connection( 'SAS:my_connection_string' ), Select( Column( 'mycolumn', 't1' ) ), From( Table( 'my_table', Schema( 'my_schema' ), Alias( 't1' ) ) ) );\r\n\r\n\t\tobj = New SQL Query( Connection( 'SAS:my_connection_string;' ), CustomSQL( 'SELECT c1, c2, c3 FROM my_table;' ) );\r\n"
    },
    "newtable": {
        "args": [
            "name",
            "<visibility('private'|'invisible'|'visible')>",
            "<actions>"
        ],
        "description": "Creates a new data table. 'Invisible' hides the data table from view but lists it in the JMP Home Window. 'Private' hides the table completely. 'Visible' is the default, and creates a normal table that is visible and listed in the JMP Home Window. The optional actions arguments are any messages that data tables support.",
        "prototype": "dt = New Table( name, <visibility('private'|'invisible'|'visible')>, <actions> )"
    },
    "newwebreport": {
        "args": [
            "..."
        ],
        "description": "Creates an interactive HTML report.",
        "prototype": "obj = New Web Report(...)"
    },
    "newwindow": {
        "args": [
            "title",
            "<",
            "<",
            "<<Modal>",
            "<",
            "<<Return",
            "Result>",
            "<",
            "<<On",
            "Open(",
            "expr",
            ")>",
            "<",
            "<<On",
            "Close(",
            "expr",
            ")>",
            "<",
            "<On",
            "Validate(",
            "expr",
            ")>",
            ">",
            "<Show",
            "Menu(",
            "0|1",
            ")>",
            "<Show",
            "Toolbars(",
            "0|1",
            ")>",
            "<Suppress",
            "AutoHide(",
            "0|1",
            ")>",
            "<",
            "<<Script",
            "'script'",
            "|",
            "<<Script('script')>",
            "<",
            "<<Journal>",
            "displayBox"
        ],
        "description": "Creates a window containing the specified display box. If the Modal argument is specified, the new window is created as a modal window. On Open, On Validate, and Return Result are available only for modal windows. On Open() evaluates its expression when the window is created. If On Close() returns zero, the window is prevented from closing. On Validate() runs its expression when the OK button is clicked. If the expression returns true, the window is closed. Otherwise, the window remains open. Return Result changes the window's return value when it closes to match that of the deprecated Dialog() function. The options Show Menu, Show Toolbars, and Suppress AutoHide are available only on Windows.",
        "prototype": "y = New Window( title, < < <<Modal>, < <<Return Result>, < <<On Open( expr )>, < <<On Close( expr )>, < <On Validate( expr )> >, <Show Menu( 0|1 )>, <Show Toolbars( 0|1 )>, <Suppress AutoHide( 0|1 )>, < <<Script, 'script' | <<Script('script')>, < <<Journal>, displayBox )"
    },
    "nitems": {
        "args": [
            "x"
        ],
        "description": "Returns the number of items in a list, the number of elements in a matrix, the number of keys in an associative array, the number of functions and variables in a namespace, the number of methods and variables in a class object, or the number of children of a display box.",
        "prototype": "y = N Items( x )"
    },
    "nmissing": {
        "args": [
            "x1",
            "x2",
            "..."
        ],
        "description": "Returns the number of missing values among arguments.",
        "prototype": "y = N Missing( x1, x2, ... )"
    },
    "normalbivdistribution": {
        "args": [
            "x",
            "y",
            "r",
            "<mu1=0>",
            "<s1=1>",
            "<mu2=0>",
            "<s2=1>"
        ],
        "description": "Computes the probability that an observation (X, Y) is less than or equal to (x, y) with correlation coefficient r where X is marginally normally distributed with mean mu1 and standard deviation s1 and Y is marginally normally distributed with mean mu2 and standard deviation s2. If mu1, s1, mu2, and s2 are not given, the function assumes the standard normal bivariate distribution with mu1=0, s1=1, mu2=0, and s2=1.",
        "prototype": "y = Normal Biv Distribution( x, y, r, <mu1=0>, <s1=1>, <mu2=0>, <s2=1> )"
    },
    "normalcontour": {
        "args": [
            "prob",
            "meanMatrix",
            "stdMatrix",
            "corrMatrix",
            "<colorsMatrix>",
            "<fill=0>"
        ],
        "description": "Draws normal probability contour(s) for k populations and two variables. The prob argument can be either a scalar probability or a matrix of probabilities. The meanMatrix and stdsMatrix arguments are k by 2 matrices, and the corrMatrix argument is a k by 1 vector. The colorsMatrix argument specifies the color(s) for the k contour(s); colors must be specified as JSL colors (either JSL color integer values or return values of JSL color functions such as the RGB Color() or HLS Color() functions). The fill argument specifies the amount of transparency for the contour fill color.",
        "prototype": "Normal Contour( prob, meanMatrix, stdMatrix, corrMatrix, <colorsMatrix>, <fill=0> )"
    },
    "normaldensity": {
        "args": [
            "q",
            "<mu=0>",
            "<sigma=1>"
        ],
        "description": "Returns the density at q of a Normal distribution with mean mu and standard deviation sigma.",
        "prototype": "y = Normal Density( q, <mu=0>, <sigma=1> )"
    },
    "normaldistribution": {
        "args": [
            "q",
            "<mu=0>",
            "<sigma=1>"
        ],
        "description": "Returns the probability that a normally distributed random variable is less than q.",
        "prototype": "p = Normal Distribution( q, <mu=0>, <sigma=1> )"
    },
    "normalintegrate": {
        "args": [
            "muVector",
            "sigmaMatrix",
            "expr",
            "x",
            "NStrata",
            "NSim"
        ],
        "description": "Returns the result of radial-spherical integration for smooth functions of multivariate normal variables. The basic idea is the same as one method in Genz and Monahan(1996). But Radau-Gauss-Laguerre type quadrature is used for the radial direction.",
        "prototype": "{mean, var} = Normal Integrate( muVector, sigmaMatrix, expr, x, NStrata, NSim )"
    },
    "normallogcdistribution": {
        "args": [
            "x",
            "<mean=0>",
            "<std",
            "dev=1>"
        ],
        "description": "Returns the log of 1 - Normal distribution at x with mean mu and standard deviation sigma.",
        "prototype": "y = Normal Log CDistribution( x, <mean=0>, <std dev=1> )"
    },
    "normallogdensity": {
        "args": [
            "x",
            "<mu=0>",
            "<sigma=1>"
        ],
        "description": "Returns the log of the Normal probability density at x with mean mu and standard deviation sigma.",
        "prototype": "y = Normal Log Density( x, <mu=0>, <sigma=1>)"
    },
    "normallogdistribution": {
        "args": [
            "x",
            "<mean=0>",
            "<std",
            "dev=1>"
        ],
        "description": "Returns the log of the Normal distribution at x with mean mu and standard deviation sigma.",
        "prototype": "y = Normal Log Distribution( x, <mean=0>, <std dev=1> )"
    },
    "normalmixturedensity": {
        "args": [
            "q",
            "meanvec",
            "sdvec",
            "probvec"
        ],
        "description": "Returns the density at q of a normal mixture distribution with group means meanvec, group standard deviations sdvec, and group probabilities probvec. Here meanvec, sdvec, and probvec are all vectors of the same size.",
        "prototype": "y = Normal Mixture Density(q, meanvec, sdvec, probvec)"
    },
    "normalmixturedistribution": {
        "args": [
            "q",
            "meanvec",
            "sdvec",
            "probvec"
        ],
        "description": "Returns the probability that a normal mixture distributed variable with group means meanvec, group standard deviations sdvec, and group probabilities probvec is less than q. Here meanvec, sdvec, and probvec are all vectors of the same size.",
        "prototype": "y = Normal Mixture Distribution(q, meanvec, sdvec, probvec)"
    },
    "normalmixturequantile": {
        "args": [
            "p",
            "meanvec",
            "sdvec",
            "probvec"
        ],
        "description": "Returns the quantile from a normal mixture distribution, the values for which the probability is p that a random value would be lower.",
        "prototype": "q = Normal Mixture Quantile(p, meanvec, sdvec, probvec)"
    },
    "normalquantile": {
        "args": [
            "p",
            "<mu=0>",
            "<sigma=1>",
            ");",
            "q",
            "=",
            "Probit(",
            "p"
        ],
        "description": "Returns the quantile from a Normal distribution, the value for which the probability is p that a random value would be lower.",
        "prototype": "q = Normal Quantile( p, <mu=0>, <sigma=1> ); q = Probit( p )"
    },
    "not": {
        "args": [
            "x"
        ],
        "description": "Returns the logical NOT of x: 1 if x is zero, missing if x is missing, and 0 otherwise.",
        "prototype": "y = !x; y = Not( x )"
    },
    "notequal": {
        "args": [
            "x",
            "y",
            "..."
        ],
        "description": "Returns 1 if each argument is not equal to the next argument; returns 0 otherwise.",
        "prototype": "z = x != y != ...; z = Not Equal( x, y, ... )"
    },
    "nrow": {
        "args": [
            ");",
            "y",
            "=",
            "N",
            "Row(",
            "dt",
            ");",
            "y",
            "=",
            "N",
            "Row(",
            "matrix"
        ],
        "description": "Returns the number of rows in the current data table, a specified data table, or a matrix.",
        "prototype": "y = N Row(); y = N Row( dt ); y = N Row( matrix )"
    },
    "nrows": {
        "args": [
            ");",
            "y",
            "=",
            "N",
            "Rows(",
            "dt",
            ");",
            "y",
            "=",
            "N",
            "Rows(",
            "matrix"
        ],
        "description": "Returns the number of rows in the current data table, a specified data table, or a matrix.",
        "prototype": "y = N Rows(); y = N Rows( dt ); y = N Rows( matrix )"
    },
    "ntable": {
        "args": [],
        "description": "Returns the number of currently open data tables.",
        "prototype": "n = N Table()"
    },
    "nthdayofweekinthemonth": {
        "args": [
            "datetime"
        ],
        "description": "Returns an integer that represents the number of instances of the day of the week of the datetime argument that have occurred in the month. For example, November 28, 2019 is the 4th Thursday of the month, so the function returns 4.",
        "prototype": "n = Nth Day Of Week in the Month( datetime )"
    },
    "num": {
        "args": [
            "s"
        ],
        "description": "Converts s to a number. Returns missing if the conversion fails.",
        "prototype": "y = Num( s )"
    },
    "number": {
        "args": [
            "x1",
            "..."
        ],
        "description": "Returns the number of non-missing arguments or values within a single matrix or list argument.",
        "prototype": "y = Number( x1, ... )"
    },
    "numbercolbox": {
        "args": [
            "title",
            "numbers"
        ],
        "description": "Returns a display box to show the numbers specified by the numbers argument, which can be a list or matrix.",
        "prototype": "y = Number Col Box( title, numbers )"
    },
    "numbercoleditbox": {
        "args": [
            "title",
            "numbers"
        ],
        "description": "Returns a display box to show the numbers specified by the numbers argument, which can be a list or matrix.",
        "prototype": "y = Number Col Edit Box( title, numbers )"
    },
    "numbereditbox": {
        "args": [
            "initValue",
            "<width>"
        ],
        "description": "Returns an edit box that accepts only numeric input. Specify the optional width argument to set the width of the box in characters.",
        "prototype": "y = Number Edit Box( initValue, <width> )"
    },
    "numberofperiods": {
        "args": [
            "rate",
            "pmt",
            "pv",
            "<fv=0>",
            "<type=0>"
        ],
        "description": "Returns the number of periods for an investment based on periodic, constant payments and a constant interest rate. The type argument is 0 for end-of-period payments and 1 for beginning-of-period payments. Equivalent to the NPER function in Microsoft Excel.",
        "prototype": "x = Number of Periods( rate, pmt, pv, <fv=0>, <type=0> )"
    },
    "numderiv": {
        "args": [
            "f(",
            "x",
            "...",
            ")",
            "<parnum>"
        ],
        "description": "Returns the numerical derivative of the f( x,... ) function with respect to one of its arguments. You can specify that argument as the second argument in the Num Deriv function. If no second argument is specified, the derivative is taken with respect to the function's first argument. The derivative is evaluated using numeric values specified in the f( x,... ) function expression.",
        "prototype": "y = Num Deriv( f( x, ... ),  <parnum>)"
    },
    "numderiv2": {
        "args": [
            "f(",
            "x",
            "...",
            ")"
        ],
        "description": "Returns the numerical second derivative of the f( x,... ) function with respect to x. The derivative is evaluated using numeric values specified in the f( x,... ) function expression.",
        "prototype": "y = Num Deriv2( f( x, ... ) )"
    },
    "open": {
        "args": [
            "filePath",
            "<data",
            "table",
            "options",
            "|",
            "Excel",
            "import",
            "options",
            "|",
            "text",
            "import",
            "options",
            "|",
            "SAS",
            "import",
            "options",
            "|",
            "HTML",
            "import",
            "options",
            "|",
            "esriShapeFile",
            "import",
            "options",
            "|",
            "PDF",
            "import",
            "options",
            "|",
            "other",
            "file",
            "options",
            ">"
        ],
        "description": "Returns a reference to a data table or other JMP file or object created from a file. If no path is specified, the Open dialog appears. Refer to the Syntax Reference for a complete description of available options.",
        "prototype": "Open( filePath, <data table options | Excel import options | text import options | SAS import options | HTML import options | esriShapeFile import options | PDF import options | other file options > )"
    },
    "opendatabase": {
        "args": [
            "dataSourceName|'Connect",
            "Dialog'",
            "'SELECT",
            "...'|'SQLFILE=...'|tableName",
            "<invisible",
            "|",
            "private>",
            "<outputTableName>"
        ],
        "description": "Opens a database using ODBC, runs the given SQL, and puts data into a data table with the given output table name.",
        "prototype": "dt = Open Database( dataSourceName|'Connect Dialog', 'SELECT ...'|'SQLFILE=...'|tableName, <invisible | private>, <outputTableName> )"
    },
    "opendatafeed": {
        "args": [
            "..."
        ],
        "description": "Creates an object and window you can send messages to manage real-time data feeds.",
        "prototype": "y = Open Datafeed( ... )"
    },
    "openhelp": {
        "args": [
            "'Help'",
            "|",
            "'Statistics",
            "Index'",
            "|",
            "'Scripting",
            "Index'",
            "..."
        ],
        "description": "Opens the help index window, statistics index, or the scripting index",
        "prototype": "w = Open Help( 'Help' | 'Statistics Index' | 'Scripting Index', ... )"
    },
    "openlog": {
        "args": [
            "<bring",
            "window",
            "to",
            "top>"
        ],
        "description": "Open the log window",
        "prototype": "Open Log( <bring window to top> )"
    },
    "or": {
        "args": [
            "x1",
            "x2",
            "..."
        ],
        "description": "Returns the logical OR of all arguments: 1 if any arguments are nonzero and 0 otherwise.",
        "prototype": "y = x1 | x2; y = Or( x1, x2, ... )"
    },
    "ormz": {
        "args": [
            "x1",
            "x2",
            "..."
        ],
        "description": "Returns the logical OR of all arguments with missing values treated as zeros: 1 if any arguments are nonzero and 0 otherwise.",
        "prototype": "y = OrMZ( x1, x2, ... )"
    },
    "ortho": {
        "args": [
            "A",
            "<Centered(",
            "0",
            ")>",
            "<Scaled(",
            "1",
            ")>"
        ],
        "description": "Orthogonalizes the columns of a matrix. Center option makes them sum to zero. Scale option makes them unit length.",
        "prototype": "L = Ortho( A, <Centered( 0 )>, <Scaled( 1 )> )"
    },
    "orthopoly": {
        "args": [
            "V",
            "order"
        ],
        "description": "Returns orthogonal polynomials of vector V up to order specified by the order argument. The V argument can be a row or column vector. Scale option makes them unit length.",
        "prototype": "L = Ortho Poly( V, order )"
    },
    "outlinebox": {
        "args": [
            "title",
            "<command",
            "script",
            "pairs",
            "list>",
            "displayBox",
            "..."
        ],
        "description": "Creates an outline element in the report, returning the display box reference. To include a menu in the outline node, specify the command script pairs list, a list specifying menu commands and associated scripts.",
        "prototype": "y = Outline Box( title, <command script pairs list>, displayBox, ... )"
    },
    "oval": {
        "args": [
            "left",
            "top",
            "right",
            "bottom",
            "<fill=0>"
        ],
        "description": "Draws an oval within the specified rectangle, filled if fill is nonzero.",
        "prototype": "Oval( left, top, right, bottom, <fill=0> )"
    },
    "pagebreakbox": {
        "args": [],
        "description": "Creates a display box that forces a page break.",
        "prototype": "Page Break Box()"
    },
    "panelbox": {
        "args": [
            "title",
            "displayBoxArgs"
        ],
        "description": "Returns a display box to label and enclose the argument display box.",
        "prototype": "y = Panel Box( title, displayBoxArgs )"
    },
    "parallelassign": {
        "args": [
            "{",
            "thread_local_var",
            "=",
            "global_var",
            "...",
            "}",
            "m[",
            "a",
            "b",
            "]",
            "=",
            "expression",
            "using",
            "a",
            "and",
            "b"
        ],
        "description": "Use multiple threads to assign values to the matrix. If any thread throws an exception, a message will be printed to the log and the return value will be 0. If all threads complete without error, the return value will be 1. Functions that launch platforms, create or use data tables, or access the graphics subsystem are only supported on the main thread, and will throw an exception if called from a worker thread.",
        "prototype": "tf = ParallelAssign( { thread_local_var = global_var, ... }, m[ a, b ] = expression using a and b )"
    },
    "parameter": {
        "args": [
            "{name=value",
            "...}",
            "model",
            "expression"
        ],
        "description": "Defines formula parameters for models for the Nonlinear platform.",
        "prototype": "y = Parameter( {name=value, ...}, model expression )"
    },
    "parse": {
        "args": [
            "s"
        ],
        "description": "Parses the string and returns the resulting JSL expression.",
        "prototype": "y = Parse( s )"
    },
    "parsedate": {
        "args": [
            "s",
            "formatString",
            "<",
            "<<Use",
            "Locale(b=1)>",
            ")\r\ndt",
            "=",
            "In",
            "Format(",
            "s",
            "'Format",
            "Pattern'",
            "pattern",
            "<",
            "<<Use",
            "Locale(b=1)>"
        ],
        "description": "Parses a string of a given format and returns date/time value expressed as if surrounded by As Date(), returning the date in ddMonyyyy format.",
        "prototype": "dt = In Format( s, formatString, < <<Use Locale(b=1)> )\r\ndt = In Format( s, 'Format Pattern', pattern, < <<Use Locale(b=1)> )"
    },
    "parsejson": {
        "args": [
            "jsonstring"
        ],
        "description": "Convert JSON text to a JSL list or associative array representing the structure specified by the JSON data.",
        "prototype": "l = Parse JSON( jsonstring )"
    },
    "parsexml": {
        "args": [
            "string",
            "OnElement(",
            "tagname",
            "StartTag(",
            "expr",
            ")",
            "EndTag(",
            "expr",
            ")",
            ")",
            "..."
        ],
        "description": "Parses an XML expression using the OnElement expressions for specified XML tags.",
        "prototype": "Parse XML( string, OnElement( tagname, StartTag( expr ), EndTag( expr ) ), ... )"
    },
    "patabort": {
        "args": [],
        "description": "Generates a pattern value that causes the entire match to fail immediately with no further backup and retry.",
        "prototype": "Pat Abort()"
    },
    "pataltern": {
        "args": [
            "pat1",
            "pat2",
            "..."
        ],
        "description": "Generates a pattern value that matches any one of the supplied patterns. Generally written as pat1 | pat2 | ....",
        "prototype": "Pat Altern( pat1, pat2, ... )"
    },
    "patany": {
        "args": [
            "string"
        ],
        "description": "Generates a pattern value that will match any one character in the string.",
        "prototype": "Pat Any( string )"
    },
    "patarb": {
        "args": [
            "pattern"
        ],
        "description": "Generates a pattern value that matches zero or more characters.",
        "prototype": "Pat Arb( pattern )"
    },
    "patarbno": {
        "args": [
            "pattern"
        ],
        "description": "Generates a pattern value that matches its argument zero or more times. Same as patRepeat(pattern,0,infinity,RELUCTANT); (*? in regex).",
        "prototype": "Pat Arb No( pattern )"
    },
    "patat": {
        "args": [
            "variable"
        ],
        "description": "Generates a pattern value that matches zero characters and assigns the current cursor position to variable. Generally written as patpos()>>variable.",
        "prototype": "Pat At( variable )"
    },
    "patbreak": {
        "args": [
            "string"
        ],
        "description": "Generates a pattern value that matches zero or more characters not in the string, stopping before a (required) character in the string.",
        "prototype": "Pat Break( string )"
    },
    "patconcat": {
        "args": [
            "pat1",
            "pat2",
            "..."
        ],
        "description": "Generates a pattern value that matches each of the supplied patterns in turn. Generally written as pat1 + pat2 + ....",
        "prototype": "Pat Concat( pat1, pat2, ... )"
    },
    "patconditional": {
        "args": [
            "pattern",
            "variable"
        ],
        "description": "Generates a pattern value that matches the supplied pattern and stores the matched text in variable on success. Generally written as pattern >? variable.",
        "prototype": "Pat Conditional( pattern, variable )"
    },
    "patfail": {
        "args": [],
        "description": "Generates a pattern value that always fails to match going forward, forcing the matcher to retry alternatives.",
        "prototype": "Pat Fail()"
    },
    "patfence": {
        "args": [],
        "description": "Generates a pattern value that matches zero characters going forwards and fails when backing up, causing the match to fail. Also used to trim down the pattern backup stack.",
        "prototype": "Pat Fence()"
    },
    "path": {
        "args": [
            "pathMatrix|pathText",
            "<fill=0>"
        ],
        "description": "Draws a stroke along the given path if fill is 0, or paints the interior of the given path if fill is not 0. The path can be specified with an N x 3 matrix or with a text representation. A path matrix has three columns for x, y, and flags for each point in the path. The flag values are 0 for control, 1 for move, 2 for line segment, 3 for cubic Bézier segment, and are negative if the point also closes the path. Path text supports SVG syntax.",
        "prototype": "Path( pathMatrix|pathText, <fill=0> )"
    },
    "pathtochar": {
        "args": [
            "pathMatrix"
        ],
        "description": "Converts a path specification from matrix form to character form.",
        "prototype": "s = Path To Char( pathMatrix )"
    },
    "patimmediate": {
        "args": [
            "pattern",
            "variable"
        ],
        "description": "Generates a pattern value that matches the supplied pattern and immediately stores the matched text in variable. Generally written as pattern >> variable.",
        "prototype": "Pat Immediate( pattern, variable )"
    },
    "patlen": {
        "args": [
            "n"
        ],
        "description": "Generates a pattern value that matches n characters.",
        "prototype": "Pat Len( n )"
    },
    "patlookahead": {
        "args": [
            "pattern",
            "<0|1>"
        ],
        "description": "A zero width pattern match after the current position. The second optional argument defaults to 0. 1 designates a negative match, or a non-match.",
        "prototype": "Pat Look Ahead( pattern, <0|1> )"
    },
    "patlookbehind": {
        "args": [
            "pattern",
            "<0|1>"
        ],
        "description": "A zero width pattern match before the current position. The second optional argument defaults to 0. 1 designates a negative match, or a non-match.",
        "prototype": "Pat Look Behind( pattern, <0|1> )"
    },
    "patmatch": {
        "args": [
            "source",
            "pattern",
            "replacement"
        ],
        "description": "Executes the pattern match in the pattern variable against the string in the source variable; optional replacement text replaces the matched text.",
        "prototype": "Pat Match( source, pattern, replacement )"
    },
    "patnotany": {
        "args": [
            "string"
        ],
        "description": "Generates a pattern value that will match any one character not in the string.",
        "prototype": "Pat Not Any( string )"
    },
    "patpos": {
        "args": [
            "n"
        ],
        "description": "Generates a pattern value that matches zero characters if the cursor is at position n. With no argument, the Pat Pos() function returns the cursor position for >> or >? assignment: patpos()>>variable.",
        "prototype": "Pat Pos( n )"
    },
    "patregex": {
        "args": [
            "string"
        ],
        "description": "Generates a pattern value that matches the regular expression in the string.",
        "prototype": "Pat Regex( string )"
    },
    "patrem": {
        "args": [],
        "description": "Generates a pattern value that matches the remainder of the text.",
        "prototype": "Pat Rem()"
    },
    "patrepeat": {
        "args": [
            "pattern",
            "<min=1>",
            "<max=infinity>",
            "<GREEDY",
            "or",
            "RELUCTANT=GREEDY>"
        ],
        "description": "Generates a pattern value that matches the supplied pattern between min and max times.",
        "prototype": "Pat Repeat( pattern, <min=1>, <max=infinity>, <GREEDY or RELUCTANT=GREEDY> )"
    },
    "patrpos": {
        "args": [
            "n"
        ],
        "description": "Generates a pattern value that matches zero characters if the cursor is n characters from the end.",
        "prototype": "Pat R Pos( n )"
    },
    "patrtab": {
        "args": [
            "n"
        ],
        "description": "Generates a pattern value that matches zero or more characters to move the cursor forward to n characters before the end.",
        "prototype": "Pat R Tab( n )"
    },
    "patspan": {
        "args": [
            "string"
        ],
        "description": "Generates a pattern value that matches one or more characters in the string.",
        "prototype": "Pat Span( string )"
    },
    "patstring": {
        "args": [
            "string"
        ],
        "description": "Generates a pattern value that matches the string. Generally the string can be used without using the Pat String() function.",
        "prototype": "Pat String( string )"
    },
    "patsucceed": {
        "args": [],
        "description": "Generates a pattern value that always matches zero characters, even when backing up.",
        "prototype": "Pat Succeed()"
    },
    "pattab": {
        "args": [
            "n"
        ],
        "description": "Generates a pattern value that matches zero or more characters to move the cursor forward to position n.",
        "prototype": "Pat Tab( n )"
    },
    "pattest": {
        "args": [
            "expression"
        ],
        "description": "Generates a pattern value that matches zero characters if the expression is nonzero. The expression is re-evaluated during each test, as if Expr() was used.",
        "prototype": "Pat Test( expression )"
    },
    "payment": {
        "args": [
            "rate",
            "nper",
            "pv",
            "<fv=0>",
            "<type=0>"
        ],
        "description": "Returns the payment for a loan based on constant payments and a constant interest rate. The type argument is 0 for end-of-period payments and 1 for beginning-of-period payments. Equivalent to the PMT function in Microsoft Excel.",
        "prototype": "x = Payment( rate, nper, pv, <fv=0>, <type=0> )"
    },
    "pencolor": {
        "args": [
            "<name|index|rgbList>"
        ],
        "description": "Sets the color for drawing lines.",
        "prototype": "Pen Color( <name|index|rgbList> )"
    },
    "pensize": {
        "args": [
            "<x>"
        ],
        "description": "Sets the pen size in pixels for drawing lines.",
        "prototype": "Pen Size( <x> )"
    },
    "pi": {
        "args": [],
        "description": "Returns the mathematical constant π, accurate to approximately 15 decimal digits: 3.1415926535....",
        "prototype": "y = Pi()"
    },
    "pickcolor": {
        "args": [
            "<window",
            "title>",
            "<name|index|rgbList>"
        ],
        "description": "Returns a color that was selected with the standard color picker.",
        "prototype": "color = Pick Color( <window title>, <name|index|rgbList> )"
    },
    "pickdirectory": {
        "args": [
            "<prompt>",
            "<path>",
            "<Show",
            "Files(",
            "boolean",
            ")>"
        ],
        "description": "Prompts the user with an Open Directory window, returning the pathname of the chosen directory. The optional prompt string is shown at the top of the window. Show Files can be any of the three arguments, and takes a Boolean argument. 1 shows files in the Pick Directory window, 0 does not. The default value is 0. The path string specifies the directory that the Pick Directory window initially displays. If you use the path string, it must follow the prompt string, but Show Files can be between them.",
        "prototype": "path = Pick Directory( <prompt>, <path>, <Show Files( boolean )> )"
    },
    "pickfile": {
        "args": [
            "<prompt>",
            "<initial",
            "directory>",
            "<filterList>",
            "<first",
            "filter>",
            "<saveFlag=0|1>",
            "<default",
            "file>",
            "<multiple>"
        ],
        "description": "Prompts the user with an Open window, returning the pathname of the chosen file. The filterList argument is a list of strings of the form: 'Label|suffix1;suffix2;...'. The first filter argument specifies which filter is initially shown. The fifth argument indicates whether the window should function as a save (saveFlag = 1) or open (saveFlag = 0) window. The default file argument specifies the file that is initially selected. The multiple argument allows multiple files to be selected if saveFlag is 0. ",
        "prototype": "path = Pick File( <prompt>, <initial directory>, <filterList>, <first filter>, <saveFlag=0|1>, <default file>, <multiple> )"
    },
    "picturebox": {
        "args": [
            "Picture",
            "Object"
        ],
        "description": "Creates a display box that contains a graphics picture object. You can either open a picture and then reference it, or you can use the Open command with a path to the picture in place of the Picture Object argument.",
        "prototype": "pict = Picture Box( Picture Object )"
    },
    "pie": {
        "args": [
            "left",
            "top",
            "right",
            "bottom",
            "startAngle",
            "endAngle"
        ],
        "description": "Draws a pie slice.",
        "prototype": "Pie( left, top, right, bottom, startAngle, endAngle )"
    },
    "pieseg": {
        "args": [
            "<{",
            "xorigin",
            "yorigin",
            "}>",
            "<radius>",
            "<style('pie'",
            "'ring'",
            "'coxcomb')>",
            "values"
        ],
        "description": "Creates a Pie Seg at the specified origin, with the specified radius, based on values specified in matrix format.",
        "prototype": "ps = Pie Seg(<{ xorigin, yorigin }>, <radius>, <style('pie', 'ring', 'coxcomb')>, values)"
    },
    "pixellineto": {
        "args": [
            "h",
            "v"
        ],
        "description": "Draws a line from the current pixel-based pen coordinate to the horizontal and vertical coordinates given.",
        "prototype": "Pixel Line To( h, v )"
    },
    "pixelmoveto": {
        "args": [
            "h",
            "v"
        ],
        "description": "Moves the pixel-addressed pen to the horizontal and vertical coordinate relative to the origin.",
        "prototype": "Pixel Move To( h, v )"
    },
    "pixelorigin": {
        "args": [
            "x",
            "y"
        ],
        "description": "Sets the origin that pixel drawing commands are based on.",
        "prototype": "Pixel Origin( x, y )"
    },
    "pixelpath": {
        "args": [
            "h",
            "v",
            "pathMatrix|pathText",
            "<fill=0>",
            "<scale=1.0>",
            "<orient={0.0",
            "1.0}>"
        ],
        "description": "Draws a stroke along the given pixel-based path if fill is 0, or paints the interior of the given path if fill is not 0. The path can be specified with an N x 3 matrix or with a text representation. A path matrix has three columns for x, y, and flags for each point in the path. The flag values are 0 for control, 1 for move, 2 for line segment, 3 for cubic Bézier segment, and are negative if the point also closes the path. Path text supports SVG syntax. The path will be scaled and translated about its origin according to the optional parameters, with the orientation specified in the axis space.",
        "prototype": "PixelPath( h, v, pathMatrix|pathText, <fill=0>, <scale=1.0>, <orient={0.0,1.0}> )"
    },
    "pixeltext": {
        "args": [
            "<properties>",
            "{h",
            "v}",
            "text",
            "..."
        ],
        "description": "Moves to the {h, v} pixel position and draws text specified by the text argument. Named property arguments include Center Justified, Right Justified, Erased, Boxed, Counterclockwise, Clockwise. The position arguments, named arguments, and strings can be mixed in any order.",
        "prototype": "Pixel Text( <properties>, {h, v}, text, ... )"
    },
    "platform": {
        "args": [
            "dataTable",
            "script"
        ],
        "description": "Evaluates the given script in the context of the given data table. Returns the resulting display box for embedding in a display tree.",
        "prototype": "y = Platform( dataTable, script )"
    },
    "platformpreference": {
        "args": [
            "platformName(",
            "optionName(",
            "value",
            ")",
            "...",
            ")",
            "..."
        ],
        "description": "Sets platform preferences as specified.",
        "prototype": "Platform Preferences( platformName( optionName( value ), ... ) ... )"
    },
    "platformpreferences": {
        "args": [
            "platformName(",
            "optionName(",
            "value",
            ")",
            "...",
            ")",
            "..."
        ],
        "description": "Sets platform preferences as specified.",
        "prototype": "Platform Preferences( platformName( optionName( value ), ... ) ... )"
    },
    "plotcolbox": {
        "args": [
            "title",
            "numbers"
        ],
        "description": "Returns a display box to graph the numbers. The numbers argument can be a list or matrix.",
        "prototype": "y = Plot Col Box( title, numbers )"
    },
    "poissondistribution": {
        "args": [
            "lambda",
            "k"
        ],
        "description": "Returns the probability that a Poisson distributed random variable is less than or equal to k, where lambda is the mean parameter and k is the count of interest.",
        "prototype": "cumprob = Poisson Distribution( lambda, k )"
    },
    "poissonprobability": {
        "args": [
            "lambda",
            "k"
        ],
        "description": "Returns the probability that a Poisson distributed random variable is equal to k, where lambda is the mean parameter and k is the count of interest.",
        "prototype": "prob = Poisson Probability( lambda, k )"
    },
    "poissonquantile": {
        "args": [
            "lambda",
            "cumprob"
        ],
        "description": "Returns the smallest integer quantile for which the cumulative probability of the Poisson( lambda ) distribution is larger than or equal to cumprob.",
        "prototype": "q = Poisson Quantile( lambda, cumprob )"
    },
    "polygon": {
        "args": [
            "{x1",
            "y1}",
            "{x2",
            "y2}",
            "...",
            ");",
            "Polygon(",
            "xMatrix",
            "yMatrix"
        ],
        "description": "Draws the polygon specified by the points.",
        "prototype": "Polygon( {x1, y1}, {x2, y2}, ... ); Polygon( xMatrix, yMatrix )"
    },
    "polygonarea": {
        "args": [
            "{x1",
            "y1}",
            "{x2",
            "y2}",
            "...",
            ");\r\narea",
            "=",
            "Polygon",
            "Area(",
            "xMatrix",
            "yMatrix"
        ],
        "description": "Calculates the area of the specified polygon.",
        "prototype": "area = Polygon Area( {x1, y1}, {x2, y2}, ... );\r\narea = Polygon Area( xMatrix, yMatrix )"
    },
    "polygoncentroid": {
        "args": [
            "{x1",
            "y1}",
            "{x2",
            "y2}",
            "...",
            ");\r\ncentroid",
            "=",
            "Polygon",
            "Centroid(",
            "xMatrix",
            "yMatrix"
        ],
        "description": "Calculates the centroid of the specified polygon.",
        "prototype": "{cx, cy} = Polygon Centroid( {x1, y1}, {x2, y2}, ... );\r\ncentroid = Polygon Centroid( xMatrix, yMatrix )"
    },
    "polyseg": {
        "args": [
            "x",
            "values",
            "y",
            "values"
        ],
        "description": "Returns a display seg that represents a polygon with vertices based on the passed in x and y values.",
        "prototype": "ps = Poly Seg(x values, y values)"
    },
    "polytopeuniformrandom": {
        "args": [
            "numSamples",
            "A",
            "b",
            "L",
            "U",
            "neq",
            "nle",
            "nge",
            "<nwarm=200>",
            "<nstride=25>"
        ],
        "description": "Generates random uniform points over a convex polytope. The numSamples argument specifies the number of random points to be generated. The A argument is the constraint coefficient matrix. The B argument is the right hand side values of constraints. The L and U arguments are the lower and upper bounds for the variables, respectively. The neq, mle, and nge arguments are the number of equality constraints, the number of less than or equal constraints, and the number of greater than or equal constraints, respectively. The nwarm argument is the number of warm-up repetitions before points are written to the output matrix. The nstride argument is the number of repetitions between each point that is written to the output matrix. Note that the constraints must be listed as equality first, less than or equal next, and greater than or equal last.",
        "prototype": "points = Polytope Uniform Random( numSamples, A, b, L, U, neq, nle, nge, <nwarm=200>, <nstride=25> )"
    },
    "popupbox": {
        "args": [
            "{label1",
            "script1",
            "...}"
        ],
        "description": "Returns a display box with a popup menu defined by label/script pairs.",
        "prototype": "y = Popup Box( {label1, script1, ...} )"
    },
    "postdecrement": {
        "args": [
            "x"
        ],
        "description": "Subtracts 1 from a variable or from a list of variables.",
        "prototype": "x--; PostDecrement( x )"
    },
    "postincrement": {
        "args": [
            "x"
        ],
        "description": "Adds 1 to a variable or to a list of variables.",
        "prototype": "x++; PostIncrement( x )"
    },
    "power": {
        "args": [
            "x",
            "<y=2>"
        ],
        "description": "Returns x raised to the y power. If x is negative, y must be an integer.",
        "prototype": "z = x ^ y; z = Power( x, <y=2> )"
    },
    "pref": {
        "args": [
            "pref1(",
            "value1",
            ")",
            "..."
        ],
        "description": "Sets preferences as specified.",
        "prototype": "Preferences( pref1( value1 ), ... )"
    },
    "preference": {
        "args": [
            "pref1(",
            "value1",
            ")",
            "..."
        ],
        "description": "Sets preferences as specified.",
        "prototype": "Preferences( pref1( value1 ), ... )"
    },
    "preferences": {
        "args": [
            "pref1(",
            "value1",
            ")",
            "..."
        ],
        "description": "Sets preferences as specified.",
        "prototype": "Preferences( pref1( value1 ), ... )"
    },
    "prefs": {
        "args": [
            "pref1(",
            "value1",
            ")",
            "..."
        ],
        "description": "Sets preferences as specified.",
        "prototype": "Preferences( pref1( value1 ), ... )"
    },
    "presentvalue": {
        "args": [
            "rate",
            "nper",
            "pmt",
            "<fv=0>",
            "<type=0>"
        ],
        "description": "Returns the present value of an investment. The type argument is 0 for end-of-period payments and 1 for beginning-of-period payments. Equivalent to the PV function in Microsoft Excel.",
        "prototype": "x = Present Value( rate, nper, pmt, <fv=0>, <type=0> )"
    },
    "principalpayment": {
        "args": [
            "rate",
            "per",
            "nper",
            "pv",
            "<fv=0>",
            "<type=0>"
        ],
        "description": "Returns the payment on the principal for a given period for an investment based on periodic, constant payments and a constant interest rate. The type argument is 0 for end-of-period payments and 1 for beginning-of-period payments. Equivalent to the PPMT function in Microsoft Excel.",
        "prototype": "x = Principal Payment( rate, per, nper, pv, <fv=0>, <type=0> )"
    },
    "print": {
        "args": [
            "x",
            "..."
        ],
        "description": "Displays values of the arguments in the log, one per line.",
        "prototype": "Print( x, ... )"
    },
    "printmatrix": {
        "args": [
            "M",
            "<<ignore",
            "locale(",
            "0",
            ")",
            "<<style(",
            "'parseable'",
            ")",
            "<<separate(",
            "'",
            "'",
            ")",
            "<<line",
            "begin(",
            "'[",
            "'",
            ")",
            "<<line",
            "end(",
            "'",
            "]'",
            ")"
        ],
        "description": "Prints the matrix M. The optional argument ignore locale determines whether the printing of decimal separators should respect Locale information, where zero means to respect Locale. The optional argument style determines whether to use a style and which style to use. The available styles are parseable, which is a reformatted JSL matrix expression, latex, and other. When the style argument is other, the last three optional arguments define the beginning and ending characters of printed rows and separating characters of concatenated entries.",
        "prototype": "s = Print Matrix( M, <<ignore locale( 0 ), <<style( 'parseable' ), <<separate( ', ' ), <<line begin( '[ ' ), <<line end( ' ]' ) )"
    },
    "probit": {
        "args": [
            "p",
            "<mu=0>",
            "<sigma=1>",
            ");",
            "q",
            "=",
            "Probit(",
            "p"
        ],
        "description": "Returns the quantile from a Normal distribution, the value for which the probability is p that a random value would be lower.",
        "prototype": "q = Normal Quantile( p, <mu=0>, <sigma=1> ); q = Probit( p )"
    },
    "product": {
        "args": [
            "assignExpr",
            "limit",
            "bodyExpr"
        ],
        "description": "Returns the product of evaluations of the bodyExpr arguments, each time incrementing the variable from the assignExpr argument until it is greater than or equal to the limit argument.",
        "prototype": "y = Product( assignExpr, limit, bodyExpr )"
    },
    "psplinecoef": {
        "args": [
            "x",
            "Internal",
            "Knot",
            "Grid",
            "<degree",
            "=",
            "3>"
        ],
        "description": "Returns the matrix of P-Spline coefficients. Internal Knot Grid is either the number of desired knot points based on percentiles of x or a vector specifying the internal knot points. Optional parameter degree specifies the degree of the P-splines with a default of 3.",
        "prototype": "coef = P Spline Coef( x, Internal Knot Grid, <degree = 3> )"
    },
    "pythonconnect": {
        "args": [
            "<",
            "Echo(",
            "boolean",
            ")",
            ">",
            "<",
            "Path(",
            "Path",
            "to",
            "Python",
            "DLL",
            "or",
            "shared",
            "library",
            ")",
            ">",
            "<",
            "Use",
            "Python",
            "Version(",
            "version",
            "string",
            ")",
            ">",
            "<",
            "Python",
            "sys",
            "path(",
            "JSL",
            "list",
            "of",
            "paths",
            "defining",
            "a",
            "Python",
            "sys",
            "path",
            "set",
            ")",
            ">"
        ],
        "description": "Returns a Python connection scriptable object.",
        "prototype": "PythonConnection = Python Connect( < Echo( boolean ), > < Path( Path to Python DLL or shared library ), > < Use Python Version( version string ), > < Python sys path( JSL list of paths defining a Python sys path set ) > )"
    },
    "pythoncontrol": {
        "args": [
            "<",
            "Echo(",
            "bool",
            ")",
            "|",
            "Interactive(",
            "bool",
            ")",
            ">*"
        ],
        "description": "Changes the control options for Python",
        "prototype": "Python Control( < Echo( bool ) | Interactive( bool ) >* )"
    },
    "pythonexecute": {
        "args": [
            "{",
            "list",
            "of",
            "Inputs",
            "}",
            "{",
            "list",
            "of",
            "Outputs",
            "}",
            "statements"
        ],
        "description": "Sends a list of inputs, executes statements and returns a list of outputs.",
        "prototype": "Python Execute( { list of Inputs }, { list of Outputs }, statements )"
    },
    "pythonget": {
        "args": [
            "name"
        ],
        "description": "Returns data from Python, where the name argument can represent any of the following Python data types ( numeric | string | matrix | list | data frame).",
        "prototype": "y = Python Get( name )"
    },
    "pythongetgraphics": {
        "args": [
            "format"
        ],
        "description": "Returns the last graphics object written to the Python graph display window in a graphics format specified by the format argument.",
        "prototype": "Python graphics = Python Get Graphics( format )"
    },
    "pythongetversion": {
        "args": [],
        "description": "Returns the version number of Python being used with the JMP Python interfaces.",
        "prototype": "version = Python Get Version()"
    },
    "pythoninit": {
        "args": [
            "<",
            "Echo(",
            "boolean",
            ")",
            ">",
            "<",
            "Path(",
            "Path",
            "to",
            "Python",
            "DLL",
            "or",
            "shared",
            "library",
            ")",
            ">",
            "<",
            "Use",
            "Python",
            "Version(",
            "version",
            "string",
            ")",
            ">",
            "<",
            "Python",
            "sys",
            "path(",
            "JSL",
            "list",
            "of",
            "paths",
            "defining",
            "a",
            "Python",
            "sys",
            "path",
            "set",
            ")",
            ">"
        ],
        "description": "Initializes the Python Interfaces.",
        "prototype": "Python Init( < Echo( boolean ), > < Path( Path to Python DLL or shared library ), > < Use Python Version( version string ), > < Python sys path( JSL list of paths defining a Python sys path set ) > )"
    },
    "pythonisconnected": {
        "args": [],
        "description": "Returns 1 if there is an active Python connection. Otherwise, returns 0.",
        "prototype": "connected = Python Is Connected()"
    },
    "pythonjmpnametopythonname": {
        "args": [
            "JMP",
            "name"
        ],
        "description": "Maps a JMP variable name to a Python variable name using Python variable naming rules.",
        "prototype": "Python name = Python JMP Name To Python Name( JMP name )"
    },
    "pythonsend": {
        "args": [
            "name",
            "<",
            "Python",
            "Name(",
            "name",
            ")>"
        ],
        "description": "Sends data to Python, where the name argument can represent any of the following JMP data types ( numeric | string | matrix | list | data table).",
        "prototype": "Python Send( name <, Python Name( name )> )"
    },
    "pythonsendfile": {
        "args": [
            "filename",
            "<",
            "Python",
            "Name(",
            "name",
            ")>"
        ],
        "description": "Sends a data file to Python, where the filename argument is a string specifying a pathname to the file to be sent to Python.",
        "prototype": "Python Send File( filename <, Python Name( name )> )"
    },
    "pythonsubmit": {
        "args": [
            "statements"
        ],
        "description": "Submits statements to Python. Statements can be in the form of a string value or list of string values.",
        "prototype": "Python Submit( statements )"
    },
    "pythonsubmitfile": {
        "args": [
            "path"
        ],
        "description": "Submits statements to Python using a file specified by the path argument.",
        "prototype": "Python Submit File( path )"
    },
    "pythonterm": {
        "args": [],
        "description": "Terminates the Python Interfaces.",
        "prototype": "Python Term()"
    },
    "qr": {
        "args": [
            "X"
        ],
        "description": "Creates an m by m orthogonal matrix Q and an m by n upper triangular matrix R, such that X = Q * R. The argument X is an m by n matrix.",
        "prototype": "{Q, R} = QR( X )"
    },
    "quantile": {
        "args": [
            "p",
            "x1",
            "..."
        ],
        "description": "Returns the specified quantile p of the x arguments. The quantile argument can be a scalar or a matrix. The x values can also be specified as values within a single matrix or list argument.",
        "prototype": "y = Quantile( p, x1, ... )"
    },
    "quarter": {
        "args": [
            "datetime"
        ],
        "description": "Returns the quarter part of a date-time value, 1 - 4.",
        "prototype": "q = Quarter( datetime )"
    },
    "query": {
        "args": [
            "<",
            "<",
            "dt1",
            "|",
            "Table(",
            "dt1",
            "alias1",
            ")",
            ">",
            "...",
            "<",
            "dtN",
            "|",
            "Table(",
            "dtN",
            "aliasN",
            ")",
            ">",
            ">",
            "\r\n",
            "<Private|Invisible>",
            "<Scalar>",
            "sqlStatement"
        ],
        "description": "Perform an SQL query on JMP data tables. sqlStatement (the SQL query, most likely a SELECT statement) is required and must be the last argument. JMP data tables referenced by the SQL statement must be passed in as arguments to Query(), using Table(dt, 'alias') to create an alias for the table that the SQL can use if desired. Invisible or Private can be passed in to control the visibility of the resulting data table. If the SQL statement returns a single value, pass in Scalar, which will cause the single value to be returned instead of a data table.",
        "prototype": "result = Query( < < dt1 | Table( dt1, alias1 ) >, ..., < dtN | Table( dtN, aliasN ) > >,\r\n     <Private|Invisible>, <Scalar>, sqlStatement )"
    },
    "quit": {
        "args": [
            "<'No",
            "Save'>);",
            "Exit(<'No",
            "Save'>"
        ],
        "description": "Exits JMP.",
        "prototype": "Quit(<'No Save'>); Exit(<'No Save'>)"
    },
    "radiobox": {
        "args": [
            "{item",
            "...}",
            "<script>"
        ],
        "description": "Returns a display box to show a set of radio buttons.",
        "prototype": "y = Radio Box( {item, ...}, <script> )"
    },
    "randombeta": {
        "args": [
            "alpha",
            "beta",
            "<theta=0>",
            "<sigma=1>"
        ],
        "description": "Returns a random number from a beta distribution.",
        "prototype": "y = Random Beta( alpha, beta, <theta=0>, <sigma=1> )"
    },
    "randombetabinomial": {
        "args": [
            "n",
            "p",
            "<delta=0>"
        ],
        "description": "Returns a random number from a beta binomial distribution for n trials with probability p and correlation delta.",
        "prototype": "y = Random Beta Binomial( n, p, <delta=0> )"
    },
    "randombinomial": {
        "args": [
            "n",
            "p"
        ],
        "description": "Returns a random number from a binomial distribution with n trials and event probability p.",
        "prototype": "y = Random Binomial( n, p )"
    },
    "randomcategory": {
        "args": [
            "probabilityA",
            "resultA",
            "probabilityB",
            "resultB",
            "resultElse"
        ],
        "description": "Returns a random category given pairs of probability and result expressions. A random uniform number is generated and compared to the probability arguments to determine which result argument is returned.",
        "prototype": "y = Random Category( probabilityA, resultA, probabilityB, resultB, resultElse )"
    },
    "randomcauchy": {
        "args": [],
        "description": "Returns a random number from a Cauchy distribution with a median of zero.",
        "prototype": "y = Random Cauchy()"
    },
    "randomchisquare": {
        "args": [
            "df",
            "<nonCentrality=0>"
        ],
        "description": "Returns a random number from a Chi-Square distribution.",
        "prototype": "y = Random ChiSquare( df, <nonCentrality=0> )"
    },
    "randomexp": {
        "args": [],
        "description": "Returns a random number from an exponential distribution.",
        "prototype": "y = Random Exp()"
    },
    "randomf": {
        "args": [
            "dfnum",
            "dfden",
            "<nonCentrality=0>"
        ],
        "description": "Returns a random number from an F distribution.",
        "prototype": "y = Random F( dfnum, dfden, <nonCentrality=0> )"
    },
    "randomfrechet": {
        "args": [
            "<mu=0>",
            "<sigma=1>"
        ],
        "description": "Returns a random number from a Fréchet distribution.",
        "prototype": "y = Random Frechet( <mu=0>, <sigma=1> )"
    },
    "randomgamma": {
        "args": [
            "alpha",
            "<scale=1>"
        ],
        "description": "Returns a random number from a gamma distribution.",
        "prototype": "y = Random Gamma( alpha, <scale=1> )"
    },
    "randomgammapoisson": {
        "args": [
            "lambda",
            "<sigma=1>"
        ],
        "description": "Returns a random number from a gamma Poisson distribution with parameters lambda and sigma.",
        "prototype": "y = Random Gamma Poisson( lambda, <sigma=1> )"
    },
    "randomgengamma": {
        "args": [
            "<mu=0>",
            "<sigma=1>",
            "<lambda=0>"
        ],
        "description": "Returns a random number from an extended generalized gamma distribution with parameters mu, sigma, and lambda.",
        "prototype": "y = Random GenGamma( <mu=0>, <sigma=1>, <lambda=0> )"
    },
    "randomgeometric": {
        "args": [
            "p"
        ],
        "description": "Returns a random number of non-events until an event occurs, for events with probability p.",
        "prototype": "y = Random Geometric( p )"
    },
    "randomglog": {
        "args": [
            "mu",
            "sigma",
            "lambda"
        ],
        "description": "Returns a random number from a generalized logarithm distribution.",
        "prototype": "y = Random GLog( mu, sigma, lambda )"
    },
    "randomindex": {
        "args": [
            "n",
            "k"
        ],
        "description": "Returns a k by 1 matrix of random integers between 1 and n with no duplicates.",
        "prototype": "x = Random Index( n, k )"
    },
    "randominteger": {
        "args": [
            "n",
            ");",
            "Random",
            "Integer(",
            "k",
            "n"
        ],
        "description": "Returns a random integer between 1 and n (or between k and n) inclusive.",
        "prototype": "y = Random Integer( n ); Random Integer( k, n )"
    },
    "randomjohnsonsb": {
        "args": [
            "gamma",
            "delta",
            "theta",
            "sigma"
        ],
        "description": "Returns a random number from a Johnson Sb distribution.",
        "prototype": "y = Random Johnson Sb( gamma, delta, theta, sigma )"
    },
    "randomjohnsonsl": {
        "args": [
            "gamma",
            "delta",
            "theta",
            "<sigma=1>"
        ],
        "description": "Returns a random number from a Johnson Sl distribution.",
        "prototype": "y = Random Johnson Sl( gamma, delta, theta, <sigma=1> )"
    },
    "randomjohnsonsu": {
        "args": [
            "gamma",
            "delta",
            "theta",
            "sigma"
        ],
        "description": "Returns a random number from a Johnson Su distribution.",
        "prototype": "y = Random Johnson Su( gamma, delta, theta, sigma )"
    },
    "randomlev": {
        "args": [
            "<mu=0>",
            "<sigma=1>"
        ],
        "description": "Returns a random number from a LEV distribution.",
        "prototype": "y = Random LEV( <mu=0>, <sigma=1> )"
    },
    "randomloggengamma": {
        "args": [
            "<mu=0>",
            "<sigma=1>",
            "<lambda=0>"
        ],
        "description": "Returns a random number from a log generalized gamma distribution with parameters mu, sigma, and lambda.",
        "prototype": "y = Random LogGenGamma( <mu=0>, <sigma=1>, <lambda=0> )"
    },
    "randomlogistic": {
        "args": [
            "<mu=0>",
            "<sigma=1>"
        ],
        "description": "Returns a random number from a logistic distribution.",
        "prototype": "y = Random Logistic( <mu=0>, <sigma=1> )"
    },
    "randomloglogistic": {
        "args": [
            "<mu=0>",
            "<sigma=1>"
        ],
        "description": "Returns a random number from a loglogistic distribution.",
        "prototype": "y = Random Loglogistic( <mu=0>, <sigma=1> )"
    },
    "randomlognormal": {
        "args": [
            "<mu=0>",
            "<sigma=1>"
        ],
        "description": "Returns a random number from a lognormal distribution with location parameter mu and scale parameter sigma.",
        "prototype": "y = Random Lognormal( <mu=0>, <sigma=1> )"
    },
    "randommultivariatenormal": {
        "args": [
            "mean",
            "covar",
            "<nrows=1>"
        ],
        "description": "Returns a random nrows by p matrix from a multivariate normal distribution with mean vector mean and (positive semi-definite) covariance matrix covar, where p is defined as the number of rows of covar.",
        "prototype": "y = Random Multivariate Normal( mean, covar, <nrows=1>)"
    },
    "randomnegativebinomial": {
        "args": [
            "r",
            "p"
        ],
        "description": "Returns a random number of non-events until r events occur, for events with probability p.",
        "prototype": "y = Random Negative Binomial( r, p )"
    },
    "randomnormal": {
        "args": [
            "<mu=0>",
            "<sigma=1>"
        ],
        "description": "Returns a random number from a normal distribution with mean mu and standard deviation sigma.",
        "prototype": "y = Random Normal( <mu=0>, <sigma=1> )"
    },
    "randomnormalmixture": {
        "args": [
            "meanvec",
            "sdvec",
            "probvec"
        ],
        "description": "Returns a random number from a normal mixture distribution with group means meanvec, group standard deviations sdvec, and group probabilities probvec.",
        "prototype": "y = Random Normal Mixture( meanvec, sdvec, probvec )"
    },
    "randompoisson": {
        "args": [
            "lambda"
        ],
        "description": "Returns a random number from a Poisson distribution.",
        "prototype": "y = Random Poisson( lambda )"
    },
    "randomreset": {
        "args": [
            "seed",
            "number"
        ],
        "description": "Restarts the random sequences with a new seed.",
        "prototype": "Random Reset( seed number )"
    },
    "randomseedstate": {
        "args": [
            "<seed",
            "state>"
        ],
        "description": "Retrieves or restores the random seed state to or from a blob object.",
        "prototype": "Random Seed State( <seed state> )"
    },
    "randomsev": {
        "args": [
            "<mu=0>",
            "<sigma=1>"
        ],
        "description": "Returns a random number from a SEV distribution.",
        "prototype": "y = Random SEV( <mu=0>, <sigma=1> )"
    },
    "randomshash": {
        "args": [
            "gamma",
            "delta",
            "theta",
            "sigma"
        ],
        "description": "Returns a pseudo-random number from the SHASH distribution.",
        "prototype": "y = Random SHASH( gamma, delta, theta, sigma )"
    },
    "randomshuffle": {
        "args": [
            "matrix"
        ],
        "description": "Returns the matrix with the elements shuffled into a random order.",
        "prototype": "y = Random Shuffle( matrix )"
    },
    "randomt": {
        "args": [
            "df",
            "<nonCentrality=0>"
        ],
        "description": "Returns a random number from an t distribution.",
        "prototype": "y = Random t( df, <nonCentrality=0> )"
    },
    "randomtriangular": {
        "args": [
            "a",
            "b",
            "c",
            ");\r\ny",
            "=",
            "Random",
            "Triangular(",
            "b",
            "c",
            ");\r\ny",
            "=",
            "Random",
            "Triangular(",
            "b"
        ],
        "description": "Returns a random number from a triangular distribution with lower limit a, mode b, and upper limit c. Random Triangular(b,c) is equivalent to Random Triangular(0,b,c). Random Triangular(b) is equivalent to Random Triangular(0,b,1).",
        "prototype": "y = Random Triangular( a, b, c );\r\ny = Random Triangular( b, c );\r\ny = Random Triangular( b )"
    },
    "randomuniform": {
        "args": [
            "<min>",
            "<max>"
        ],
        "description": "Returns a random number from a uniform distribution between min and max, exclusive.",
        "prototype": "y = Random Uniform( <min>, <max> )"
    },
    "randomweibull": {
        "args": [
            "beta",
            "<alpha=1>"
        ],
        "description": "Returns a random number from a Weibull distribution.",
        "prototype": "y = Random Weibull( beta, <alpha=1> )"
    },
    "range": {
        "args": [
            "x1",
            "..."
        ],
        "description": "Returns the minimum and maximum values among the combined arguments, which can be scalar, matrix or list arguments.",
        "prototype": "y = Range( x1, ... )"
    },
    "rangesliderbox": {
        "args": [
            "minValue",
            "maxValue",
            "lowVariable",
            "highVariable",
            "script"
        ],
        "description": "Returns a display box that shows a range slider control that ranges from minValue to maxValue. As the two sliders' positions change, their values are placed into lowVariable and highVariable, and the script is run.",
        "prototype": "y = Range Slider Box( minValue, maxValue, lowVariable, highVariable, script )"
    },
    "rank": {
        "args": [
            "x"
        ],
        "description": "Returns a vector of indices that, used as a subscript to the original vector v, sorts the vector by rank. Excludes missing values.",
        "prototype": "y = Rank Index( x )"
    },
    "rankindex": {
        "args": [
            "x"
        ],
        "description": "Returns a vector of indices that, used as a subscript to the original vector v, sorts the vector by rank. Excludes missing values.",
        "prototype": "y = Rank Index( x )"
    },
    "ranking": {
        "args": [
            "x"
        ],
        "description": "Returns a vector of ranks of the values of x, low to high as 1 to n, ties arbitrary.",
        "prototype": "y = Ranking( x )"
    },
    "rankingtie": {
        "args": [
            "x"
        ],
        "description": "Returns a vector of ranks of the values of x, but ranks for ties averaged.",
        "prototype": "y = Ranking Tie( x )"
    },
    "rconnect": {
        "args": [],
        "description": "Returns an R connection scriptable object.",
        "prototype": "RConnection = R Connect()"
    },
    "rcontrol": {
        "args": [
            "Interrupt",
            "|",
            "Async(",
            "bool",
            ")",
            "|",
            "Echo(",
            "bool",
            ")"
        ],
        "description": "Changes the control options for R",
        "prototype": "R Control( Interrupt | Async( bool ) | Echo( bool ) )"
    },
    "recode": {
        "args": [
            "string|number|list",
            "{<transform>",
            "...}",
            "<Multiple",
            "Response",
            "(Separator(sepChar))>",
            "<By",
            "Word(Delimiters(<chars>)>"
        ],
        "description": "Apply the listed transformations to the supplied value and return the result.",
        "prototype": "recode(string|number|list, {<transform>, ...}, <Multiple Response (Separator(sepChar))>, <By Word(Delimiters(<chars>)>)\r\n"
    },
    "rect": {
        "args": [
            "left",
            "top",
            "right",
            "bottom",
            "<fill=0>",
            ");",
            "Rect(",
            "{left",
            "top}",
            "{right",
            "bottom}"
        ],
        "description": "Draws a rectangle, filled if fill is nonzero.",
        "prototype": "Rect( left, top, right, bottom, <fill=0> ); Rect( {left, top}, {right, bottom} )"
    },
    "recurse": {
        "args": [
            "x1",
            "..."
        ],
        "description": "Calls the containing function.",
        "prototype": "y = Recurse( x1, ... )"
    },
    "regex": {
        "args": [
            "source",
            "pattern",
            "<format",
            "<IGNORECASE>",
            "<GLOBALREPLACE>>"
        ],
        "description": "Searches in the source text for a match to the pattern. The format defaults to '\\0' (the entire match) but could be 'Fred' (for a constant replacement) or '\\1' (to use the text matched by the first parenthesis in the pattern). Returns numeric missing for no match. Case must match by default.",
        "prototype": "result = Regex( source, pattern, <format, <IGNORECASE>, <GLOBALREPLACE>> )"
    },
    "regexmatch": {
        "args": [
            "source",
            "pattern",
            "replacement"
        ],
        "description": "Executes a regular expression match and returns a list of the entire matched text and the matches for each back reference created by an open parenthesis. Optionally, the third argument specifies a replacement string for the entire match; the replacement string may use back references.",
        "prototype": "Regex Match( source, pattern, replacement )"
    },
    "registeraddin": {
        "args": [
            "uniqueId",
            "homeFolder",
            "<displayName(name)>",
            "<JMPVersion(version)>",
            "<LoadsAtStartup(autoLoad)>",
            "<LoadNow(load)>"
        ],
        "description": "Register an add-in",
        "prototype": "Register Addin( uniqueId, homeFolder, <displayName(name)>, <JMPVersion(version)>, <LoadsAtStartup(autoLoad)>, <LoadNow(load)> )"
    },
    "remove": {
        "args": [
            "x",
            "<i>",
            "<n=1>",
            ");",
            "y",
            "=",
            "Remove(",
            "x",
            "{list}"
        ],
        "description": "Returns a copy of list x, deleting n items starting with the ith item or deleting a list of items specified by the list argument.",
        "prototype": "y = Remove( x, <i>, <n=1> ); y = Remove( x, {list} )"
    },
    "removecolortheme": {
        "args": [
            "'Name'|{'Name'",
            "<flags>",
            "{color",
            "...}",
            "<{position",
            "...}>}"
        ],
        "description": "Removes a custom color theme from the global list, either by name or by the full color theme object.",
        "prototype": "Remove Color Theme('Name'|{'Name', <flags>, {color, ...}, <{position, ...}>})"
    },
    "removecustomfunctions": {
        "args": [
            "{function",
            "1",
            "full",
            "name",
            "function",
            "2",
            "full",
            "name",
            "...}",
            "|",
            "function",
            "full",
            "name"
        ],
        "description": "Removes a list of custom functions from the environment.",
        "prototype": "Remove Custom Functions({function 1 full name, function 2 full name, ...} | function full name)"
    },
    "removefrom": {
        "args": [
            "x",
            "<i>",
            "<n=1>"
        ],
        "description": "Modifies list, associative array, or display box x by removing items. Associative arrays specify the item to be removed with a key value i. Lists and display boxes remove starting with the item in position i. A list will remove multiple items at once if the n option is specified. Note that the x argument must be a variable.",
        "prototype": "Remove From( x, <i>, <n=1> )"
    },
    "renamedirectory": {
        "args": [
            "old",
            "new"
        ],
        "description": "Renames a directory without moving or copying it; the new name does NOT include a path.",
        "prototype": "rc = Rename Directory( old, new )"
    },
    "renamefile": {
        "args": [
            "old",
            "new"
        ],
        "description": "Renames a file without moving or copying it; the new name does NOT include a path.",
        "prototype": "rc = Rename File( old, new )"
    },
    "repeat": {
        "args": [
            "x",
            "n",
            "<m=1>"
        ],
        "description": "Returns the text, matrix, or list specified by the x argument concatenated with itself n times. If x is a number or a matrix, then n indicates vertical repetition and the optional argument m designates horizontal repetition.",
        "prototype": "s = Repeat( x, n, <m=1> )"
    },
    "report": {
        "args": [
            "platform",
            "object"
        ],
        "description": "Returns a reference to the display tree for the report from a platform.",
        "prototype": "y = Report( platform object )"
    },
    "resamplefreq": {
        "args": [
            "<rate=1>",
            "<column>"
        ],
        "description": "Generates a frequency count for sampling with replacement, useful for bootstrap samples. With no arguments, the function generates a 100% resample. The rate argument specifies the rate of resampling. If the column argument is specified, the sample size chosen is rate multiplied by the sum of the specified column. A negative rate signals that fractional frequencies are allowed.",
        "prototype": "Resample Freq( <rate=1>, <column> )"
    },
    "return": {
        "args": [
            "<Expr>",
            "...",
            "<ExprN>"
        ],
        "description": "Returns an expression value from a user-defined function.",
        "prototype": "Return(<Expr>, ..., <ExprN>)"
    },
    "reverse": {
        "args": [
            "x"
        ],
        "description": "Returns a copy of list x with the item order reversed.",
        "prototype": "y = Reverse( x )"
    },
    "reverseinto": {
        "args": [
            "x"
        ],
        "description": "Modifies list or display box x with the item order reversed. Note that the x argument must be a variable.",
        "prototype": "Reverse Into( x )"
    },
    "revertmenu": {
        "args": [],
        "description": "Reverts to factory default menus.",
        "prototype": "Revert Menu()"
    },
    "rexecute": {
        "args": [
            "{",
            "list",
            "of",
            "Inputs",
            "}",
            "{",
            "list",
            "of",
            "Outputs",
            "}",
            "statements"
        ],
        "description": "Sends a list of inputs, executes statements and returns a list of outputs.",
        "prototype": "R Execute( { list of Inputs }, { list of Outputs }, statements )"
    },
    "rgbcolor": {
        "args": [
            "r",
            "g",
            "b",
            ");",
            "y",
            "=",
            "RGB",
            "Color(",
            "{r",
            "g",
            "b}"
        ],
        "description": "Returns a color number from the red, green, and blue components, all between 0 and 1. RGB Color(1, 1, 1) is white.",
        "prototype": "y = RGB Color( r, g, b ); y = RGB Color( {r, g, b} )"
    },
    "rget": {
        "args": [
            "name"
        ],
        "description": "Returns data from R, where the name argument can represent any of the following R data types ( numeric | string | matrix | list | data frame).",
        "prototype": "y = R Get( name )"
    },
    "rgetgraphics": {
        "args": [
            "format"
        ],
        "description": "Returns the last graphics object written to the R graph display window in a graphics format specified by the format argument.",
        "prototype": "R graphics = R Get Graphics( format )"
    },
    "rgetversion": {
        "args": [],
        "description": "Returns the version number of R being used with the JMP R interfaces.",
        "prototype": "version = R Get Version()"
    },
    "right": {
        "args": [
            "s",
            "n",
            "<filler>"
        ],
        "description": "Returns a truncated or padded version of the original string or list s. The result contains the right n characters or list items, padded with any filler on the left if the length of s is less than n.",
        "prototype": "sub = Right( s, n, <filler> )"
    },
    "rinit": {
        "args": [],
        "description": "Initializes the R Interfaces.",
        "prototype": "R Init()"
    },
    "risconnected": {
        "args": [],
        "description": "Returns 1 if there is an active R connection; otherwise, returns 0.",
        "prototype": "connected = R Is Connected()"
    },
    "rjmpnametorname": {
        "args": [
            "JMP",
            "name"
        ],
        "description": "Maps a JMP variable name to an R variable name using R variable naming rules.",
        "prototype": "R name = R JMP Name To R Name( JMP name )"
    },
    "root": {
        "args": [
            "x",
            "<n=2>"
        ],
        "description": "Returns the nth root of x.",
        "prototype": "y = Root( x, <n=2> )"
    },
    "round": {
        "args": [
            "x",
            "<n>"
        ],
        "description": "Rounds x to n digits after the decimal point (or 0 digits if n is not specified). Note that the n argument can be negative.",
        "prototype": "y = Round( x, <n> )"
    },
    "row": {
        "args": [
            ");",
            "Row("
        ],
        "description": "Returns the current row in a data table. May be set as an L-value.",
        "prototype": "y = Row(); Row() = y"
    },
    "rowstate": {
        "args": [
            "<dt>",
            "<r>",
            ");",
            "Row",
            "State(",
            "<dt>",
            "<r>"
        ],
        "description": "Returns the row state of the current (or rth) row in the current data table. If the Row State() function is used as an L-value, it changes the row state of the current (or rth) row in the current data table.",
        "prototype": "y = Row State( <dt>, <r> ); Row State( <dt>, <r> ) = y"
    },
    "rsend": {
        "args": [
            "name",
            "<",
            "R",
            "Name(",
            "name",
            ")>"
        ],
        "description": "Sends data to R, where the name argument can represent any of the following JMP data types ( numeric | string | matrix | list | data table).",
        "prototype": "R Send( name <, R Name( name )> )"
    },
    "rsendfile": {
        "args": [
            "filename",
            "<",
            "R",
            "Name(",
            "name",
            ")>"
        ],
        "description": "Sends a data file to R, where the filename argument is a string specifying a pathname to the file to be sent to R.",
        "prototype": "R Send File( filename <, R Name( name )> )"
    },
    "rsubmit": {
        "args": [
            "statements"
        ],
        "description": "Submit statements to R. Statements can be in the form of a string value or list of string values.",
        "prototype": "R Submit( statements )"
    },
    "rsubmitfile": {
        "args": [
            "path"
        ],
        "description": "Submits statements to R using a file specified by the path argument.",
        "prototype": "R Submit File( path )"
    },
    "rterm": {
        "args": [],
        "description": "Terminate the R Interfaces.",
        "prototype": "R Term()"
    },
    "runprogram": {
        "args": [
            "Executable(",
            "'path/etc.exe'",
            ")",
            "\r\n",
            "<",
            "Options(",
            "{'/a'",
            "'/b",
            "etc'",
            "}",
            ")",
            ">",
            "\r\n",
            "<",
            "Parameter(",
            "optParm",
            ")",
            ">",
            "\r\n",
            "<",
            "Read",
            "Function(",
            "Function(",
            "{this",
            "optParm}",
            "etc",
            ")",
            "|",
            "'text'",
            "|",
            "'blob'",
            ")",
            ">",
            "\r\n",
            "<",
            "Write",
            "Function(",
            "Function(",
            "{this",
            "optParm}",
            "etc",
            ")",
            ")",
            ">"
        ],
        "description": "Control an external program using stdin and stdout.",
        "prototype": "obj = Run Program(\r\n    Executable( 'path/etc.exe' ),\r\n  < Options( {'/a', '/b etc' } ) >,\r\n  < Parameter( optParm ) >,\r\n  < Read Function( Function( {this, optParm}, etc ) | 'text' | 'blob' ) >,\r\n  < Write Function( Function( {this, optParm}, etc ) ) >\r\n)"
    },
    "sasassignlibrefs": {
        "args": [
            "libref",
            "path",
            "<engine>",
            "<engineOptions>"
        ],
        "description": "Assigns a SAS libref on the active SAS server connection. Returns 1 if successful, 0 otherwise.",
        "prototype": "y = SAS Assign Lib Refs( libref, path, <engine>, <engineOptions> )"
    },
    "sasconnect": {
        "args": [
            "<machine_or_logical_name>",
            "<port>",
            "<named_arguments>"
        ],
        "description": "Connects to a local, remote, or logical SAS server. Named optional arguments include Prompt(Always|Never|IfNeeded) and ConnectLibraries(true|false), string-valued arguments UserName, Password and SASVersion, as well as Boolean-valued arguments ReplaceGlobalConnection and ShowDialog. Returns a SAS server scriptable object.",
        "prototype": "conn = SAS Connect( <machine_or_logical_name>, <port>, <named_arguments> )"
    },
    "sasconnectlibrefs": {
        "args": [
            "libref"
        ],
        "description": "Connects a SAS libref on the active SAS server connection. Returns 1 if successful, 0 otherwise.",
        "prototype": "y = SAS Connect Lib Refs( libref)"
    },
    "sasdeassignlibrefs": {
        "args": [
            "libref"
        ],
        "description": "De-assigns a SAS libref on the active SAS server connection. Returns 1 if successful, 0 otherwise.",
        "prototype": "y = SAS Deassign Lib Refs( libref )"
    },
    "sasdisconnect": {
        "args": [],
        "description": "Disconnects the active SAS connection. Returns 1 if a SAS connection existed and was successfully disconnected, 0 otherwise.",
        "prototype": "y = SAS Disconnect()"
    },
    "sasexportdata": {
        "args": [
            "dt",
            "libref",
            "dataset",
            "<named_arguments>",
            ");\r\ny",
            "=",
            "SAS",
            "Export",
            "Data(",
            "dt",
            "libref.dataset",
            "<named_arguments>"
        ],
        "description": "Exports a JMP data table to a SAS data set with the specified name in the specified library on the active SAS server connection. Named optional arguments include Columns(list|col1,col2,...,coln), string-valued arguments Password, AlterPassword, ReadPassword, and WritePassword, as well as Boolean-valued arguments HonorExcludedRows, PreserveSASColumnNames, PreserveSASFormats, ReplaceExisting, and SaveJMPMetadata. Returns 1 if the export was successful, 0 otherwise.",
        "prototype": "y = SAS Export Data( dt, libref, dataset, <named_arguments> );\r\ny = SAS Export Data( dt, libref.dataset, <named_arguments> )"
    },
    "sasgetdatasets": {
        "args": [
            "libref"
        ],
        "description": "Returns a list of the data sets defined in a SAS library.",
        "prototype": "result = SAS Get Data Sets( libref )"
    },
    "sasgetfile": {
        "args": [
            "source",
            "dest"
        ],
        "description": "Transfers a file from a SAS server path to your computer, using the active SAS server connection. Source is a file path on the server. Dest is a file path on your computer.",
        "prototype": "y = SAS Get File( source, dest )"
    },
    "sasgetfilenames": {
        "args": [
            "fileref"
        ],
        "description": "Returns a list of filenames found in the given fileref from the active SAS server connection.",
        "prototype": "result = SAS Get File Names( fileref )"
    },
    "sasgetfilenamesinpath": {
        "args": [
            "path"
        ],
        "description": "Returns a list of filenames in the specified directory path on the active SAS server connection.",
        "prototype": "result = SAS Get File Names In Path( path )"
    },
    "sasgetfilerefs": {
        "args": [],
        "description": "Retrieves fileref mappings from the active SAS server connection. Returns a list of two lists. The first has the filerefs available from the global SAS server connection. The second list has the file, directory, or device names for the corresponding item in the fileref list.",
        "prototype": "result = SAS Get File Refs()"
    },
    "sasgetlibrefs": {
        "args": [
            "<named_arguments>"
        ],
        "description": "Returns a list of the currently defined SAS librefs from the active SAS server connection. Named optional arguments include FriendlyNames(True|False).",
        "prototype": "result = SAS Get Lib Refs( <named_arguments> )"
    },
    "sasgetlog": {
        "args": [],
        "description": "Returns a string containing the Log Window contents of the active SAS server connection.",
        "prototype": "result = SAS Get Log()"
    },
    "sasgetoutput": {
        "args": [],
        "description": "Returns a string containing the listing output from the last submission of SAS code to the active SAS server connection.",
        "prototype": "result = SAS Get Output()"
    },
    "sasgetresults": {
        "args": [],
        "description": "Returns a SAS Results scriptable object containing the results of the most recently submitted SAS code on the active SAS server connection.",
        "prototype": "results = SAS Get Results()"
    },
    "sasgetvarnames": {
        "args": [
            "libref",
            "dataset",
            "<named_arguments>",
            ");\r\nresult",
            "=",
            "SAS",
            "Get",
            "Var",
            "Names(",
            "libref.dataset|path",
            "<named_arguments>"
        ],
        "description": "Retrieves the variable names contained in the specified data set from the active SAS server connection. Named optional arguments include Password(password) for password-protected data sets.",
        "prototype": "result = SAS Get Var Names( libref, dataset, <named_arguments> );\r\nresult = SAS Get Var Names( libref.dataset|path, <named_arguments> )"
    },
    "sasimportdata": {
        "args": [
            "libref",
            "dataset",
            "<named_arguments>",
            ");\r\ndt",
            "=",
            "SAS",
            "Import",
            "Data(",
            "libref.dataset|path",
            "<named_arguments>"
        ],
        "description": "Imports a SAS data set from the active SAS server connection into a JMP data table. Named optional arguments include Sample(<named_arguments>), Columns(list|col1,col2,...,coln), string-valued arguments Password and Where, as well as Boolean-valued arguments ConvertCustomFormats, Invisible, UseLabelsForVarNames, SQLTableVariable, and RestoreJMPMetadata. Returns a JMP data table object.",
        "prototype": "dt = SAS Import Data( libref, dataset, <named_arguments> );\r\ndt = SAS Import Data( libref.dataset|path, <named_arguments> )"
    },
    "sasimportquery": {
        "args": [
            "sqlquery",
            "<named_arguments>"
        ],
        "description": "Executes the requested SQL query on the active SAS server connection, importing the results into a JMP data table. Named optional arguments include Boolean-valued arguments ConvertCustomFormats, Invisible, UseLabelsForVarNames, SQLTableVariable, and RestoreJMPMetadata.",
        "prototype": "dt = SAS Import Query( sqlquery, <named_arguments> )"
    },
    "sasisconnected": {
        "args": [],
        "description": "Returns 1 if there is an active SAS server connection, 0 otherwise.",
        "prototype": "y = SAS Is Connected()"
    },
    "sasislocalserveravailable": {
        "args": [],
        "description": "Returns 1 if a Local SAS Server is available.",
        "prototype": "SAS Is Local Server Available()"
    },
    "sasloadtextfile": {
        "args": [
            "path"
        ],
        "description": "Downloads the file specified in path from the active SAS server connection and retrieves its contents as a string. If path specifies a JSL file, you can evaluate it by executing: Eval(Parse(result)).",
        "prototype": "result = SAS Load Text File( path )"
    },
    "sasname": {
        "args": [
            "string|namelist"
        ],
        "description": "Converts JMP variable names to a string containing valid SAS variable names by changing special characters and blanks to underscores. The argument can be specified as a string or a list of strings.",
        "prototype": "sasName = SAS Name( string|namelist )"
    },
    "sasopenforvarnames": {
        "args": [
            "path"
        ],
        "description": "Returns a list of variable names from a SAS data set.",
        "prototype": "nameList = SAS Open For Var Names( path )"
    },
    "sassendfile": {
        "args": [
            "source",
            "dest"
        ],
        "description": "Transfers a file from your computer to a SAS server, using the active SAS server connection. Source is a file path on your computer. Dest is a file path on the server.",
        "prototype": "y = SAS Send File( source, dest )"
    },
    "sassubmit": {
        "args": [
            "sasCode",
            "<named_arguments>"
        ],
        "description": "Submits SAS code to the active SAS server connection. Named optional arguments include DeclareMacros(var1,var2,...,varN), GetSASLog(<<True|False|OnError>, <JMPLog|Window>>), OnSubmitComplete(script), OpenOutputDatasets(<All|None|dataset1,dataset2,...,datasetN>), string-valued arguments GraphicsDevice (or GDevice), ODSFormat, ODSStyle, ODSStyleSheet, and Title, as well as Boolean-valued arguments Async, ConvertCustomFormats, NoOutputWindow, ODS, ODSGraphics, and OpenODSResults. Returns 1 if successful, 0 otherwise.",
        "prototype": "y = SAS Submit( sasCode, <named_arguments> )"
    },
    "sassubmitfile": {
        "args": [
            "filename",
            "<named_arguments>"
        ],
        "description": "Submits a file containing SAS code to the active SAS server connection. Named optional arguments are the same as those for SAS Submit. Returns 1 if successful, 0 otherwise.",
        "prototype": "y = SAS Submit File( filename, <named_arguments> )"
    },
    "savelog": {
        "args": [
            "<path>"
        ],
        "description": "Writes the contents of the log to the specified file location. If the write is successful, this function returns the name of the created file.",
        "prototype": "f = Save Log( <path> )"
    },
    "savetextfile": {
        "args": [
            "path",
            "text|blob",
            "<mode('replace'|'append')>"
        ],
        "description": "Creates a text file with filename specified by the path argument and contents specified by the text string argument. If the save is successful, the Save Text File() function returns the pathname of the created file.",
        "prototype": "f = Save Text File( path, text|blob, <mode('replace'|'append')> )"
    },
    "sbinv": {
        "args": [
            "z",
            "gamma",
            "delta",
            "theta",
            "sigma"
        ],
        "description": "Transforms a standard normal variable to a double bounded Johnson variable.",
        "prototype": "x = SbInv( z, gamma, delta, theta, sigma )"
    },
    "sbtrans": {
        "args": [
            "x",
            "gamma",
            "delta",
            "theta",
            "sigma"
        ],
        "description": "Transforms a double bounded Johnson variable to a standard normal variable.",
        "prototype": "z = SbTrans( x, gamma, delta, theta, sigma )"
    },
    "scenebox": {
        "args": [
            "xsize",
            "ysize"
        ],
        "description": "Returns a display box for 3D graphics.",
        "prototype": "box = Scene Box( xsize, ysize )"
    },
    "scenedisplaylist": {
        "args": [],
        "description": "Returns a display list for 3D graphics.",
        "prototype": "list = Scene Display List()"
    },
    "schedule": {
        "args": [
            "sec",
            "scpt"
        ],
        "description": "Schedules an event that runs the scpt script argument after sec seconds have elapsed.",
        "prototype": "Schedule( sec, scpt )"
    },
    "scheffecubic": {
        "args": [
            "x1",
            "x2"
        ],
        "description": "Evaluates as x1*x2*(x1-x2); used to support modeling notation for cubic mixture models.",
        "prototype": "y = Scheffe Cubic( x1, x2 )"
    },
    "scoringimpute": {
        "args": [
            "rowWithMissing",
            "VMat",
            "colMeanVec",
            "colStdDevVec"
        ],
        "description": "Provides streaming functionality for the Automated Data Imputation (ADI) algorithm. The input arguments are a row vector that contains missing values, a loading matrix (also called the V matrix) that is produced by the ADI algorithm, a vector of the column means ignoring missing cells, and a vector of the column standard deviations ignoring missing cells. It returns the row vector with the missing values imputed using least squares estimation.",
        "prototype": "{imputedRow} = Scoring Impute ( rowWithMissing , VMat, colMeanVec, colStdDevVec)"
    },
    "scriptbox": {
        "args": [
            "<s>",
            "<JSL|Text|SAS|R|Python|JSON|XML>",
            "<width>",
            "<height>"
        ],
        "description": "Returns a display box to edit a script.",
        "prototype": "y = Script Box( <s>, <JSL|Text|SAS|R|Python|JSON|XML>, <width>, <height> )"
    },
    "scrollbox": {
        "args": [
            "<Size(",
            "h",
            "v",
            ")>",
            "displayBox"
        ],
        "description": "Returns a display box that positions a larger child box using scroll bars.",
        "prototype": "y = Scroll Box( <Size( h, v )>, displayBox )"
    },
    "second": {
        "args": [
            "datetime"
        ],
        "description": "Returns the seconds part of a date-time value, including any fractional part, 0 - 60 exclusive.",
        "prototype": "sec = Second( datetime )"
    },
    "selected": {
        "args": [
            "rs",
            ");Selected(",
            "<Row",
            "State(",
            "<r>",
            ")>"
        ],
        "description": "Returns the selected component of the specified row state value, 0 or 1. If Selected is used as an L-value, it changes the selected state of the current (or rth) row in the current data table.",
        "prototype": "y = Selected( rs );Selected( <Row State( <r> )> ) = y"
    },
    "selectedstate": {
        "args": [
            "x"
        ],
        "description": "Returns a row state value with the selected component set to the specified value.",
        "prototype": "rs = Selected State( x )"
    },
    "send": {
        "args": [
            "args",
            ");",
            "r",
            "=",
            "obj",
            "<<",
            "msg;",
            "r",
            "=",
            "Send(",
            "obj",
            "msg"
        ],
        "description": "Sends a message (in the form of an expression) to an object.",
        "prototype": "r = obj << msg( args ); r = obj << msg; r = Send( obj, msg )"
    },
    "sequence": {
        "args": [
            "start",
            "end",
            "<incr=1>",
            "<n=1>"
        ],
        "description": "Returns the Row()th item in the sequence of numbers from start to end incremented by incr. Each number in the sequence is repeated n times. Because of its dependence on Row(), the Sequence() function is mainly useful in column formulas. To create sequences as JSL matrices, see Index().",
        "prototype": "y = Sequence( start, end, <incr=1>, <n=1> )"
    },
    "setclipboard": {
        "args": [
            "text"
        ],
        "description": "Puts the specified text onto the system clipboard used by the Edit menu.",
        "prototype": "Set Clipboard( text )"
    },
    "setdefaultdirectory": {
        "args": [
            "path"
        ],
        "description": "Sets the JMP default directory, which is used as a base for subsequent relative paths.",
        "prototype": "Set Default Directory( path )"
    },
    "setenvironmentvariable": {
        "args": [
            "string",
            "<",
            "string>"
        ],
        "description": "Sets the value of the specified environment variable in the operating system. If the second argument is either missing or is an empty string then the environment variable is deleted.\r\n\r\nNOTE: On the Macintosh operating system, the variable name is case-sensitive.",
        "prototype": "value = Set Environment Variable( string, < string> )"
    },
    "setfilesearchpath": {
        "args": [
            "{path",
            "or",
            "list",
            "of",
            "paths}",
            "..."
        ],
        "description": "Sets the current list of directories to search for opening files. '.' means the current directory.",
        "prototype": "Set File Search Path( {path or list of paths}, ... )"
    },
    "setpathvariable": {
        "args": [
            "name",
            "<value>"
        ],
        "description": "Sets a path variable, which is a name like SAMPLE_DATA that is substituted for when found in pathnames.",
        "prototype": "Set Path Variable( name, <value> )"
    },
    "setplatformpreference": {
        "args": [
            "platformName(",
            "optionName(",
            "value",
            ")",
            "...",
            ")",
            "..."
        ],
        "description": "Sets platform preferences as specified.",
        "prototype": "Platform Preferences( platformName( optionName( value ), ... ) ... )"
    },
    "setplatformpreferences": {
        "args": [
            "platformName(",
            "optionName(",
            "value",
            ")",
            "...",
            ")",
            "..."
        ],
        "description": "Sets platform preferences as specified.",
        "prototype": "Platform Preferences( platformName( optionName( value ), ... ) ... )"
    },
    "setpreference": {
        "args": [
            "pref1(",
            "value1",
            ")",
            "..."
        ],
        "description": "Sets preferences as specified.",
        "prototype": "Preferences( pref1( value1 ), ... )"
    },
    "setpreferences": {
        "args": [
            "pref1(",
            "value1",
            ")",
            "..."
        ],
        "description": "Sets preferences as specified.",
        "prototype": "Preferences( pref1( value1 ), ... )"
    },
    "settoolbarvisibility": {
        "args": [
            "'toolbar-name'",
            "|",
            "Default",
            "|",
            "All",
            "<window-class-name",
            "|",
            "All>",
            "<True",
            "|",
            "False>"
        ],
        "description": "Sets the visibility of a given toolbar for a given class of windows. toolbar-name is the internal name of the toolbar. If Default is passed in as the toolbar name, the specified window class is restored to the default toolbar set for that class of windows. Examples of window-class-name are Data Table, Script, Report, and Journal. If window-class-name is All, then visibility for the specified toolbar is set for all classes of windows.\r\nReturns 1 if successful, 0 if unsuccessful.",
        "prototype": "rc = Set Toolbar Visibility( 'toolbar-name' | Default | All, <window-class-name | All>, <True | False> )"
    },
    "sevdensity": {
        "args": [
            "x",
            "mu",
            "sigma"
        ],
        "description": "Returns the density at x of a smallest extreme distribution with location mu and scale sigma.",
        "prototype": "y = SEV Density( x, mu, sigma )"
    },
    "sevdistribution": {
        "args": [
            "x",
            "mu",
            "sigma"
        ],
        "description": "Returns the probability at x of a smallest extreme distribution with location mu and scale sigma.",
        "prototype": "p = SEV Distribution( x, mu, sigma )"
    },
    "sevquantile": {
        "args": [
            "p",
            "mu",
            "sigma"
        ],
        "description": "Returns the quantile at p of a smallest extreme distribution with location mu and scale sigma.",
        "prototype": "q = SEV Quantile( p, mu, sigma )"
    },
    "shadestate": {
        "args": [
            "x"
        ],
        "description": "Returns a row state value with the color shade component set to the specified value. Needs to be combined with a Hue State() value to produce a valid color.",
        "prototype": "rs = Shade State( x )"
    },
    "shape": {
        "args": [
            "M",
            "nr",
            "<nc>",
            "<<bycol"
        ],
        "description": "Reshapes the M matrix or scalar across rows to be nr rows by nc columns. A missing value is permitted for nr. Data from M is replicated as needed to fill the nr by nc matrix. The optional argument <<bycol fills the data by column. By default, the data is filled by row. Common uses are to reshape a vector into a matrix or to vectorize a matrix.",
        "prototype": "r = Shape( M, nr, <nc>, <<bycol)"
    },
    "shapeseg": {
        "args": [
            "{Path(<path>)",
            "...}",
            "<",
            "Row",
            "States(",
            "dt",
            "|",
            "dt",
            "[rows]",
            "|",
            "dt",
            "{{rows}",
            "...}",
            "|",
            "{states}",
            ")",
            ">"
        ],
        "description": "Returns a display seg with a collection of shapes.  Each shape draws a stroke along the given path if fill is 0, or paints the interior of the given path if fill is not 0. The path can be specified with an N x 3 matrix or with a text representation. A path matrix has three columns for x, y, and flags for each point in the path. The flag values are 0 for control, 1 for move, 2 for line segment, 3 for cubic Bézier segment, and are negative if the point also closes the path. Path text supports SVG syntax.",
        "prototype": "me = Shape Seg( {Path(<path>), ...}, < Row States( dt | dt,[rows] | dt,{{rows}, ...} | {states} ) > )"
    },
    "shashdensity": {
        "args": [
            "x",
            "gamma",
            "delta",
            "theta",
            "sigma"
        ],
        "description": "Returns the density at x of a SHASH distribution.",
        "prototype": "d = SHASH Density( x, gamma, delta, theta, sigma )"
    },
    "shashdistribution": {
        "args": [
            "q",
            "gamma",
            "delta",
            "theta",
            "sigma"
        ],
        "description": "Returns the probability that a SHASH distributed random variable is less than q.",
        "prototype": "p = SHASH Distribution( q, gamma, delta, theta, sigma )"
    },
    "shashinv": {
        "args": [
            "z",
            "gamma",
            "delta",
            "theta",
            "sigma"
        ],
        "description": "Transforms a standard normal variable to a SHASH distributed variable.",
        "prototype": "x = SHASHInv( z, gamma, delta, theta, sigma )"
    },
    "shashquantile": {
        "args": [
            "p",
            "gamma",
            "delta",
            "theta",
            "sigma"
        ],
        "description": "Returns the quantile from a SHASH distribution, the value for which the probability is p that a random value would be lower.",
        "prototype": "q = SHASH Quantile( p, gamma, delta, theta, sigma )"
    },
    "shashtrans": {
        "args": [
            "x",
            "gamma",
            "delta",
            "theta",
            "sigma"
        ],
        "description": "Transforms a SHASH distributed variable to a standard normal distributed variable.",
        "prototype": "z = SHASHTrans( x, gamma, delta, theta, sigma )"
    },
    "sheetpart": {
        "args": [
            "title",
            "childbox"
        ],
        "description": "Returns a display box containing the childbox display box argument with the specified title.",
        "prototype": "y = Sheet Part( title, childbox )"
    },
    "shift": {
        "args": [
            "x",
            "<n=1>"
        ],
        "description": "Returns a copy of list x with the first n items moved to the end of the list, or, if n is negative, the last n items moved to the start.",
        "prototype": "y = Shift( x, <n=1> )"
    },
    "shiftinto": {
        "args": [
            "x",
            "<n=1>"
        ],
        "description": "Modifies list or display box x with the first n items moved to the end of the list, or, if n is negative, the last n items moved to the start. Note that the x argument must be a variable.",
        "prototype": "Shift Into( x, <n=1> )"
    },
    "shortdate": {
        "args": [
            "datetime",
            "<format>"
        ],
        "description": "Returns a numeric (MM/DD/YYYY) locale-specific representation of a date-time value.",
        "prototype": "s = Short Date( datetime, <format> )"
    },
    "shortesteditscript": {
        "args": [
            "A",
            "B);",
            "matrix",
            "=",
            "Shortest",
            "Edit",
            "Script(",
            "strings(",
            "A",
            "B",
            "matrix(1)",
            "limit(9999)",
            ")",
            ");",
            "list",
            "=",
            "Shortest",
            "Edit",
            "Script(",
            "lines(",
            "A",
            "B",
            "separators('defaults",
            "to",
            "newline')",
            "ignore('defaults",
            "to",
            "none')|ignoreWhiteSpace()",
            "matrix(0)",
            "limit(9999)",
            ")",
            ");",
            "matrix",
            "=",
            "Shortest",
            "Edit",
            "Script(",
            "sequences(nA",
            "nB",
            "Function({iA",
            "iB}",
            "adata[iA]",
            "==",
            "bdata[ib]",
            ")",
            ")"
        ],
        "description": "Returns one of the shortest edit scripts to convert string A into string B.  The simple form only returns a list.  strings() and lines() have an option to return a matrix or a list.  sequences() only returns a matrix.  The optional limit() will stop the function early if the edit list has more than limit inserts and deletes.  lines() compares lines rather than characters; the optional ignore('characters') or ignoreWhiteSpace() defaults to no ignored characters.   ESC will stop the function if needed.",
        "prototype": "list = Shortest Edit Script(A,B); matrix = Shortest Edit Script( strings( A, B, matrix(1), limit(9999) ) ); list = Shortest Edit Script( lines( A, B, separators('defaults to newline'), ignore('defaults to none')|ignoreWhiteSpace(), matrix(0), limit(9999) ) ); matrix = Shortest Edit Script( sequences(nA, nB, Function({iA,iB}, adata[iA] == bdata[ib] ) ) )"
    },
    "show": {
        "args": [
            "x",
            "..."
        ],
        "description": "Displays the name and value of the arguments in the log, one per line.",
        "prototype": "Show( x, ... )"
    },
    "showaddinbuilderdialog": {
        "args": [],
        "description": "Brings up a dialog that can be used to make custom add-ins.",
        "prototype": "Show Addin Builder Dialog()"
    },
    "showaddinsdialog": {
        "args": [],
        "description": "Brings up a dialog that shows the status of all registered add-ins.",
        "prototype": "Show Addins Dialog()"
    },
    "showclasses": {
        "args": [
            "<",
            "<class",
            "reference>",
            "...",
            ">"
        ],
        "description": "Show the contents of all user-defined classes, both named and anonymous.",
        "prototype": "Show Classes( < <class reference>, ... > )"
    },
    "showcommands": {
        "args": [],
        "description": "Displays a list of all JSL operators to the log.",
        "prototype": "Show Commands()"
    },
    "showglobals": {
        "args": [],
        "description": "Lists all the currently defined global symbols and their values.",
        "prototype": "Show Globals()"
    },
    "shownamespaces": {
        "args": [
            "<",
            "<namespace",
            "reference>",
            "...",
            ">"
        ],
        "description": "Show the contents of all user defined namespaces, both named and anonymous.",
        "prototype": "Show Namespaces( < <namespace reference>, ... > )"
    },
    "showpreferences": {
        "args": [],
        "description": "Shows the current preference settings in the log.",
        "prototype": "Show Preferences()"
    },
    "showproperties": {
        "args": [
            "object"
        ],
        "description": "Shows in the log the messages that an object responds to.",
        "prototype": "Show Properties( object )"
    },
    "showsymbols": {
        "args": [],
        "description": "Lists all the currently defined symbols and their values.",
        "prototype": "Show Symbols()"
    },
    "simplifyexpr": {
        "args": [
            "expr(",
            "...",
            ")"
        ],
        "description": "Returns an equivalent expression that simplifies the argument expression in various ways.",
        "prototype": "resultExpr = Simplify Expr( expr( ... ) )"
    },
    "sin": {
        "args": [
            "x"
        ],
        "description": "Returns the trigonometric sine of x, where x is an angle in radians.",
        "prototype": "y = Sine( x )"
    },
    "sine": {
        "args": [
            "x"
        ],
        "description": "Returns the trigonometric sine of x, where x is an angle in radians.",
        "prototype": "y = Sine( x )"
    },
    "sinh": {
        "args": [
            "x"
        ],
        "description": "Returns the hyperbolic sine of x.",
        "prototype": "y = SinH( x )"
    },
    "sliderbox": {
        "args": [
            "minValue",
            "maxValue",
            "variable",
            "script",
            "<set",
            "width(n)>",
            "<rescale",
            "slider(minValue",
            "maxValue)>"
        ],
        "description": "Returns a display box that shows a slider control that ranges from minValue to maxValue. As the slider's position changes, its value is placed into variable and the script is run.",
        "prototype": "box = Slider Box(minValue, maxValue, variable, script, <set width(n)>, <rescale slider(minValue, maxValue)>)"
    },
    "slinv": {
        "args": [
            "z",
            "gamma",
            "delta",
            "theta",
            "<sigma=1>"
        ],
        "description": "Transforms a standard normal variable to a Johnson SL variable.",
        "prototype": "x = SlInv( z, gamma, delta, theta, <sigma=1> )"
    },
    "sltrans": {
        "args": [
            "x",
            "gamma",
            "delta",
            "theta",
            "<sigma=1>"
        ],
        "description": "Transforms a Johnson SL variable to a standard normal variable.",
        "prototype": "z = SlTrans( x, gamma, delta, theta, <sigma=1> )"
    },
    "sobolquasirandomsequence": {
        "args": [
            "nDim",
            "nRow"
        ],
        "description": "Generate a sequence of space filling quasi-random numbers using the Sobol sequence in up to 4000 dimensions.",
        "prototype": "points = Sobol Quasi Random Sequence(nDim, nRow)"
    },
    "socket": {
        "args": [
            "<STREAM",
            "|",
            "DGRAM>"
        ],
        "description": "Creates a socket variable that can communicate with sockets on this or another networked computer. The default argument is STREAM. Try your own company's website.",
        "prototype": "socketHandle = Socket( <STREAM | DGRAM> )"
    },
    "solve": {
        "args": [
            "A",
            "B"
        ],
        "description": "Solves the linear system A*x=B for x. The Solve() function is equivalent to Inverse(A)*B if A is non-singular. Note that the A argument must be a square matrix.",
        "prototype": "y = Solve( A, B )"
    },
    "sortascending": {
        "args": [
            "x"
        ],
        "description": "Returns a copy of list or matrix x with the items in ascending order.",
        "prototype": "y = Sort Ascending( x )"
    },
    "sortdescending": {
        "args": [
            "x"
        ],
        "description": "Returns a copy of list or matrix x with the items in descending order.",
        "prototype": "y = Sort Descending( x )"
    },
    "sortlist": {
        "args": [
            "x"
        ],
        "description": "Returns a copy of list x with the items in ascending order.",
        "prototype": "y = Sort List( x )"
    },
    "sortlistinto": {
        "args": [
            "x"
        ],
        "description": "Modifies list x with the items in ascending order. Note that the x argument must be a variable.",
        "prototype": "Sort List Into( x )"
    },
    "spacerbox": {
        "args": [
            "<Size(",
            "h",
            "v",
            ")>",
            "<Color(",
            "c",
            ")>"
        ],
        "description": "Returns a display box that can be used to maintain space between other display boxes or fill a cell in a Lineup Box. The Size arguments are specified in pixels, and the Color argument is any valid JSL color.",
        "prototype": "y = Spacer Box( <Size( h, v )>, <Color( c )>)"
    },
    "sparsesvd": {
        "args": [
            "X",
            "<nSingularValues=min(nRow",
            "nCol)>",
            "<tolerance=1e-10>"
        ],
        "description": "Computes the singular value decomposition of matrix X using the implicitly restarted, partially reorthogonalized Lanczos method for sparse matrices by returning a list {U, M, V} such that U*diag(M)*V` is equal to X.",
        "prototype": "{U, M, V} = Sparse SVD( X , <nSingularValues=min(nRow,nCol)>, <tolerance=1e-10>)"
    },
    "speak": {
        "args": [
            "text",
            "<Wait(",
            "sync",
            ")>"
        ],
        "description": "Speaks the text if supported by the operating system. Specifying the optional Wait(true) argument delays script execution until speech has finished.",
        "prototype": "Speak( text, <Wait( sync )> )"
    },
    "spinbox": {
        "args": [
            "<script>"
        ],
        "description": "Returns a display box to show a button with up/down controls.  The script argument is invoked with an argument indicating the direction of the arrow clicked (negative is down, positive is up).  A magnitude of 1 indicates a single click, while larger values may be used to indicate a repeating action.",
        "prototype": "y = Spin Box( <script> )"
    },
    "splinecoef": {
        "args": [
            "x",
            "y",
            "lambda",
            "<weight>"
        ],
        "description": "Returns a 5-column matrix of the form: knots||a||b||c||d for each of the unique values in x.",
        "prototype": "coef = Spline Coef( x, y, lambda, <weight> )"
    },
    "splineeval": {
        "args": [
            "x",
            "coef",
            "<extrapolation=-1>"
        ],
        "description": "Evaluates the spline predictions using the coef matrix in the same form as returned by the Spline Coef() function. extrapolation indicates how far beyond the spline range, as a fraction of the range, to extend evaluation before returning missing values.",
        "prototype": "yhat = Spline Eval( x, coef, <extrapolation=-1> )"
    },
    "splinesmooth": {
        "args": [
            "x",
            "y",
            "lambda",
            "<weight>"
        ],
        "description": "Returns the smoothed predicted values from a spline fit. Larger values of lambda cause greater stiffness.",
        "prototype": "yhat = Spline Smooth( x, y, lambda, <weight> )"
    },
    "sqrt": {
        "args": [
            "x"
        ],
        "description": "Returns the positive square root of the x argument, which can be a number, matrix, or list of numbers.",
        "prototype": "y = Sqrt( x )"
    },
    "squash": {
        "args": [
            "x"
        ],
        "description": "Returns 1 / (1 + Exp( x )), which converts a number in the domain -∞...+∞ into range 1...0. The Squash() function is useful in logistic regression.",
        "prototype": "y = Squash( x )"
    },
    "squish": {
        "args": [
            "x"
        ],
        "description": "Returns 1 / (1 + Exp( -x )), which converts a number in the domain -∞...+∞ into range 0...1. The Logist() function is useful in logistic regression.",
        "prototype": "y = Logist( x )"
    },
    "ssq": {
        "args": [
            "x1",
            "..."
        ],
        "description": "Returns the sum of squares of all elements",
        "prototype": "y = SSQ( x1, ... )"
    },
    "startswith": {
        "args": [
            "s",
            "sub"
        ],
        "description": "Returns 1 if s starts with sub, otherwise returns 0. The s and sub arguments can be both strings or both lists. Equivalent to Left( s, Length( sub )) == sub.",
        "prototype": "b = Starts With( s, sub )"
    },
    "statusmsg": {
        "args": [
            "message"
        ],
        "description": "Displays the specified message in the status bar.",
        "prototype": "Status Msg( message )"
    },
    "stddev": {
        "args": [
            "x1",
            "..."
        ],
        "description": "Returns the standard deviation of the arguments or of the values within a single matrix or list argument.",
        "prototype": "y = Std Dev( x1, ... )"
    },
    "step": {
        "args": [
            "x",
            "x1",
            "y1",
            "x2",
            "y2",
            "...",
            ")\r\ny",
            "=",
            "Step(",
            "x",
            "[x1",
            "x2",
            "...]",
            "[y1",
            "y2",
            "...]"
        ],
        "description": "Returns the yi argument corresponding to the largest xi value which satisfies xi less than or equal to the x argument. Note that the xi arguments must be specified in order.",
        "prototype": "y = Step( x, x1, y1, x2, y2, ... )\r\ny = Step( x, [x1, x2, ...], [y1, y2, ...] )"
    },
    "stop": {
        "args": [],
        "description": "Immediately terminates the execution of a JSL script",
        "prototype": "Stop()"
    },
    "straightlinedepreciation": {
        "args": [
            "cost",
            "salvage",
            "life"
        ],
        "description": "Returns the straight-line depreciation of an asset for one period. Equivalent to the SLN function in Microsoft Excel.",
        "prototype": "x = Straight Line Depreciation( cost, salvage, life )"
    },
    "stringcolbox": {
        "args": [
            "title",
            "{strings}"
        ],
        "description": "Returns a display box to show the strings specified by the strings argument, which is a list of character strings.",
        "prototype": "y = String Col Box( title, {strings} )"
    },
    "stringcoleditbox": {
        "args": [
            "title",
            "{strings}"
        ],
        "description": "Returns a display box to show the strings specified by the strings argument, which is a list of character strings.",
        "prototype": "y = String Col Edit Box( title, {strings} )"
    },
    "studentstdensity": {
        "args": [
            "q",
            "df",
            "<nonCentrality=0>"
        ],
        "description": "Returns the density function of Student's t.",
        "prototype": "p = t Density( q, df, <nonCentrality=0> )"
    },
    "studentstdistribution": {
        "args": [
            "q",
            "df",
            "<nonCentrality=0>"
        ],
        "description": "Returns the probability that a Student's t distributed random variable is less than q.",
        "prototype": "p = t Distribution( q, df, <nonCentrality=0> )"
    },
    "studentstquantile": {
        "args": [
            "p",
            "df",
            "<nonCentrality=0>"
        ],
        "description": "Returns the quantile from a Student's t distribution, the value for which the probability is p that a random value would be lower.",
        "prototype": "q = t Quantile( p, df, <nonCentrality=0> )"
    },
    "subscribetodatatablelist": {
        "args": [
            "<subscriber",
            "name",
            "|",
            "''>",
            "<OnOpen",
            "|",
            "OnClose>"
        ],
        "description": "Subscribe to the data table list to be notified when a new data table has been added or closed.",
        "prototype": "aSub = Subscribe to Data Table List( <subscriber name | ''>, <OnOpen | OnClose>)"
    },
    "subscript": {
        "args": [
            "x",
            "i"
        ],
        "description": "Returns the ith value of a subscriptable object, which can be a column of a data table, a matrix, a list, or a report display element.",
        "prototype": "y = x[i]; y = m[row, col]; y = Subscript( x, i )"
    },
    "substitute": {
        "args": [
            "x",
            "patternExpr1",
            "replacementExpr1",
            "..."
        ],
        "description": "Returns a copy of string, list or expression x, replacing instances of each pattern expression with the corresponding replacement expression.",
        "prototype": "y = Substitute( x, patternExpr1, replacementExpr1, ... )"
    },
    "substituteinto": {
        "args": [
            "x",
            "patternExpr1",
            "replacementExpr1",
            "..."
        ],
        "description": "Modifies string, list or expression x, replacing instances of each pattern expression with the corresponding replacement expression. Note that the x argument must be a variable.",
        "prototype": "Substitute Into( x, patternExpr1, replacementExpr1, ... )"
    },
    "substr": {
        "args": [
            "s",
            "start",
            "<count>"
        ],
        "description": "Returns the part of string s composed of count characters starting at position start. A negative or absent count means the rest of the string. A negative start means starting start characters from the end. The Substr() function can also be applied to lists.",
        "prototype": "sub = Substr( s, start, <count> )"
    },
    "subtract": {
        "args": [
            "x0",
            "x1",
            "..."
        ],
        "description": "Subtracts all subsequent arguments from the first argument. Arguments can be numbers, matrices, or lists of numbers.",
        "prototype": "y = x0 - x1; y = Subtract( x0, x1, ... )"
    },
    "subtractto": {
        "args": [
            "y",
            "x"
        ],
        "description": "Subtracts a value from a variable or from a list of variables.",
        "prototype": "y -= x; Subtract To( y, x )"
    },
    "suinv": {
        "args": [
            "z",
            "gamma",
            "delta",
            "theta",
            "sigma"
        ],
        "description": "Transforms a standard normal variable to an unbounded Johnson variable.",
        "prototype": "x = SuInv( z, gamma, delta, theta, sigma )"
    },
    "sum": {
        "args": [
            "x1",
            "..."
        ],
        "description": "Returns the sum of the arguments or of the values within a single matrix or list argument.",
        "prototype": "y = Sum( x1, ... )"
    },
    "summarize": {
        "args": [
            "<dt",
            ">",
            "nameBy=By(",
            "colBy",
            ")",
            "name1=statName1(",
            "col1",
            ")",
            "..."
        ],
        "description": "Calculates various summary statistics across a By column. The statistic names are Count, Sum, Mean, Max or Maximum, Min or Minimum, StdDev, Corr, Quantile, First. The statistics can be calculated only for numeric columns. The results are stored as matrices in variables with the specified names.",
        "prototype": "Summarize( <dt,> nameBy=By( colBy ), name1=statName1( col1 ), ... )"
    },
    "summarizeybyx": {
        "args": [
            "X(x",
            "columns)",
            "Y(y",
            "columns)",
            "Group(grouping",
            "columns)",
            "Freq(freq",
            "column)",
            "Weight(Weight",
            "column)"
        ],
        "description": "Calculates all Fit Y by X combinations",
        "prototype": "Summarize YByX( X(x columns),Y(y columns), Group(grouping columns), Freq(freq column), Weight(Weight column))"
    },
    "summation": {
        "args": [
            "assignExpr",
            "limit",
            "bodyExpr"
        ],
        "description": "Returns the sum of evaluations of the bodyExpr arguments, each time incrementing the variable from the assignExpr argument until it is greater than or equal to the limit argument.",
        "prototype": "y = Summation( assignExpr, limit, bodyExpr )"
    },
    "sumofyearsdigitsdepreciation": {
        "args": [
            "cost",
            "salvage",
            "life",
            "per"
        ],
        "description": "Returns the sum-of-years' digits depreciation of an asset for a specified period. Equivalent to the SYD function in Microsoft Excel.",
        "prototype": "x = Sum Of Years Digits Depreciation( cost, salvage, life, per )"
    },
    "suppressformulaeval": {
        "args": [
            "<suppress=1>"
        ],
        "description": "Suppresses the evaluation of formulas in all data tables if the argument if nonzero.",
        "prototype": "Suppress Formula Eval( <suppress=1> )"
    },
    "sutrans": {
        "args": [
            "x",
            "gamma",
            "delta",
            "theta",
            "sigma"
        ],
        "description": "Transforms an unbounded Johnson variable to a standard normal variable.",
        "prototype": "z = SuTrans( x, gamma, delta, theta, sigma )"
    },
    "svd": {
        "args": [
            "X"
        ],
        "description": "Computes the singular value decomposition of matrix X by returning a list {U, M, V} such that U*diag(M)*V` is equal to X.",
        "prototype": "{U, M, V} = SVD( X )"
    },
    "sweep": {
        "args": [
            "A",
            "<indices>"
        ],
        "description": "Returns the sweep of the matrix A on diagonal pivots indicated by indices. This is a way of inverting a matrix one pivot at a time.",
        "prototype": "y = Sweep( A, <indices> )"
    },
    "tabbox": {
        "args": [
            "tabName1",
            "tabExpr1",
            "..."
        ],
        "description": "Creates a tabbed-page panel in a display box window.",
        "prototype": "y = Tab Box( tabName1, tabExpr1, ... )"
    },
    "tablebox": {
        "args": [
            "displayBox",
            "..."
        ],
        "description": "Returns a display box that composes a table of the String Col Box, Number Col Box, and Plot Col Box column display boxes provided by the arguments.",
        "prototype": "y = Table Box( displayBox, ... )"
    },
    "tabpagebox": {
        "args": [
            "[options",
            "]",
            "contents"
        ],
        "description": "Returns a display box that that can be used in a Tab Box or as a stand-alone container with title. Recognized options include Title(string) to specify a title, Tip(string) to specify a tooltip, Closeable(0|1) to specify whether the page can be closed, Icon(string) to specify the icon, and Moveable(0|1) to specify whether the page can be moved.",
        "prototype": "y = Tab Page Box( [options,] contents)"
    },
    "tan": {
        "args": [
            "x"
        ],
        "description": "Returns the trigonometric tangent of x, where x is an angle in radians.",
        "prototype": "y = Tangent( x )"
    },
    "tangent": {
        "args": [
            "x"
        ],
        "description": "Returns the trigonometric tangent of x, where x is an angle in radians.",
        "prototype": "y = Tangent( x )"
    },
    "tanh": {
        "args": [
            "x"
        ],
        "description": "Returns the hyperbolic tangent of x.",
        "prototype": "y = TanH( x )"
    },
    "tdensity": {
        "args": [
            "q",
            "df",
            "<nonCentrality=0>"
        ],
        "description": "Returns the density function of Student's t.",
        "prototype": "p = t Density( q, df, <nonCentrality=0> )"
    },
    "tdistribution": {
        "args": [
            "q",
            "df",
            "<nonCentrality=0>"
        ],
        "description": "Returns the probability that a Student's t distributed random variable is less than q.",
        "prototype": "p = t Distribution( q, df, <nonCentrality=0> )"
    },
    "text": {
        "args": [
            "<properties>",
            "{x",
            "y}",
            "text",
            "...",
            ")\r\nText(",
            "{left",
            "top",
            "right",
            "bottom}",
            "text"
        ],
        "description": "Moves to the {x, y} position and draws text specified by the text argument. Named property arguments include Center Justified, Right Justified, Erased, Boxed, Counterclockwise, Clockwise. The position arguments, named arguments, and strings can be mixed in any order. You can also use four x, y coordinates to describe a box within which to draw the text. In that case, properties are not used.",
        "prototype": "Text( <properties>, {x, y}, text, ... )\r\nText( {left, top, right, bottom}, text )"
    },
    "textbox": {
        "args": [
            "text",
            "<<Justify",
            "Text(",
            "strPos",
            ")",
            "<<Set",
            "Wrap(",
            "width",
            ")"
        ],
        "description": "Constructs a display box that contains the text in the string argument text. The optional arguments are available to control the text justification or set the text wrap width. The argument for Justify Text should be a string containing left, right, or center.",
        "prototype": "y = Text Box( text, <<Justify Text( strPos ), <<Set Wrap( width ) )"
    },
    "textcolor": {
        "args": [
            "<name|index|rgbList>"
        ],
        "description": "Sets the color for drawing text.",
        "prototype": "Text Color( <name|index|rgbList> )"
    },
    "texteditbox": {
        "args": [
            "text",
            "<<Password",
            "Style(",
            "bool",
            ")",
            "<<Set",
            "Script(",
            "script",
            ")",
            "<<Set",
            "Width(",
            "value",
            ")"
        ],
        "description": "Constructs an editable box that contains the quoted string text, returning the display box reference. The optional arguments are available to control the display of the text, to attach a script to the text box, and to set the width in pixels of the text box. Specifying Set Width(-1) forces a resize to content. Note that a script can be attached to the text edit box either by adding the script as an optional argument or by sending the Set Script message.",
        "prototype": "y = Text Edit Box( text, <<Password Style( bool ), <<Set Script( script ), <<Set Width( value ) )"
    },
    "textfont": {
        "args": [],
        "description": "Sets the font for subsequent Text() drawing. Use without any arguments to get the current font settings. Angle is in degrees clockwise.",
        "prototype": "{nm, sz, st, an} = Text Font(fontName, <size>, <'bold italic underline strikeout'>, <angle>"
    },
    "textscore": {
        "args": [
            "text",
            "column",
            "text-to-number",
            "<weighting>",
            "<{<center",
            "><scale",
            ">scoring",
            "matrix}>"
        ],
        "description": "Used to create scoring formulas in Text Explorer. The text-to-number argument is an associative array mapping lowercase words to numbers. The weighting argument is either 'Binary', 'Ternary', 'Count', 'LogCount', 'LCA' or an array of inverse document frequency weights for TFLogIDF. The scoring matrix must have the same number of columns as words in the associative array, or one more if LCA. The output is a vector of scores. If no scoring matrix is specified, it returns a vector of count scores. If no weighting is specified, it uses Count. This function does not support the Stem for Combining option.",
        "prototype": "score vector = Text Score( text column, text-to-number, <weighting>, <{<center,><scale,>scoring matrix}>); "
    },
    "textsize": {
        "args": [
            "n"
        ],
        "description": "Sets the font size that for text drawing.",
        "prototype": "Text Size( n )"
    },
    "thisproject": {
        "args": [],
        "description": "From within a project, returns the corresponding project object. Outside of a project, returns nothing.",
        "prototype": "project = this project()"
    },
    "throw": {
        "args": [],
        "description": "Diverts execution to the enclosing Try(). Otherwise, script execution is stopped.",
        "prototype": "Throw()"
    },
    "tickseconds": {
        "args": [],
        "description": "Returns a time value in seconds, usually accurate to at least 1/60 of a second (a 'tick'), depending on the computer. Only useful relative to another Tick Seconds() value.",
        "prototype": "t = Tick Seconds()"
    },
    "timeofday": {
        "args": [
            "datetime"
        ],
        "description": "Returns the time part of a date-time value, including any fractional seconds.",
        "prototype": "sec = Time Of Day( datetime )"
    },
    "titlecase": {
        "args": [
            "s"
        ],
        "description": "Converts to title case",
        "prototype": "st = Titlecase( s )"
    },
    "tlogcdistribution": {
        "args": [
            "x",
            "df",
            "<nc>"
        ],
        "description": "Returns the log of 1 - t distribution.",
        "prototype": "y = t Log CDistribution( x, df, <nc> )"
    },
    "tlogdensity": {
        "args": [
            "x",
            "df",
            "<nc>"
        ],
        "description": "Returns the log of the t probability density.",
        "prototype": "y = t Log Density( x, df, <nc> )"
    },
    "tlogdistribution": {
        "args": [
            "x",
            "df",
            "<nc>"
        ],
        "description": "Returns the log of the t distribution.",
        "prototype": "y = t Log Distribution( x, df, <nc> )"
    },
    "tnoncentrality": {
        "args": [
            "x",
            "df",
            "prob"
        ],
        "description": "Solves for the noncentrality parameter of a Student's t distribution such that prob = t Distribution( x, df, nc ).",
        "prototype": "nc = t Noncentrality( x, df, prob )"
    },
    "today": {
        "args": [],
        "description": "Returns the date-time value of the current moment.",
        "prototype": "dt = Today()"
    },
    "tolerancelimit": {
        "args": [
            "1-alpha",
            "p",
            "n"
        ],
        "description": "Constructs a 1-alpha confidence interval to contain proportion p of the means with sample size n.",
        "prototype": "q = Tolerance Limit( 1-alpha, p, n )"
    },
    "tquantile": {
        "args": [
            "p",
            "df",
            "<nonCentrality=0>"
        ],
        "description": "Returns the quantile from a Student's t distribution, the value for which the probability is p that a random value would be lower.",
        "prototype": "q = t Quantile( p, df, <nonCentrality=0> )"
    },
    "trace": {
        "args": [
            "x"
        ],
        "description": "Returns the sum of the diagonal elements of a square matrix.",
        "prototype": "y = Trace( x )"
    },
    "transparency": {
        "args": [
            "<alpha>"
        ],
        "description": "Sets the transparency used in the drawing commands. Alpha ranges between 0 (clear) and 1 (opaque, the default). Some operating systems do not support this.",
        "prototype": "Transparency( <alpha> )"
    },
    "transpose": {
        "args": [
            "matrix"
        ],
        "description": "Transposes the matrix argument by interchanging the rows and columns.",
        "prototype": "y = Transpose( matrix ); y = matrix`"
    },
    "treebox": {
        "args": [
            "<{rootnodes}>",
            "<Size(",
            "width",
            "height",
            ")>",
            "<Multiselect(",
            "0|1",
            ")>"
        ],
        "description": "Constructs a display box to show hierarchical information.",
        "prototype": "tree = Tree Box( <{rootnodes}>, <Size( width, height )>, <Multiselect( 0|1 )> )"
    },
    "treenode": {
        "args": [
            "<label>"
        ],
        "description": "Constructs a tree node intended for display within a Tree Box.",
        "prototype": "node = Tree Node( <label> )"
    },
    "triangulation": {
        "args": [
            "X(Column1",
            "Column2)",
            "<",
            "Y(Column)",
            ">"
        ],
        "description": "Returns an object containing the Delaunay triangulation of the given point set. The optional Y will be averaged for duplicate points, and all points in the output will be unique.",
        "prototype": "triangulation = Triangulation( X(Column1, Column2), < Y(Column) > )"
    },
    "trigamma": {
        "args": [
            "x"
        ],
        "description": "Returns the trigamma function evaluated at x, where the trigamma function is the derivative of the digamma function.",
        "prototype": "y = Trigamma( x )"
    },
    "trim": {
        "args": [
            "s",
            "<left|right|both>"
        ],
        "description": "Returns a copy of string s with any leading or trailing whitespace characters removed. The second argument specifies either the leading or trailing whitespace characters. If you do not specify the second argument, whitespace characters are removed from both ends.",
        "prototype": "sub = Trim( s, <left|right|both> )"
    },
    "trimwhitespace": {
        "args": [
            "s",
            "<left|right|both>"
        ],
        "description": "Returns a copy of string s with any leading or trailing whitespace characters removed. The second argument specifies either the leading or trailing whitespace characters. If you do not specify the second argument, whitespace characters are removed from both ends.",
        "prototype": "sub = Trim Whitespace( s, <left|right|both> )"
    },
    "triplesimport": {
        "args": [
            "<path",
            "to",
            "xml",
            "file>"
        ],
        "description": "Opens Triple-S files. The Triple-S format comprises an xml or sss file and either a csv file or a dat/asc file. Both files must have the same name with the appropriate extension and must be in the same directory. Specify the xml or sss filepath to import the data.",
        "prototype": "TripleSImport( <path to xml file> )"
    },
    "try": {
        "args": [
            "expr",
            "<catchExpr>"
        ],
        "description": "Evaluates and returns the expr argument, unless the evaluation causes a Throw() or internal exception. In that case, the evaluation of catchExpr is returned. If you use exception_msg as the catchExpr, a list containing more information about the error is returned.",
        "prototype": "y = Try( expr, <catchExpr> )"
    },
    "tukeyhsdpvalue": {
        "args": [
            "q",
            "nGroups",
            "dfe"
        ],
        "description": "Returns the p-value from Tukey's HSD multiple comparisons test, where q is the test statistic, nGroups is the number of groups in the study, and dfe is the error degrees of freedom (based on the total study sample).\r\n\r\nNote that q is Tukey's adjusted critical value, which is the quantile of Tukey's studentized range distribution divided by the sqrt(2).",
        "prototype": "p = Tukey HSD P value( q, nGroups, dfe )"
    },
    "tukeyhsdquantile": {
        "args": [
            "1-alpha",
            "nGroups",
            "dfe"
        ],
        "description": "Returns the quantile needed in Tukey's HSD multiple comparisons test, where 1-alpha is the confidence level, nGroups is the number of groups in the study, and dfe is the error degrees of freedom (based on the total study sample).\r\n\r\nNote that q is Tukey's adjusted critical value, which is the quantile of Tukey's studentized range distribution divided by the sqrt(2).",
        "prototype": "q = Tukey HSD Quantile( 1-alpha, nGroups, dfe )"
    },
    "type": {
        "args": [
            "x"
        ],
        "description": "Returns a string naming the type of the value of the argument x.",
        "prototype": "y = Type( x )"
    },
    "unlineupbox": {
        "args": [
            "displayBoxArgs",
            "..."
        ],
        "description": "Returns a display box that temporarily suspends the column layout of a Lineup Box. The child of the Unlineup Box will be stretched to span all columns of the Lineup Box.",
        "prototype": "y = UnLineup Box(displayBoxArgs, ... )"
    },
    "unlockglobals": {
        "args": [
            "name",
            "..."
        ],
        "description": "Unlocks specified global names, allowing them to be modified and to be cleared by the Clear Globals function.",
        "prototype": "Unlock Globals( name, ... )"
    },
    "unlocksymbols": {
        "args": [
            "name",
            "..."
        ],
        "description": "Unlocks specified global names, allowing them to be modified and to be cleared by the Clear Symbols function.",
        "prototype": "Unlock Symbols( name, ... )"
    },
    "unregisteraddin": {
        "args": [
            "uniqueId"
        ],
        "description": "Unregister an add-in",
        "prototype": "Unregister Addin( uniqueId)"
    },
    "unsubscribetodatatablelist": {
        "args": [
            "<subscriber",
            "name>",
            "<OnOpen",
            "|",
            "OnClose",
            "|",
            "'ALL'>"
        ],
        "description": "Remove a subscription to the data table list that had been added thru the command 'subscribe to data table list'.",
        "prototype": "aSub = Unsubscribe to Data Table List(<subscriber name>, <OnOpen | OnClose | 'ALL'>)"
    },
    "uppercase": {
        "args": [
            "s"
        ],
        "description": "Converts lowercase letters to uppercase letters in the specified string. Rules for upper and lower case are locale dependent.",
        "prototype": "su = Uppercase( s )"
    },
    "varimax": {
        "args": [
            "F",
            "<norm=1>"
        ],
        "description": "Performs a varimax rotation of the specified matrix F. Returns a list that contains the rotated matrix and the orthogonal rotation matrix. By default, a normalized varimax rotation is performed. Specify norm = 0 to perform a non-normalized varimax rotation.",
        "prototype": "{R,T} = Varimax( F, <norm=1> )"
    },
    "vcenterbox": {
        "args": [
            "<childbox>"
        ],
        "description": "Returns a display box with the childbox display box argument centered in the vertical space defined by the maximum size of that child and all the other siblings of the center box.",
        "prototype": "y = V Center Box( <childbox> )"
    },
    "vconcat": {
        "args": [
            "a",
            "b",
            "..."
        ],
        "description": "Concatenates matrices vertically. Arguments must have same number of columns.",
        "prototype": "y = a |/ b; y = V Concat( a, b, ... )"
    },
    "vconcatto": {
        "args": [
            "matrix1",
            "matrix2"
        ],
        "description": "Concatenates in place, vertically. a |/= b is equivalent to a = a |/ b. This is an assignment operator.",
        "prototype": "matrix1 |/= matrix2; V Concat To( matrix1, matrix2 )"
    },
    "vecdiag": {
        "args": [
            "x"
        ],
        "description": "Returns the diagonal elements of the square matrix as a vector.",
        "prototype": "y = Vec Diag( x )"
    },
    "vecquadratic": {
        "args": [
            "S",
            "X"
        ],
        "description": "Evaluates as Vec Diag( X * S * X` ).",
        "prototype": "Vec Quadratic( S, X )"
    },
    "vline": {
        "args": [
            "x",
            ");",
            "V",
            "Line(",
            "x",
            "y1",
            "y2"
        ],
        "description": "Draws a vertical line at x from y1 to y2 or through the entire frame.",
        "prototype": "V Line( x ); V Line( x, y1, y2 )"
    },
    "vlistbox": {
        "args": [
            "<Align(",
            "center|right",
            ")>",
            "displayBox",
            "..."
        ],
        "description": "Returns a display box that arranges the display boxes provided by the arguments in a vertical layout. The <<Hold message tells the sheet to own the report(s) that will be excerpted. The optional Align argument allows for right or center alignment of the contents inside the display box.",
        "prototype": "y = V List Box( <Align( center|right )>, displayBox, ... )"
    },
    "vmax": {
        "args": [
            "matrix"
        ],
        "description": "Returns a row vector containing the maximum of each column in the argument.",
        "prototype": "b = V Max( matrix )"
    },
    "vmean": {
        "args": [
            "matrix"
        ],
        "description": "Returns a row vector containing the mean of each column in the argument.",
        "prototype": "m = V Mean( matrix )"
    },
    "vmedian": {
        "args": [
            "matrix"
        ],
        "description": "Returns a row vector containing the median of each column in the argument.",
        "prototype": "m = V Mean( matrix )"
    },
    "vmin": {
        "args": [
            "matrix"
        ],
        "description": "Returns a row vector containing the minimum of each column in the argument.",
        "prototype": "a = V Min( matrix )"
    },
    "vptree": {
        "args": [
            "[",
            "matrix",
            "]"
        ],
        "description": "Returns a table for efficiently looking up near neighbors. The matrix arguments are k-dimensional points. There is no built in limit on the number of dimensions or points.",
        "prototype": "tab = VPTree( [ matrix ] )"
    },
    "vquantile": {
        "args": [
            "matrix",
            "p"
        ],
        "description": "Returns a row vector containing the specified quantile p of each column in the argument.",
        "prototype": "m = V Quantile( matrix, p )"
    },
    "vscrollbox": {
        "args": [
            "<Size(",
            "v",
            ")>",
            "displayBox"
        ],
        "description": "Returns a display box that positions a larger child box using a vertical scroll bar.",
        "prototype": "y = V Scroll Box( <Size( v )>, displayBox )"
    },
    "vsheetbox": {
        "args": [
            "<<Hold(",
            "rpt",
            ")",
            "displayBox",
            "..."
        ],
        "description": "Returns a display box that arranges the display boxes provided by the arguments in a vertical layout. The <<Hold message tells the sheet to own the report(s) that will be excerpted. The optional Align argument allows for right or center alignment of the contents inside the display box.",
        "prototype": "y = V Sheet Box( <<Hold( rpt ), displayBox, ... )"
    },
    "vsize": {
        "args": [],
        "description": "Returns the vertical size of the graphics frame in pixels.",
        "prototype": "v = V Size()"
    },
    "vsplitterbox": {
        "args": [
            "<Size(x",
            "y)>",
            "displayBox",
            "..."
        ],
        "description": "Returns a display box that arranges other display boxes vertically, with interactive control of sizes. Child sizes are specified as a proportion of the width or height of the Splitter Box. The optional Size argument is only used for the top-most Splitter Box; lower level boxes are sized like any other child box.",
        "prototype": "y = V Splitter Box( <Size(x,y)>, displayBox, ... )"
    },
    "vstandardize": {
        "args": [
            "X"
        ],
        "description": "Returns a matrix that is the centered and scaled version of matrix X. Each column of b has mean 0 and standard deviation 1.",
        "prototype": "b = V Standardize( X )"
    },
    "vstd": {
        "args": [
            "matrix"
        ],
        "description": "Returns a row vector containing the standard deviations of each column in the argument.",
        "prototype": "b = V Std( matrix )"
    },
    "vsum": {
        "args": [
            "matrix"
        ],
        "description": "Returns a row vector containing the sum of each column in the argument.",
        "prototype": "s = V Sum( matrix )"
    },
    "wait": {
        "args": [
            "x"
        ],
        "description": "Waits for x seconds before proceeding with execution. While the script is waiting, other events such as screen drawing continue to proceed.",
        "prototype": "Wait( x )"
    },
    "watch": {
        "args": [
            "all|name1",
            "..."
        ],
        "description": "Creates a window showing variables (global, local, and variables within namespaces) and their values.",
        "prototype": "w = Watch( all|name1, ... )"
    },
    "web": {
        "args": [
            "string",
            "<JMP",
            "Window>"
        ],
        "description": "Opens the URL or file stored in string in the default web browser. The optional second argument specifies that the HTML open in a JMP browser window.",
        "prototype": "Web( string, <JMP Window> )"
    },
    "webbrowserbox": {
        "args": [
            "url"
        ],
        "description": "Returns a display box to view a web page, specified by the url string argument.",
        "prototype": "y = Web Browser Box( url )"
    },
    "weekofyear": {
        "args": [
            "datetime",
            "<rule=1>"
        ],
        "description": "Returns the week of the year containing a date-time value using one of three rules. By default (rule 1), weeks start on Sunday with the first Sunday of the year being week 2. Week 1 will be a partial week or empty (as in 2006). For rule 2, the first Sunday is week 1, with previous days being week 0. For rule 3, the ISO week number is returned, where weeks start on Monday and week 1 is the first week of the year with four days in that year. With ISO weeks, it's possible for the first or last three days of the year to belong to the neighboring year's week number.",
        "prototype": "d = Week Of Year( datetime, <rule=1> )"
    },
    "weibulldensity": {
        "args": [
            "x",
            "shape",
            "<scale=1>",
            "<threshold=0>"
        ],
        "description": "Returns the density at x of a Weibull probability distribution with a shape parameter and optional scale parameter.",
        "prototype": "y = Weibull Density( x, shape, <scale=1>, <threshold=0> )"
    },
    "weibulldistribution": {
        "args": [
            "x",
            "shape",
            "<scale=1>",
            "<threshold=0>"
        ],
        "description": "Returns the probability that a Weibull distributed random variable (with a shape parameter and optional scale parameter) is less than x.",
        "prototype": "p = Weibull Distribution( x, shape, <scale=1>, <threshold=0> )"
    },
    "weibullquantile": {
        "args": [
            "p",
            "beta",
            "<alpha=1>",
            "<threshold=0>"
        ],
        "description": "Returns the quantile from a Weibull distribution, the value for which the probability is p that a random value would be lower, where beta and alpha are the shape and scale parameters, respectively.",
        "prototype": "q = Weibull Quantile( p, beta, <alpha=1>, <threshold=0> )"
    },
    "while": {
        "args": [
            "testExpr",
            "bodyExpr"
        ],
        "description": "Evaluates the testExpr and bodyExpr expressions repeatedly as long as testExpr evaluates to a nonzero value.",
        "prototype": "While( testExpr, bodyExpr )"
    },
    "wild": {
        "args": [],
        "description": "Denotes a wildcard position that matches any expression (only used in expression patterns).",
        "prototype": "Wild()"
    },
    "wildlist": {
        "args": [],
        "description": "Denotes a series of wildcard arguments that match anything (only used in expression patterns).",
        "prototype": "Wild List()"
    },
    "window": {
        "args": [
            "<string|int>"
        ],
        "description": "This function is deprecated and retained only for backward compatibility with existing scripts. For new scripts, use Get Window() or Get Window List().",
        "prototype": "y = Window( <string|int> )"
    },
    "word": {
        "args": [
            "n|[first",
            "last]",
            "s",
            "<delim>",
            "<Unmatched(result",
            "string"
        ],
        "description": "Returns the nth word of string s, where words are sub-strings separated by any number of any of the characters in the delim argument. If delim is absent, whitespace characters are used. If delim is the empty string, each character is treated as a separate word.",
        "prototype": "w = Word( n|[first last], s, <delim>, <Unmatched(result string)>"
    },
    "words": {
        "args": [
            "<[first",
            "last]>",
            "s",
            "<delim>"
        ],
        "description": "Returns a list of sub-strings separated by any of the characters specified in the delim argument. If delim is absent, whitespace characters are used. If delim is the empty string, each character is treated as a separate word.",
        "prototype": "wl = Words( <[first last]>, s, <delim>)"
    },
    "wraplistbox": {
        "args": [
            "displayBox",
            "..."
        ],
        "description": "Returns a display box that arranges the display boxes provided by the arguments in a horizontal layout, but will wrap that list when printing.",
        "prototype": "y = Wrap List Box( displayBox, ... )"
    },
    "write": {
        "args": [
            "x",
            "..."
        ],
        "description": "Displays the specified values in the log without adding quotation marks, spaces, or line breaks (as Print() does).",
        "prototype": "Write( x, ... )"
    },
    "xfunction": {
        "args": [
            "xExpr",
            "yName",
            "<properties>"
        ],
        "description": "Draws function xExpr in the X dimension as variable yName varies across the range of the Y axis of the graph. Additional named property arguments include Min(minimum X), Max(maximum Y), Fill(fill pattern, value to fill to), Inc(upper bound of increment).",
        "prototype": "X Function( xExpr, yName, <properties> )"
    },
    "xmlattr": {
        "args": [
            "attr",
            "name",
            ");",
            "aa",
            "=",
            "XML",
            "Attr("
        ],
        "description": "Extracts the string value of an XML attribute in the context of being evaluating in a Parse XML() command. If no name if given, an associative array of all attribute name/value pairs is returned.",
        "prototype": "value = XML Attr( attr name ); aa = XML Attr()"
    },
    "xmldecode": {
        "args": [
            "textxml"
        ],
        "description": "Decodes symbols in XML to ordinary text, changes &quot; to ', &lt; to <, &gt to >; &amp; to &.",
        "prototype": "text = XML Decode( textxml )"
    },
    "xmlencode": {
        "args": [
            "text"
        ],
        "description": "Prepares text for embedding in XML, changes ' to &quot;, < to &lt;, > to &gt; & to &amp;.",
        "prototype": "textxml = XML Encode( text )"
    },
    "xmltext": {
        "args": [],
        "description": "Extracts the string text of the body of an XML tag in the context of being evaluating in a Parse XML() command.",
        "prototype": "value = XML Text()"
    },
    "xorigin": {
        "args": [],
        "description": "Returns the x value for the left edge of the graphics frame.",
        "prototype": "x = X Origin()"
    },
    "xpathquery": {
        "args": [
            "xml",
            "xpath",
            "expression"
        ],
        "description": "Runs an XPath query against an XML document.",
        "prototype": "result = XPath Query(xml, xpath expression)"
    },
    "xrange": {
        "args": [],
        "description": "Returns the x distance from left to right. X Origin() + X Range() is the right edge.",
        "prototype": "x = X Range()"
    },
    "xscale": {
        "args": [
            "<xMin>",
            "<xMax>"
        ],
        "description": "Sets a new scale for the graphics frame.",
        "prototype": "X Scale( <xMin>, <xMax> )"
    },
    "xyfunction": {
        "args": [
            "x(t)",
            "y(t)",
            "t",
            "min(0)",
            "max(1)",
            "inc(.01)",
            "|",
            "steps(100)"
        ],
        "description": "This graphic script function combines an expression x(t) and an expression y(t) to draw an x-y curve for the specified range of parameter t. Inc() is the maximum increment on t, or steps() is the minimum number of steps on t. Use steps() or inc() if the default value does not show details.",
        "prototype": "XY Function( x(t), y(t), t, min(0), max(1), inc(.01) | steps(100) )"
    },
    "year": {
        "args": [
            "datetime"
        ],
        "description": "Returns the year part of a date-time value.",
        "prototype": "yr = Year( datetime )"
    },
    "yfunction": {
        "args": [
            "yExpr",
            "xName",
            "<properties>"
        ],
        "description": "Draws function yExpr in the Y dimension as variable xName varies across the range of the X axis of the graph. Additional named property arguments include Min(minimum X), Max(maximum X), Fill(fill pattern, value to fill to), Inc(upper bound of increment).",
        "prototype": "Y Function( yExpr, xName, <properties> )"
    },
    "yorigin": {
        "args": [],
        "description": "Returns the y value for the bottom edge of the graphics frame.",
        "prototype": "y = Y Origin()"
    },
    "yrange": {
        "args": [],
        "description": "Returns the y distance from bottom to top. Y Origin() + Y Range() is the top edge.",
        "prototype": "y = Y Range()"
    },
    "yscale": {
        "args": [
            "<yMin>",
            "<yMax>"
        ],
        "description": "Sets a new scale for the graphics frame.",
        "prototype": "Y Scale( <yMin>, <yMax> )"
    },
    "zeroormissing": {
        "args": [
            "x"
        ],
        "description": "Returns the logical NOT of x with missing values treated as zeros: 1 if x is missing or zero and 0 otherwise.",
        "prototype": "y = Zero Or Missing( x )"
    }
};