## JSL VSCode Extension
[![Marketplace Version](https://vsmarketplacebadge.apphb.com/version/vincefaller.jmp-scripting.svg)](https://marketplace.visualstudio.com/items?itemName=vincefaller.jmp-scripting) [![Installs](https://vsmarketplacebadge.apphb.com/installs/vincefaller.jmp-scripting.svg)](https://marketplace.visualstudio.com/items?itemName=vincefaller.jmp-scripting) [![Downloads](https://vsmarketplacebadge.apphb.com/downloads/vincefaller.jmp-scripting.svg)](https://marketplace.visualstudio.com/items?itemName=vincefaller.jmp-scripting) [![Rating](https://vsmarketplacebadge.apphb.com/rating/vincefaller.jmp-scripting.svg)](https://marketplace.visualstudio.com/items?itemName=vincefaller.jmp-scripting)

A Visual Studio Code extension with increasing support for the JMP Scripting Language (JSL)  

Developed By [Predictum Inc](https://predictum.com/). 

### Quick Start
1. Install the JSL Extension
2. In the extension there is a `JMPVSCode.jmpaddin` file.  Install it to JMP. 
    * Windows: `%USERPROFILE%\.vscode\extensions\`
    * Mac: `$HOME/.vscode/extensions`
    * Linux: `$HOME/.vscode/extensions`
3. Open a JSL workspace and start scripting!

### Features
* Syntax highlighting
* Document Symbols/Outlining  
![_symbols](https://gitlab.com/predictum/jsl-for-vscode/raw/master/src/assets/symbols.png)
* Hover Help  
![_hover](https://gitlab.com/predictum/jsl-for-vscode/raw/master/src/assets/Hover.gif)
* Running JSL through a [JMP addin](#running-jmp)  
![_running_jmp](https://gitlab.com/predictum/jsl-for-vscode/raw/master/src/assets/RunningJSL.gif)
* [Snippets](#snippets)  
![_snippets](https://gitlab.com/predictum/jsl-for-vscode/raw/master/src/assets/snippets.gif)

### Running JMP
In order to actually run JMP commands from VSCode, you'll have to:
1. Install the included `JSL-VSCode.jmpaddin` addin to JMP
2. Run the `VSCode Socket` command in Addins in it's **OWN INSTANCE** of JMP. 
    * it will lock up that instance.  Press `ESC` to stop it.  
3. Make sure your output is set to JSL
4. Now you can run selected text just like in JMP

### Snippets
Currently these are existing snippets.  Snippets for functions have comments compatible for use with [Natural Docs](https://www.naturaldocs.org/)
* for
* function
* new custom function
* method

### Coming Features
* Intellisense

### Issues
Please [visit the repo](https://gitlab.com/predictum/jsl-for-vscode) to put in additional issues or feature requests. 


### Caveat
I'm definitely not a Javascript person so updates will likely be slow going.

### Contributing
Email me at vince.faller@predictum.com.  I currently don't have any guidelines in place as I'm just trying to put something out.  



